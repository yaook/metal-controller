import contextlib
import json
import typing

import hvac

import environ


@environ.config(prefix="VAULT")
class VaultClientConfig:
    auth_path = environ.var("")
    role_id = environ.var("")
    secret_id = environ.var("")
    token = environ.var("")


@environ.config(prefix="VAULT")
class VaultConfig:
    policy_prefix = environ.var("yaook")
    path_prefix = environ.var("yaook")


@contextlib.contextmanager
def get_client() -> hvac.Client:
    cfg = environ.to_config(VaultClientConfig)
    client = hvac.Client()
    revoke = False
    if not cfg.role_id or not cfg.secret_id or not cfg.auth_path:
        if not cfg.token:
            # token is only a fallback, so we lie here
            raise environ.exceptions.MissingEnvValueError(
                "VAULT_ROLE_ID",
                "VAULT_SECRET_ID",
                "VAULT_AUTH_PATH",
            )
    else:
        revoke = True
        client.auth.approle.login(
            cfg.role_id,
            cfg.secret_id,
            mount_point=cfg.auth_path,
        )

    try:
        yield client
    finally:
        if revoke:
            client.auth.token.revoke_self()


def create_cluster(
        cluster: str,
        *,
        client: typing.Optional[hvac.Client] = None,
        ) -> None:
    if client is None:
        with get_client() as client:
            return create_cluster(
                cluster,
                client=client,
            )

    cfg = environ.to_config(VaultConfig)
    cluster_path = f"{cfg.path_prefix}/{cluster}"
    client.sys.enable_secrets_engine(
        "kv",
        path=f"{cluster_path}/kv",
        options={"version": 2},
    )
    client.sys.enable_secrets_engine(
        "ssh",
        path=f"{cluster_path}/ssh-ca",
    )
    client.write(
        f"{cluster_path}/ssh-ca/config/ca",
        generate_signing_key=True,
    )


def create_server(
        cluster: str,
        hostname: str,
        yaook_policies: typing.Collection[str],
        primary_ipv4: typing.Optional[str],
        primary_ipv6: typing.Optional[str],
        *,
        client: typing.Optional[hvac.Client] = None,
        ) -> typing.Tuple[str, str]:
    if client is None:
        with get_client() as client:
            return create_server(
                cluster,
                hostname,
                yaook_policies,
                primary_ipv4,
                primary_ipv6,
                client=client,
            )

    cfg = environ.to_config(VaultConfig)
    node_approle_name = f"{hostname}.node.{cluster}"
    node_approle_path = \
        f"auth/{cfg.path_prefix}/nodes/role/{node_approle_name}"
    node_sshca_path = \
        f"{cfg.path_prefix}/{cluster}/ssh-ca/roles/{node_approle_name}"

    client.delete(node_sshca_path)
    client.write(
        node_sshca_path,
        key_type="ca",
        ttl="720h",
        allow_host_certificates="true",
        allow_bare_domains="true",
        allowed_domains=node_approle_name,
        algorithm_signer="rsa-sha2-512",
    )
    client.delete(node_approle_path)
    client.write(
        node_approle_path,
        token_ttl="10m",
        token_max_ttl="1h",
        token_policies=",".join(f"{cfg.policy_prefix}/{policy}"
                                for policy in yaook_policies),
        token_no_default_policy="false",
        token_type="service",
    )

    role_id = client.read(
        f"{node_approle_path}/role-id",
    )["data"]["role_id"]

    metadata = json.dumps({
        "yaook_deployment": cluster,
        "yaook_hostname": hostname,
        "yaook_primary_ipv4": primary_ipv4 or "127.0.0.1",
        "yaook_primary_ipv6": primary_ipv6 or "::1",
    })

    wrapping_token = client.write(
        f"{node_approle_path}/secret-id",
        wrap_ttl="1h",
        metadata=metadata,
    )["wrap_info"]["token"]

    return role_id, wrapping_token
