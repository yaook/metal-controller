import ipaddress
import re
import random
import string
import typing

import attr
import environ
import yaml

import pynetbox
import socket

from . import backend, model, vault


rng = random.SystemRandom()

if typing.TYPE_CHECKING:
    class NetboxNodeStatusType:
        value: str
        label: str

    class NetboxIpAddress:
        address: str

    class NetboxNodeType:
        id: int
        name: str
        comments: str
        status: NetboxNodeStatusType
        cluster: typing.Any
        primary_ip4: typing.Optional[NetboxIpAddress]
        primary_ip6: typing.Optional[NetboxIpAddress]
        role: typing.Any
        device_type: typing.Any
        config_context: typing.Any
        tags: typing.List[typing.Any]
        save: typing.Callable[["NetboxNode"], None]

    NetboxNode = typing.NewType("NetboxNode", NetboxNodeType)
else:
    NetboxNode = typing.Any


# omitting c to avoid ambiguity with k
# omitting v to avoid ambiguity with f
# omitting w to avoid ambiguity with v for german speakers :)
# omitting g to avoid ambiguity with k
# omitting b to avoid ambiguity with p
# omitting z to avoid ambiguity with s
# omitting q to avoid ambiguity with k
# omitting y to avoid ambiguity with i and j
CONSONANTS = "dfhklmnprstxy"
VOWELS = "aeiou"


CF_HOSTNAME_PREFIX = "yaook_hostname_prefix"
CF_SCHEDULING_KEYS = "yaook_scheduling_keys"
CF_CONTROL_PLANE = "yaook_control_plane"
CF_YAOOK_KUBERNETES_NODE = "yaook_kubernetes_node"
CF_DNS_LABEL = "yaook_dns_label"

DEFAULT_BMC_VENDOR_DDNS_TEMPLATES = {
    "HPE": "ILO{serial}.{bmc_dns_suffix}",
    "DELL": "idrac-{serial}.{bmc_dns_suffix}"
}

_DUMP_MARKER_HEAD = "\n\n<!-- BEGIN OF METAL CONTROLLER DUMP -->\n\n"
_DUMP_MARKER_TAIL = "\n\n<!-- END OF METAL CONTROLLER DUMP -->\n\n"
_DUMP_MARKER_HEAD_RE = re.compile(
    r"(^|\r?\n?\r?\n)<!--\s*BEGIN\s+OF\s+METAL\s+CONTROLLER\s+DUMP\s*-->\r?\n\r?\n?",  # noqa:E501
    re.I,
)
_DUMP_MARKER_TAIL_RE = re.compile(
    r"\r?\n?\r?\n<!--\s*END\s+OF\s+METAL\s+CONTROLLER\s+DUMP\s*-->(\r?\n\r?\n?|$)",  # noqa:E501
    re.I,
)

assert _DUMP_MARKER_HEAD_RE.match(_DUMP_MARKER_HEAD)
assert _DUMP_MARKER_TAIL_RE.match(_DUMP_MARKER_TAIL)


_INFO_MARKER_HEAD = "\n\n<!-- BEGIN OF METAL CONTROLLER INFO -->\n\n"
_INFO_MARKER_TAIL = "\n\n<!-- END OF METAL CONTROLLER INFO -->\n\n"
_INFO_MARKER_HEAD_RE = re.compile(
    r"(^|\r?\n?\r?\n)<!--\s*BEGIN\s+OF\s+METAL\s+CONTROLLER\s+INFO\s*-->\r?\n\r?\n?",  # noqa:E501
    re.I,
)
_INFO_MARKER_TAIL_RE = re.compile(
    r"\r?\n?\r?\n<!--\s*END\s+OF\s+METAL\s+CONTROLLER\s+INFO\s*-->(\r?\n\r?\n?|$)",  # noqa:E501
    re.I,
)

assert _INFO_MARKER_HEAD_RE.match(_INFO_MARKER_HEAD)
assert _INFO_MARKER_TAIL_RE.match(_INFO_MARKER_TAIL)


def check_format_str(_instance, _attribute, value):
    string.Formatter().parse(value)


def check_custom_field(_instance, _attribute, value):
    if not value.startswith("cf_"):
        raise ValueError("custom field names must start with cf_")


def value_set(s):
    return {v.strip() for v in s.split(",")}


def match_hostname_re(assigned_hostname):
    hostname_re = re.compile(r"^[a-z][a-z0-9-]{0,31}(\..*|$)")
    return bool(hostname_re.match(assigned_hostname))


class PreflightError(Exception):
    pass


@environ.config(prefix="NETBOX")
class NetBoxConfig:
    url = environ.var()
    token = environ.var()

    error_tag = environ.var("yaook--has-orchestration-error")
    error_tag_autocreate = environ.bool_var(True)
    cluster_types = environ.var("yaook", converter=value_set)
    cluster_domain_field = environ.var("cf_yaook_domain")

    gateway_tag = environ.var("yaook--default-gateway")
    nameserver_field = environ.var("yaook_nameserver")
    ip_allocate_tag = environ.var("yaook--ip-auto-assign")
    ip_primary_tag = environ.var("yaook--ip-primary")

    @environ.config
    class NodeMatch:
        mode = environ.var(
            "by-interface",
            validator=attr.validators.in_([
                "by-interface",
                "by-attribute",
            ]),
        )
        field = environ.var(
            "mac_address",
        )
        expr = environ.var(
            "{inventory[bmc_mac]}",
            validator=check_format_str,
        )

    node_match = environ.group(NodeMatch)

    manageable_statuses = environ.var("planned,staged,active",
                                      converter=value_set)
    deployable_statuses = environ.var("staged,active",
                                      converter=value_set)

    ipmi_vault_mountpoint_template = environ.var(
        "yaook/{cluster}/kv",
        validator=check_format_str,
    )
    ipmi_vault_path_template = environ.var(
        "ipmi/{device.asset_tag}",
        validator=check_format_str,
    )
    ipmi_vault_username_key = environ.var("username")
    ipmi_vault_password_key = environ.var("password")

    should_deploy_yaook_kubernetes = environ.var("yaook_kubernetes_node")


def _merge_dump(existing: str, contents: str) -> str:
    """
    Inject `contents` into `existing`, using a marker to be able to substitute
    the contents later on while keeping other changes to `existing` intact.
    """
    existing = existing or ""
    parts = []
    head_match = _DUMP_MARKER_HEAD_RE.search(existing)
    tail_match = _DUMP_MARKER_TAIL_RE.search(existing)
    if (head_match is not None and
            tail_match is not None and
            head_match.end() <= tail_match.start()):
        parts.append(existing[:head_match.start()])
        parts.append(_DUMP_MARKER_HEAD)
        parts.append(contents)
        parts.append(_DUMP_MARKER_TAIL)
        parts.append(existing[tail_match.end():])
    else:
        parts.append(existing)
        parts.append(_DUMP_MARKER_HEAD)
        parts.append(contents)
        parts.append(_DUMP_MARKER_TAIL)
    return "".join(parts).rstrip()


def _merge_info(existing: str, contents: str) -> str:
    """
    Inject `contents` into `existing`, using a marker to be able to substitute
    the contents later on while keeping other changes to `existing` intact.
    """
    existing = existing or ""
    parts = []
    head_match = _INFO_MARKER_HEAD_RE.search(existing)
    tail_match = _INFO_MARKER_TAIL_RE.search(existing)
    if (head_match is not None and
            tail_match is not None and
            head_match.end() <= tail_match.start()):
        parts.append(existing[:head_match.start()])
        parts.append(_INFO_MARKER_HEAD)
        parts.append(contents)
        parts.append(_INFO_MARKER_TAIL)
        parts.append(existing[tail_match.end():])
    else:
        parts.append(existing)
        parts.append(_INFO_MARKER_HEAD)
        parts.append(contents)
        parts.append(_INFO_MARKER_TAIL)
    return "".join(parts).rstrip()


def gen_cvcv_Nnnn():
    num = rng.randint(1000, 9999)
    cvcv = (
        rng.choice(CONSONANTS) + rng.choice(VOWELS) +
        rng.choice(CONSONANTS) + rng.choice(VOWELS)
    )
    return f"{cvcv}-{num}"


def _netbox_longest_prefix_match(prefix):
    # As netbox returns already hierarchical sorted list of prefixes
    # we just need to convert the RecordSet into a list and use the
    # last prefix as this is always the most specific one

    return [a for a in prefix][-1]


def _make_netplan(client, device, gateway_tag, nameserver_field,
                  search_domains):
    ifaces = client.dcim.interfaces.filter(
        device_id=device.id,
        mgmt_only=False,
    )

    ethernets = {}
    bonds = {}
    vlans = {}

    netplan = {
        "version": 2,
        "ethernets": ethernets,
        "bonds": bonds,
        "vlans": vlans,
    }

    for iface in ifaces:
        main = {
            "link-local": [],
        }

        decls = {}
        if iface.untagged_vlan:
            decls[iface.untagged_vlan.vid] = main

        if iface.type.value == "lag":
            main["interfaces"] = [
                child_iface.name
                for child_iface in client.dcim.interfaces.filter(
                    lag_id=iface.id,
                    mgmt_only=False,
                )
            ]
            if iface.custom_fields["bond_mode"] == "active-backup":
                main["parameters"] = {
                    "mode": iface.custom_fields["bond_mode"],
                }
            else:
                main["parameters"] = {
                    "mode": iface.custom_fields["bond_mode"],
                    "transmit-hash-policy": iface.custom_fields["bond_hash"],
                }
            bonds[iface.name] = main
            bonds[iface.name]["link-local"] = []
            if iface.mtu:
                bonds[iface.name]["mtu"] = iface.mtu

        elif iface.type.value == "virtual":
            continue
        else:
            main["match"] = {"macaddress": iface.mac_address.lower()}
            ethernets[iface.name] = main
            ethernets[iface.name]["link-local"] = []
            ethernets[iface.name]["set-name"] = iface.name
            if iface.mtu:
                ethernets[iface.name]["mtu"] = iface.mtu

        for tagged_vlan in iface.tagged_vlans:
            vlan_decl = {
                "id": tagged_vlan.vid,
                "link": iface.name,
                "link-local": [],
            }
            if iface.mtu:
                vlan_decl["mtu"] = iface.mtu

            vlans[f"{iface.name}.{tagged_vlan.vid}"] = vlan_decl
            decls[tagged_vlan.vid] = vlan_decl

        for addr in client.ipam.ip_addresses.filter(
                interface_id=iface.id):

            match_cidr = _netbox_longest_prefix_match(
                client.ipam.prefixes.filter(
                    contains=addr.address,
                    vrf_id=addr.vrf.id,
                    status="active",
                )
            )

            prefix = client.ipam.prefixes.get(prefix=match_cidr,
                                              vrf_id=addr.vrf.id,
                                              status="active"
                                              )

            if addr.address.split("/")[1] != prefix.prefix.split("/")[1]:
                raise ValueError(
                    f"Netmask of address {addr.address} does not "
                    f"match that of prefix {prefix.prefix}.")

            try:
                decl = decls[prefix.vlan.vid]
            except KeyError as e:
                raise ValueError(
                    f"IP address {addr.address} from Prefix ID "
                    f"{prefix.id} ({prefix.prefix}) with VLAN {e}"
                    f" in VRF ID {prefix.vrf.id} is assigned to "
                    f" interface {iface.name}, but the VLAN {e} is "
                    f"not assigned to the interface {iface.name}."
                )
            decl.setdefault("addresses", []).append(
                addr.address,
            )

            try:
                gw = next(iter(client.ipam.ip_addresses.filter(
                    status="active",
                    vrf_id=addr.vrf.id,
                    parent=prefix.prefix,
                    tag=gateway_tag,
                )))
            except StopIteration:
                # no gateway
                pass
            else:
                decl.setdefault(
                    f"gateway{addr.family.value}",
                    str(ipaddress.ip_interface(gw.address).ip),
                )

            if addr.family.value == 6:
                link_local = decl["link-local"]
                if "ipv6" not in link_local:
                    link_local.append("ipv6")

            if prefix.custom_fields.get(nameserver_field):
                nameservers = [
                    x.strip() for x in
                    prefix.custom_fields[nameserver_field].split(",")
                ]
                nameservers_decl = decl.setdefault(
                    "nameservers",
                    {
                        "search": search_domains,
                    }
                )
                existing_addrs = nameservers_decl.setdefault("addresses", [])
                for nsaddr in nameservers:
                    if nsaddr not in existing_addrs:
                        existing_addrs.append(nsaddr)

    return netplan


class NetBoxBackend(backend.Backend):
    def __init__(self, logger):
        super().__init__()
        self.logger = logger
        config = environ.to_config(NetBoxConfig)
        self.client = pynetbox.api(
            url=config.url,
            token=config.token,
        )
        self._config = config

        if config.error_tag_autocreate:
            existing_tag = self.client.extras.tags.get(
                slug=self._config.error_tag,
            )
            if existing_tag is None:
                # create tag
                self.client.extras.tags.create(slug=self._config.error_tag,
                                               name=self._config.error_tag)

    def _compose_filter(
            self,
            field: str,
            expr: str,
            introspection: typing.Mapping,
            ) -> typing.Mapping:
        return {
            field: expr.format(inventory=introspection["inventory"]),
        }

    def _find_node_in_netbox_by_interface(
            self, introspection: typing.Mapping
            ) -> typing.Optional[NetboxNode]:
        iface_filter = self._compose_filter(
            self._config.node_match.field,
            self._config.node_match.expr,
            introspection,
        )

        ifaces = list(self.client.dcim.interfaces.filter(**iface_filter))
        if len(ifaces) == 0:
            self.logger.debug("no interface found for filter %r", iface_filter)
            return None
        elif len(ifaces) > 1:
            raise ValueError(
                "multiple matching interfaces found for "
                f"node filter {iface_filter!r}"
            )

        iface, = ifaces
        return self.client.dcim.devices.get(iface.device.id)

    def _find_node_in_netbox_by_attribute(
            self, introspection: typing.Mapping
            ) -> typing.Optional[NetboxNode]:
        device_filter = self._compose_filter(
            self._config.node_match.field,
            self._config.node_match.expr,
            introspection,
        )

        devices = list(self.client.dcim.devices.filter(**device_filter))
        if len(devices) == 0:
            return None
        elif len(devices) > 1:
            raise ValueError(
                "multiple matching devices found for "
                f"node filter {device_filter!r}"
            )

        return devices[0]

    def _find_node_in_netbox(
            self, introspection: typing.Mapping
            ) -> typing.Optional[NetboxNode]:
        if self._config.node_match.mode == "by-interface":
            return self._find_node_in_netbox_by_interface(introspection)
        else:
            return self._find_node_in_netbox_by_attribute(introspection)

    def _read_interfaces(
            self,
            device_id: int,
            ) -> typing.Collection:
        return list(self.client.dcim.interfaces.filter(
            device_id=device_id,
        ))

    def _read_cluster(
            self,
            device: NetboxNode,
            ) -> typing.Optional[str]:
        cluster = device.cluster
        if cluster is None:
            return None

        cluster.full_details()
        if cluster.type.slug not in self._config.cluster_types:
            return None

        return cluster.custom_fields[self._config.cluster_domain_field[3:]]

    def _read_node_type(
            self,
            device: NetboxNode,
            ) -> model.MetalControllerDeployMethod:

        match device.config_context.get("metal-controller", {}).get("deployment_method", "").upper():   # noqa:E501
            case str(model.MetalControllerDeployMethod.YAOOK_K8S):
                return model.MetalControllerDeployMethod.YAOOK_K8S
            case str(model.MetalControllerDeployMethod.NO_ADDITIONAL_DEPLOYMENT):  # noqa:E501
                return model.MetalControllerDeployMethod.NO_ADDITIONAL_DEPLOYMENT  # noqa:E501
            case str(model.MetalControllerDeployMethod.TEMPLATE_BASE):
                return model.MetalControllerDeployMethod.TEMPLATE_BASE
            case _:
                return model.MetalControllerDeployMethod.YAOOK_K8S

    def _merge_interfaces(
            self,
            device_id: int,
            device_interfaces: typing.Collection,
            inv_interfaces: typing.Collection[typing.Mapping]) -> None:
        nb_ifs_by_name = {
            iface.name: iface
            for iface in device_interfaces
        }
        nb_ifs_by_mac = {
            iface.mac_address: iface
            for iface in device_interfaces
            if iface.mac_address is not None
        }

        dirty = set()

        for inv_iface in inv_interfaces:
            try:
                nb_iface_by_mac = nb_ifs_by_mac[inv_iface["mac_address"]]
            except KeyError:
                pass
            else:
                new_name = inv_iface["name"]
                if nb_iface_by_mac.name == new_name:
                    # all good
                    continue

                try:
                    nb_iface_by_name = nb_ifs_by_name[new_name]
                except KeyError:
                    pass
                else:
                    # oh no. another interface has the name currently. we have
                    # to change it
                    orphan_name = f"orphaned-{nb_iface_by_name.id}"
                    self.logger.debug(
                        "naming conflict: renaming iface %s(%s) to %s",
                        nb_iface_by_name.name, nb_iface_by_name.id,
                        orphan_name,
                    )
                    nb_iface_by_name.name = orphan_name
                    nb_iface_by_name.save()
                    dirty.discard(nb_iface_by_name)

                self.logger.debug(
                    "renaming %s(%s) -> %s (matched on MAC %s)",
                    nb_iface_by_mac.name, nb_iface_by_mac.id, new_name,
                    inv_iface["mac_address"],
                )
                nb_iface_by_mac.name = new_name
                dirty.add(nb_iface_by_mac)
                continue

            try:
                nb_iface_by_name = nb_ifs_by_name[inv_iface["name"]]
            except KeyError:
                pass
            else:
                if nb_iface_by_name.mac_address is None:
                    self.logger.debug(
                        "setting MAC address on %s(%s) to %s "
                        "(matched on name)",
                        nb_iface_by_name.name, nb_iface_by_name.id,
                        inv_iface["mac_address"],
                    )
                    nb_iface_by_name.mac_address = inv_iface["mac_address"]
                    dirty.add(nb_iface_by_name)
                continue

            # no match, we create it
            self.client.dcim.interfaces.create(
                device=device_id,
                type="other",
                name=inv_iface["name"],
                mac_address=inv_iface["mac_address"],
                description=(
                    "filled in by metal-operator from Ironic "
                    "introspection data"
                ),
            )

        for iface in dirty:
            iface.save()

    def _merge_comment(
            self,
            device: NetboxNode,
            introspection: typing.Mapping,
            ) -> bool:
        parts = []
        parts.append("### Yaook Introspection Information")
        parts.append("")
        parts.append(f"IPMI address: {introspection['ipmi_address']}")
        parts.append("")

        new_comments = _merge_dump(device.comments, "\n".join(parts))
        if new_comments != device.comments:
            device.comments = new_comments
            return True

        return False

    def _set_message(
            self,
            device: NetboxNode,
            message: str,
            ) -> None:
        new_comments = _merge_info(device.comments, message)
        if new_comments != device.comments:
            device.comments = new_comments
            device.save()

    def _set_error(
            self,
            device: NetboxNode,
            message: str) -> None:
        self._set_message(
            device,
            f"### Yaook Orchestration Status\n\n**Error:** {message}",
        )
        try:
            device.tags.append({"slug": self._config.error_tag})
            device.save()
        except Exception as exc:
            self.logger.error("failed to add error tag to {}: {}",
                              device.id, exc)

    def _clear_error(
            self,
            device):
        self._set_message(device, "")
        for i, tag in enumerate(device.tags):
            if tag.slug == self._config.error_tag:
                del device.tags[i]
                device.save()
                break

    def _merge_introspection(
            self, device: NetboxNode, introspection: typing.Mapping) -> None:
        nb_ifs = self._read_interfaces(device.id)
        self._merge_interfaces(
            device.id,
            nb_ifs,
            introspection["inventory"]["interfaces"],
        )

        changed = False
        changed = self._merge_comment(
            device,
            introspection,
        ) or changed

        if changed:
            device.save()

    def _get_ipmi_credentials(
            self,
            cluster: str,
            device: NetboxNode) -> typing.Tuple[str, str]:
        kwargs = {
            "cluster": cluster,
            "device": device,
        }

        mount_point = self._config.ipmi_vault_mountpoint_template.format(
            **kwargs
        )
        path = self._config.ipmi_vault_path_template.format(**kwargs)

        with vault.get_client() as vault_client:
            payload = vault_client.secrets.kv.read_secret_version(
                mount_point=mount_point,
                path=path,
            )
        data = payload["data"]["data"]

        return (
            data[self._config.ipmi_vault_username_key],
            data[self._config.ipmi_vault_password_key],
        )

    def _validate_device(
            self, device: NetboxNode, node: model.NodeBrief) -> None:
        if node.backend_node_id is None:
            return
        try:
            id_ = int(node.backend_node_id)
        except (ValueError, TypeError):
            raise ValueError(
                "device ID from Ironic is invalid",
            )

        if id_ != device.id:
            raise ValueError(
                "device ID from Ironic mismatches discovered node",
            )

    def _get_device(
            self, backend_device_id: typing.Optional[str]
            ) -> typing.Optional[NetboxNode]:
        if backend_device_id is None:
            return None

        try:
            device_id = int(backend_device_id)
        except (ValueError, TypeError):
            raise ValueError(
                f"invalid device id: {backend_device_id!r}"
            ) from None

        return self.client.dcim.devices.get(device_id)

    def _get_bmc_management_type(self, device):
        return device.device_type.custom_fields.get(
                'yaook_bmc_management_method',
                'ipmi'
             )

    def _lookup_ipmi_address_if_not_present(self, introspection, device):
        vendor = introspection["inventory"]["system_vendor"]["manufacturer"]
        params = {
            "bmc_dns_suffix": device.config_context.get(
                "metal-controller", {}
            ).get("bmc_dns_suffix"),
            "serial": introspection["inventory"]["system_vendor"][
                "serial_number"
            ],
        }
        bmc_fqdn = DEFAULT_BMC_VENDOR_DDNS_TEMPLATES[vendor].format(**params)

        try:
            return socket.gethostbyaddr(bmc_fqdn)[2][0]
        except socket.gaierror:
            raise ValueError(
                "failed to get ipmi-address through fallback method."
                f" skipping {str(device.id)} with serial {params['serial']}."
            )

    def _dns_reverse_lookup(self, address):
        try:
            return socket.gethostbyaddr(address)[0]
        except socket.herror:
            return address

    def _get_ip_address_in_vlan(self, iface, vlan, family):
        addrs = self.client.ipam.ip_addresses.filter(
            interface_id=iface.id,
        )

        for addr in addrs:
            bare_address = ipaddress.ip_interface(addr.address).ip
            if bare_address.version != family:
                continue

            try:
                next(iter(self.client.ipam.prefixes.filter(
                    contains=str(bare_address),
                    vlan_id=vlan.id
                )))
            except StopIteration:
                continue
            else:
                return addr

        return None

    def _find_interface_prefix_length(self, address, vlan_id):
        smallest_prefix_length = ipaddress.ip_address(address).max_prefixlen
        for prefix in self.client.ipam.prefixes.filter(
                vlan_id=vlan_id,
                contains=address):
            prefix_len = ipaddress.ip_network(prefix.prefix).prefixlen
            if prefix_len < smallest_prefix_length:
                smallest_prefix_length = prefix_len

        return smallest_prefix_length

    def _autoallocate_ip_address_in_vlan(self, iface, vlan, family, hostfqdn):
        existing = self._get_ip_address_in_vlan(iface, vlan, family)
        if existing is not None:
            return existing

        last_exc = None

        for prefix in self.client.ipam.prefixes.filter(
                vlan_id=vlan.id,
                tag=self._config.ip_allocate_tag,
                family=family,
                status="active"):
            try:
                addr = prefix.available_ips.create()
            except pynetbox.core.query.AllocationError as exc:
                last_exc = exc
                continue
            ipaddr = ipaddress.ip_interface(addr.address).ip
            prefixlen = self._find_interface_prefix_length(
                str(ipaddr),
                vlan.id,
            )
            addr.address = f"{ipaddr}/{prefixlen}"
            addr.assigned_object_type = "dcim.interface"
            addr.assigned_object_id = iface.id
            role = prefix.role or vlan.role
            role.full_details()

            dns_label = role.custom_fields[CF_DNS_LABEL]
            if dns_label != ".":
                if not dns_label:
                    addr.dns_name = hostfqdn
                else:
                    addr.dns_name = f"{dns_label}.{hostfqdn}"

            try:
                addr.save()
            except Exception:
                addr.delete()
                raise

            return addr

        if last_exc is not None:
            # pools found, but all failed to allocate
            raise last_exc

        raise ValueError(
            f"no prefix declared with tag {self._config.ip_allocate_tag!r} in"
            f" vlan {vlan.name}({vlan.id}), but the VLAN requests to allocate "
            "an IP"
        )

    def _autoallocate_ip_addresses_for_iface(self, iface, hostfqdn):
        vlans = []
        if iface.untagged_vlan is not None:
            vlans.append(iface.untagged_vlan)
        for tagged in iface.tagged_vlans:
            vlans.append(tagged)

        vlans = [
            vlan for vlan in vlans
            if any(tag.slug == self._config.ip_allocate_tag
                   for tag in vlan.tags)
        ]

        primary_v4 = None
        primary_v6 = None
        for vlan in vlans:
            is_primary = any(tag.slug == self._config.ip_primary_tag
                             for tag in vlan.tags)
            ipv4 = self._autoallocate_ip_address_in_vlan(
                iface, vlan, 4, hostfqdn,
            )
            ipv6 = self._autoallocate_ip_address_in_vlan(
                iface, vlan, 6, hostfqdn,
            )
            if is_primary:
                if ipv4 is not None:
                    primary_v4 = ipv4
                if ipv6 is not None:
                    primary_v6 = ipv6

        return primary_v4, primary_v6

    def _autoallocate_ip_addresses_for_device(self, device):
        cluster_domain_name = self._read_cluster(device)
        fqdn = f"{device.name}.node.{cluster_domain_name}"

        primary_v4, primary_v6 = None, None
        for iface in self.client.dcim.interfaces.filter(device_id=device.id):
            new_prim_v4, new_prim_v6 = \
                self._autoallocate_ip_addresses_for_iface(iface, fqdn)

            if new_prim_v4:
                primary_v4 = new_prim_v4
            if new_prim_v6:
                primary_v6 = new_prim_v6

        primary_updated = False
        if primary_v4 is not None:
            primary_updated = True
            device.primary_ip4 = primary_v4.id
        if primary_v6 is not None:
            primary_updated = True
            device.primary_ip6 = primary_v6.id
        if primary_updated:
            device.save()

    def _validate_role(self, role):
        try:
            hostname_prefix = role.custom_fields[CF_HOSTNAME_PREFIX]
            role.custom_fields[CF_SCHEDULING_KEYS]
            role.custom_fields[CF_CONTROL_PLANE]
            role.custom_fields[CF_YAOOK_KUBERNETES_NODE]
        except KeyError as exc:
            raise ValueError(
                f"role {role.name}({role.id}) is missing the custom field "
                f"{exc}",
            ) from None

        if not hostname_prefix:
            return False

        return True

    def _autoassign_hostname(self, device):
        assigned_hostname = device.name
        if assigned_hostname:
            if not match_hostname_re(assigned_hostname):
                raise ValueError(
                    f"{assigned_hostname!r} is not a valid hostname",
                )
            return

        role = device.role
        role.full_details()
        prefix = role.custom_fields[CF_HOSTNAME_PREFIX]
        for i in range(3):
            new_hostname = f"{prefix}-{gen_cvcv_Nnnn()}"
            device.name = new_hostname
            try:
                device.save()
            except pynetbox.core.query.RequestError as exc:
                last_exc = exc
                continue
            break
        else:
            raise RuntimeError(
                f"failed to generate unique hostname for device {device.id}"
                f" after three attempts; giving up. last error: {last_exc}",
            )

    def enroll(
            self,
            node: model.NodeBrief,
            introspection: typing.Mapping,
            ) -> backend.Action:
        device = self._find_node_in_netbox(introspection)
        if device is None:
            self.logger.warning("failed to find netbox device for %r", node)
            return backend.ErrorAction(
                message="node not found in backend",
                backend_node_id=None,
            )

        self._validate_device(device, node)

        try:
            cluster = self._read_cluster(device)
        except KeyError:
            raise RuntimeError(
                "cluster domain field %r not found in cluster of device: %r",
                self._config.cluster_domain_field, device,
            )

        try:
            metal_controller_deployment_mode = self._read_node_type(device)
        except KeyError:
            raise RuntimeError(
                "device role field %r not found for cluster of device: %r",
                self._config.should_deploy_yaook_kubernetes, device
            )

        self.logger.debug(
            "syncing introspection data for node %s with device %s(%d)",
            node.id_, device.name, device.id,
        )
        self._merge_introspection(device, introspection)

        if device.status.value not in self._config.manageable_statuses:
            self.logger.debug(
                "ignoring node %s because its device %s(%d) is in status "
                "%s (=%r), which is not one of %r allowing enroll",
                node, device.name, device.id, device.status.label,
                device.status.value, self._config.manageable_statuses,
            )
            return backend.PassAction(
                backend_node_id=str(device.id),
                message=f"status {device.status.value!r} is not enrollable",
            )

        if not cluster:
            self.logger.debug(
                "ignoring device %s(%d) of node %s not associated with any "
                "cluster",
                device.name, device.id, node.id_,
            )
            return backend.PassAction(
                message="no cluster assigned",
                backend_node_id=str(device.id),
            )

        device.role.full_details()
        device.device_type.full_details()
        if not self._validate_role(device.role):
            self._set_error(
                device,
                f"role {device.role.name!r} ({device.role.id}) "
                "is incomplete; "
                "ensure that the hostname prefix is set correctly"
            )
            return backend.ErrorAction(
                message="role is invalid; see comment for details",
                backend_node_id=str(device.id),
            )

        self._autoassign_hostname(device)

        try:
            ipmi_username, ipmi_password = self._get_ipmi_credentials(
                cluster,
                device,
            )
        except Exception as exc:
            self._set_error(
                device,
                "failed to fetch IPMI credentials: "
                f"`{type(exc).__name__}: {exc}`",
            )
            raise

        try:
            ipmi_address = introspection["ipmi_v6address"]
            if ipmi_address is None or ipmi_address.startswith("fe80"):
                ipmi_address = None
        except KeyError:
            ipmi_address = None

        if ipmi_address is None:
            ipmi_address = introspection["ipmi_address"]

        if ipmi_address is None:
            ipmi_address = \
                  self._lookup_ipmi_address_if_not_present(introspection=introspection, device=device)  # noqa E0501

        if self._get_bmc_management_type(device=device) == "redfish":
            _driver = "redfish"
            _driver_info = {
                "redfish_address": f"https://{self._dns_reverse_lookup(ipmi_address)}",  # noqa E0501
                "redfish_username": ipmi_username,
                "redfish_password": ipmi_password,
                "redfish_verify_ca": False,
            }
        else:
            _driver = "ipmi"
            _driver_info = {
                "ipmi_username": ipmi_username,
                "ipmi_password": ipmi_password,
                "ipmi_address": ipmi_address,
            }
        return backend.ManageAction(
            name=device.name,
            driver=_driver,
            driver_info=_driver_info,
            description=(
                "Managed by the Yaook Metal Controller "
                f"(NetBox device ID {device.id})"
            ),
            backend_node_id=str(device.id),
            metal_controller_deployment_mode=metal_controller_deployment_mode,
            config_context=device.config_context.get("metal-controller", {})
        )

    def list_sync(self, nodes: typing.Collection[model.NodeBrief]) -> None:
        pass

    def _manage_preflight(
            self,
            device: NetboxNode) -> typing.Tuple[
                # network-config
                typing.Mapping[str, typing.Any],
                # is control plane
                bool,
                # is yaook node
                model.MetalControllerDeployMethod,
                # cluster domain
                str,
                # message
                typing.List[str],
            ]:
        """
        Preflight the node deployment and return, among others, a sequence of
        paragraphs to include in the status message.
        """

        try:
            cluster = self._read_cluster(device)
        except KeyError:
            raise RuntimeError("cluster field %r not found in device: %r",
                               self._config.cluster_field, device)

        if cluster is None:
            raise PreflightError("device is not assigned to a Yaook cluster "
                                 "and/or cluster role name is not specified "
                                 "in NETBOX_CLUSTER_TYPES env var")

        role = device.role
        role.full_details()
        if not self._validate_role(role):
            raise PreflightError(
                "the assigned role is incomplete; ensure that the hostname "
                "prefix is set"
            )
        is_control_plane = role.custom_fields[CF_CONTROL_PLANE]

        metal_controller_deployment_method = self._read_node_type(device)

        self._autoallocate_ip_addresses_for_device(device)
        network_config = _make_netplan(
            self.client,
            device,
            self._config.gateway_tag,
            self._config.nameserver_field,
            [f"node.{cluster}", cluster],
        )

        message = [
            "#### Planned Network Configuration",
            f"```yaml\n{yaml.safe_dump(network_config, sort_keys=False)}\n```",
        ]

        return network_config, bool(is_control_plane), \
            metal_controller_deployment_method, str(cluster), message

    def manage(
            self, node: model.NodeBrief, introspection: typing.Mapping
            ) -> backend.Action:
        device = self._find_node_in_netbox(introspection)
        if device is None:
            self.logger.warning("failed to find netbox device for %r", node)
            return backend.ErrorAction(
                message="node not found in backend",
                backend_node_id=None,
            )

        self._validate_device(device, node)

        if device.status.value not in self._config.manageable_statuses:
            return backend.PassAction(
                message=(
                    "waiting for manageable status (one of {})".format(
                        ", ".join(self._config.deployable_statuses),
                    )
                ),
                backend_node_id=str(device.id),
            )

        if device.primary_ip4 is None and device.primary_ip6 is None:
            return backend.PassAction(
                message="waiting for primary ip to be assigned",
                backend_node_id=str(device.id),
            )

        network_config, is_control_plane, metal_controller_deployment_mode, \
            cluster, message = self._manage_preflight(device)

        msg_parts = [
            "### Yaook Orchestration Status",
            "",
        ] + message

        if metal_controller_deployment_mode == model.MetalControllerDeployMethod.YAOOK_K8S:   # noqa:E501
            try:
                cluster_info = self._get_cluster_info(device.cluster.id)
            except Exception as exc:
                self._set_error(device,
                                f"Failed to collect cluster information: {exc}")   # noqa:E501
                return backend.ErrorAction(
                    message="failed to collect cluster information",
                    backend_node_id=str(device.id),
                )
        else:
            cluster_info = None

        if device.status.value not in self._config.deployable_statuses:
            msg_parts[1] = (
                "Status: Waiting for the NetBox device to enter a deployable "
                "status (one of {})".format(
                    ", ".join(self._config.deployable_statuses)
                )
            )
            self._set_message(device, "\n\n".join(msg_parts))
            return backend.PassAction(
                message=(
                    "pre-flight checks passed, waiting for deployable status"
                    " (one of {})".format(
                        ", ".join(self._config.deployable_statuses),
                    )
                ),
                backend_node_id=str(device.id),
            )

        msg_parts[1] = "Status: Deploying!"

        self._set_message(device, "\n\n".join(msg_parts))

        def unwrap_addr(addr):
            if addr is None:
                return None
            return str(ipaddress.ip_interface(addr.address).ip)

        return backend.DeployAction(
            backend_node_id=str(device.id),
            network_config=network_config,
            hostname=device.name,
            primary_ipv4=unwrap_addr(device.primary_ip4),
            primary_ipv6=unwrap_addr(device.primary_ip6),
            cluster=cluster,
            cluster_info=cluster_info,
            host_vars={},
            is_control_plane=is_control_plane,
            metal_controller_deployment_mode=metal_controller_deployment_mode,
            config_context=device.config_context.get("metal-controller", {})
        )

    def failure(self, node: model.NodeBrief, error: str) -> None:
        try:
            device = self._get_device(node.backend_node_id)
        except ValueError:
            self.logger.warning(
                "discarding error for node %s because its associated device "
                "id is invalid: %r",
                node.id_, node.backend_node_id,
            )
            return

        if device is None:
            self.logger.warning(
                "discarding error for node %s because no valid device is "
                "associated (backend_node_id=%r)",
                node.id_, node.backend_node_id,
            )
            return

        self._set_error(
            device,
            f"Ironic failed to reach state `{node.intended_state}`:\n"
            f"```\n{error}\n```"
        )

    def success(self, node: model.NodeBrief) -> None:
        try:
            device = self._get_device(node.backend_node_id)
        except ValueError:
            self.logger.warning(
                "discarding success for node %s because its associated device "
                "id is invalid: %r",
                node.id_, node.backend_node_id,
            )
            return

        if device is None:
            self.logger.warning(
                "discarding success for node %s because no valid device is "
                "associated (backend_node_id=%r)",
                node.id_, node.backend_node_id,
            )
            return

        self._clear_error(device)

    def _get_node_info(self, device: NetboxNode) -> model.NodeInfo:
        device.role.full_details()

        def unwrap_addr(addr):
            if addr is None:
                return None
            return str(ipaddress.ip_interface(addr.address).ip)

        is_control_plane = device.role.custom_fields[CF_CONTROL_PLANE]
        return model.NodeInfo(
            is_control_plane=is_control_plane,
            ipv4=unwrap_addr(device.primary_ip4),
            ipv6=unwrap_addr(device.primary_ip6),
        )

    def _get_active_nodes(
            self,
            cluster_id: int,
            ) -> typing.Dict[str, model.NodeInfo]:
        nodes = {}
        for status in self._config.deployable_statuses:
            for node in self.client.dcim.devices.filter(
                    cluster_id=cluster_id,
                    status=status):
                node.role.full_details()
                if not self._validate_role(node.role):
                    continue
                if node.primary_ip4 is None and node.primary_ip6 is None:
                    continue
                nodes[node.name] = self._get_node_info(node)
        if not nodes:
            raise ValueError(
                "Deploying a cluster without active nodes is not reasonable.")
        return nodes

    def _get_cluster_info(self, cluster_id: int) -> model.ClusterInfo:
        return model.ClusterInfo(
            active_nodes=self._get_active_nodes(cluster_id),
            subnet_v4=None,
            api_server_frontend_ip=None,
            api_server_vrrp_ip=None,
            subnet_v6=None,
            api_server_frontend_ip6=None,
            api_server_vrrp_ip6=None,
        )

    def get_cluster_info(self, cluster: str) -> model.ClusterInfo:
        filter_args = {
            self._config.cluster_domain_field: cluster,
        }

        try:
            nbcluster = next(iter(self.client.virtualization.clusters.filter(
                **filter_args,
            )))
        except StopIteration:
            raise LookupError(cluster) from None

        return self._get_cluster_info(nbcluster.id)
