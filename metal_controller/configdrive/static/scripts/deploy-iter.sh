#!/bin/bash
set -euo pipefail
umask 0027
skiptags="$(jq -r .ds.meta_data.yaook_deploy_skip_tags /run/cloud-init/instance-data-sensitive.json || echo 'dontskipanytagsorryforthis')"
node_role="$(jq -r .ds.meta_data.node_role /run/cloud-init/instance-data-sensitive.json)"
export AFLAGS="-c local -l $(hostname) --skip-tags ssh-known-hosts,detect-user,$(printf '%q' "$skiptags")"

. /etc/vault/config
export VAULT_ADDR VAULT_CACERT
mkdir -p inventory/.etc

function revoke() {
    vault token revoke -self 2>/dev/null || true
}

trap revoke EXIT

deployment="$(cat /etc/vault/deployment)"
export VAULT_TOKEN="$(/var/lib/yaook-cloud-init/vault-login.sh)"

pushd managed-k8s >/dev/null
path_bak="$PATH"
export VIRTUAL_ENV="$(realpath ../../venv)"
export PATH="$VIRTUAL_ENV/bin:$path_bak"
export VIRTUAL_ENV="$(poetry env info --path)"
export PATH="$VIRTUAL_ENV/bin:$path_bak"
popd >/dev/null
export WG_USAGE=false
export TF_USAGE=false
export KUBECONFIG=/etc/kubernetes/admin.conf

./managed-k8s/actions/apply-k8s-core.sh
systemctl disable yaook-deploy.service
