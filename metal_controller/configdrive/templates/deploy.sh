#!/bin/bash
set -euo pipefail
umask 0027
# Some globals
yaook_path=/var/lib/yaook-cloud-init
vault_dir=/etc/vault
export DEBIAN_FRONTEND=noninteractive

# configure apt mirror, if any
python3 <<EOF
import json
import subprocess
data = json.load(open("/run/cloud-init/instance-data-sensitive.json", "r", encoding="utf-8"))["ds"]["meta_data"]
try:
    apt_proxy = data["yaook_apt_proxy"]
except KeyError:
    pass
else:
    with open("/etc/apt/apt.conf.d/00proxy", "w") as f:
        print('Acquire::http::Proxy "{}";'.format(apt_proxy), file=f)

EOF
apt-get update
apt-get install -y jq

hashicorp_gpg_key=$(jq -r '.ds.meta_data.yaook_apt_hashicorp_gpg_key // ""' /run/cloud-init/instance-data-sensitive.json)
if [ -n "$hashicorp_gpg_key" ]; then
    gpg --dearmor <<<"$hashicorp_gpg_key" > /usr/share/keyrings/hashicorp.gpg
else
    wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor > /usr/share/keyrings/hashicorp.gpg
fi

hashicorp_repo_address=$(jq -r '.ds.meta_data.yaook_apt_hashicorp_repo_url // "https://apt.releases.hashicorp.com"' /run/cloud-init/instance-data-sensitive.json)

# install some stuff we need
echo "deb [signed-by=/usr/share/keyrings/hashicorp.gpg] $hashicorp_repo_address $(lsb_release -cs) main" > /etc/apt/sources.list.d/hashicorp.list
chmod go+r /usr/share/keyrings/hashicorp.gpg
apt-get update
apt-get install -y pass git tar jq moreutils vault

if ! [ -d "$vault_dir" ]; then
  # We need to unwrap our secret ASAP for security reasons
  mkdir -m 0700 -p "$vault_dir"  # for the ca, we need this early
  ln -s /etc/vault /etc/vault-client  # for backward compatibility
  role_id="$(jq -r .ds.meta_data.yaook_role_id /run/cloud-init/instance-data-sensitive.json)"
  deployment="$(jq -r .ds.meta_data.yaook_deployment /run/cloud-init/instance-data-sensitive.json)"
  secret_id_wrapping_token="$(jq -r .ds.meta_data.yaook_secret_id_wrapping_token /run/cloud-init/instance-data-sensitive.json)"
  export VAULT_ADDR="$(jq -r .ds.meta_data.yaook_vault_addr /run/cloud-init/instance-data-sensitive.json)"
  jq -r .ds.meta_data.yaook_vault_cacert /run/cloud-init/instance-data-sensitive.json > "$vault_dir/ca.crt"
  export VAULT_CACERT="$vault_dir/ca.crt"
  secret_id="$(VAULT_TOKEN="$secret_id_wrapping_token" vault unwrap -field=secret_id)"

  # Prepare the vault client config
  groupadd --force --system vault-client
  chown -R root:vault-client "$vault_dir"
  chmod -R u=rwx,g=rx "$vault_dir"

  # Write all the config into the directory
  cat >"$vault_dir/role-id" <<<"$role_id"
  cat >"$vault_dir/secret-id" <<<"$secret_id"
  cat >"$vault_dir/deployment" <<<"$deployment"
  printf 'VAULT_ADDR=%q\nVAULT_CACERT=%q\nVAULT_CAPATH="$VAULT_CACERT"\n' "$VAULT_ADDR" "$VAULT_CACERT" > "$vault_dir/config"
fi

# Set up the pip mirror if needed
pip_mirror_url=$(jq -r '.ds.meta_data.yaook_pip_mirror_url // ""' /run/cloud-init/instance-data-sensitive.json)
if [ -n "$pip_mirror_url" ]; then
    cat > /etc/pip.conf <<EOF
[global]
index-url=$pip_mirror_url
EOF
    pip_mirror_ca=$(jq -r '.ds.meta_data.yaook_pip_mirror_cacert // ""' /run/cloud-init/instance-data-sensitive.json)
    if [ -n "$pip_mirror_ca" ]; then
        mkdir -p -m 0755 /etc/pip
        cat > /etc/pip/pip_mirror_ca.pem <<<"$pip_mirror_ca"
        cat >> /etc/pip.conf <<EOF
cert=/etc/pip/pip_mirror_ca.pem
EOF
        chmod go+rx /etc/pip
        chmod go+r /etc/pip/pip_mirror_ca.pem
    fi
    # ensure that all users can read pip config and the CA file
    chmod go+r /etc/pip.conf
fi

# Now unpack the embedded archive to get all the nice scripts
cd "$yaook_path/tmp"
base64 -d <<'EOF' | tar -xj
{{ tarball }}
EOF
cd "$yaook_path"
mv tmp/scripts/*.sh .
mv tmp/systemd/*.* /etc/systemd/system/
rm -rf cluster # this is here to make the script run again without errors
mv tmp/cluster/ cluster
systemctl daemon-reload

# Now we can start and do useful things
systemctl enable --now --no-block renew-ssh-certificates.timer

apt-get install -y python3-venv python3-cryptography python3-dev
venv_path="$yaook_path/venv"
python3 -m venv "$venv_path"
export VIRTUAL_ENV="$venv_path"
path_bak="$PATH"
export PATH="$venv_path/bin:$PATH"
python3 -m pip install poetry
pushd "$yaook_path/cluster/managed-k8s" >/dev/null
poetry --no-root install
popd

unset VIRTUAL_ENV
export PATH="$path_bak"
unset path_bak

collections_preload=$(jq -r '.ds.meta_data.yaook_ansible_collections_preload // ""' /run/cloud-init/instance-data-sensitive.json)
if [ -n "$collections_preload" ]; then
    collections_preload_ca=$(jq -r '.ds.meta_data.yaook_ansible_collections_preload_ca // ""'  /run/cloud-init/instance-data-sensitive.json)
    cat <<<"$collections_preload_ca" > "$yaook_path/tmp/collections_ca.crt"
    pushd ~ >/dev/null
    # this is totally evil.
    curl --cacert "$yaook_path/tmp/collections_ca.crt" "$collections_preload" | tar -xz
    chown -R root: ~/.ansible || true  # if we don't actually get that
    popd >/dev/null
fi

# And now we’re good to go to iterate on the installation until we’re happy
systemctl enable --now --no-block yaook-deploy.service
