import argparse
import base64
import email.mime.multipart
import gzip

import yaml

import metal_controller.configdrive as configdrive


parser = argparse.ArgumentParser()
parser.add_argument(
    "-o", "--out-file",
    dest="out_file",
    required=True,
)
parser.add_argument(
    "-E", "--no-encode",
    dest="encode",
    default=True,
    action="store_false",
    help="If set, the output is not compressed and not encoded with base64",
)
parser.add_argument(
    "meta_data",
    type=argparse.FileType("r"),
)
parser.add_argument(
    "network_config",
    type=argparse.FileType("r"),
)
parser.add_argument(
    "deploy_script",
    type=argparse.FileType("rb"),
)
parser.add_argument(
    "-U", "--user-data-part",
    metavar=("SRCFILE", "NAME", "MIMETYPE"),
    nargs=3,
    action="append",
    default=[],
    dest="user_data_parts",
)

args = parser.parse_args()

with args.meta_data as f:
    meta_data = yaml.safe_load(f)

with args.network_config as f:
    network_config = yaml.safe_load(f)

parts = []
for (filename, name, mimetype) in args.user_data_parts:
    with open(filename, "rb") as f:
        data = f.read()
    maintype, subtype = mimetype.split("/")
    parts.append(configdrive._pack_mime(name, maintype, subtype, data))

if parts:
    user_data = email.mime.multipart.MIMEMultipart()
    for part in parts:
        user_data.attach(part)
    user_data_bytes = user_data.as_bytes()
else:
    user_data_bytes = b""

with args.deploy_script as f:
    deploy_script_bytes = f.read()

drive = configdrive.assemble_configdrive(
    meta_data=meta_data,
    network_config=network_config,
    user_data=user_data_bytes,
    deploy_script=deploy_script_bytes
)

if args.encode:
    drive = base64.b64encode(gzip.compress(drive))

with open(args.out_file, "wb") as f:
    f.write(drive)
