import dataclasses
import typing

from openstack.connection import Connection as OSConnection
from openstack.baremetal.v1.node import Node as BaremetalNode

from enum import auto
from strenum import StrEnum

EXTRA_KEY_INTENDED_STATE = "yaook_cloud_intended_state"
EXTRA_KEY_BACKEND_NODE_ID = "yaook_cloud_backend_node_id"
EXTRA_KEY_YAOOK_KUBERNETES_NODE_ROLE = "metal_controller_deployment_method"
EXTRA_KEY_LAST_MESSAGE = "yaook_cloud_last_message"


class MetalControllerDeployMethod(StrEnum):
    YAOOK_K8S = auto()
    NO_ADDITIONAL_DEPLOYMENT = auto()
    TEMPLATE_BASE = auto()


def update_extra_dict(
        extra: typing.Mapping[str, str],
        *,
        backend_node_id: typing.Optional[str] = None,
        metal_controller_deployment_mode: MetalControllerDeployMethod = MetalControllerDeployMethod.YAOOK_K8S,  # noqa:E501
        intended_state: typing.Optional[str] = None,
        last_message: typing.Optional[str] = None,
        ) -> typing.Dict[str, str]:
    extra = dict(extra)
    if backend_node_id is None:
        extra.pop(EXTRA_KEY_BACKEND_NODE_ID, None)
    else:
        extra[EXTRA_KEY_BACKEND_NODE_ID] = backend_node_id

    extra[EXTRA_KEY_YAOOK_KUBERNETES_NODE_ROLE] = \
        str(metal_controller_deployment_mode)
    if intended_state is None:
        extra.pop(EXTRA_KEY_INTENDED_STATE, None)
    else:
        extra[EXTRA_KEY_INTENDED_STATE] = intended_state
    if last_message is None:
        extra.pop(EXTRA_KEY_LAST_MESSAGE, None)
    else:
        extra[EXTRA_KEY_LAST_MESSAGE] = last_message
    return extra


def update_extra_attributes(
        conn: OSConnection,
        node: BaremetalNode,
        *,
        backend_node_id: typing.Optional[str] = None,
        metal_controller_deployment_mode: MetalControllerDeployMethod = MetalControllerDeployMethod.YAOOK_K8S,  # noqa:E501
        intended_state: typing.Optional[str] = None,
        last_message: typing.Optional[str] = None,
        ) -> None:
    conn.baremetal.update_node(
        node.id,
        extra=update_extra_dict(
            node.extra,
            backend_node_id=backend_node_id,
            metal_controller_deployment_mode=metal_controller_deployment_mode,
            intended_state=intended_state,
            last_message=last_message,
        ),
    )


@dataclasses.dataclass
class NodeBrief:
    id_: str
    name: str
    provision_state: str
    backend_node_id: typing.Optional[str]
    intended_state: typing.Optional[str]
    ignored: bool

    @classmethod
    def from_openstack(cls, obj):
        return NodeBrief(
            id_=obj.id,
            name=obj.name,
            provision_state=obj.provision_state,
            backend_node_id=obj.extra.get(EXTRA_KEY_BACKEND_NODE_ID),
            intended_state=obj.extra.get(EXTRA_KEY_INTENDED_STATE),
            ignored=False,
        )


@dataclasses.dataclass
class NodeInfo:
    ipv4: typing.Optional[str]
    ipv6: typing.Optional[str]
    is_control_plane: bool


@dataclasses.dataclass
class ClusterInfo:
    subnet_v4: typing.Optional[str]
    api_server_vrrp_ip: typing.Optional[str]
    api_server_frontend_ip: typing.Optional[str]
    subnet_v6: typing.Optional[str]
    api_server_vrrp_ip6: typing.Optional[str]
    api_server_frontend_ip6: typing.Optional[str]
    # mapping of hostname -> (ipv4, ipv6)
    active_nodes: typing.Mapping[str, NodeInfo]
