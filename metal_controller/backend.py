import abc
import dataclasses
import typing

from . import deploy, model


class Action(metaclass=abc.ABCMeta):
    KIND: str

    @abc.abstractmethod
    def apply_to(self, conn, ironic_node):
        pass

    def to_json(self) -> typing.Mapping:
        result = {
            "kind": self.KIND,
        }
        for field in dataclasses.fields(self):
            result[field.name] = getattr(self, field.name)
        return result

    @classmethod
    def _from_json(cls, data: typing.Mapping) -> "Action":
        kwargs = {
            k: v
            for k, v in data.items()
            if k != "kind"
        }
        return cls(**kwargs)

    @classmethod
    def from_json(self, data: typing.Mapping) -> "Action":
        classes: typing.List[typing.Type[Action]] = [
                PassAction,
                IgnoreAction,
                ManageAction,
                ErrorAction,
            ]
        classmap: typing.Dict[str, typing.Type[Action]] = {
            cls.KIND: cls
            for cls in classes
        }
        kind = data["kind"]
        impl = classmap[kind]
        return impl._from_json(data)


@dataclasses.dataclass
class ManageAction(Action):
    KIND = "manage"

    name: str
    driver: str
    driver_info: typing.Dict[str, object]
    description: str
    backend_node_id: str
    metal_controller_deployment_mode: model.MetalControllerDeployMethod
    config_context: typing.Mapping

    def apply_to(self, conn, ironic_node):
        try:
            caps = [
                cap
                for cap
                in ironic_node.properties.get("capabilities").split(",")
                if not cap.startswith("boot_mode:")
            ]
        except KeyError:
            caps = []
        except AttributeError:
            caps = []

        caps.append("boot_mode:uefi")

        properties = dict(ironic_node.properties or {})
        properties["capabilities"] = ",".join(caps)

        driver_interface = {}
        if self.driver == "redfish":
            driver_interface["power_interface"] = "redfish"
            driver_interface["management_interface"] = "redfish"
            driver_interface["vendor_interface"] = "redfish"

        if {"deploy_kernel", "deploy_ramdisk"} <= self.config_context.keys():
            self.driver_info["deploy_kernel"] = \
                self.config_context["deploy_kernel"]
            self.driver_info["deploy_ramdisk"] = \
                self.config_context["deploy_ramdisk"]

        if "driver_info_override" in self.config_context.keys():
            driver_info_override = self.config_context.get("driver_info_override")
            if isinstance(driver_info_override, dict):
                self.driver_info.update(driver_info_override)
            else:
                self.description += (
                    " driver_info_override was skipped due to not being a proper dict"
                )

        conn.baremetal.update_node(
            ironic_node.id,
            driver=self.driver,
            driver_info=self.driver_info,
            extra=model.update_extra_dict(
                ironic_node.extra,
                backend_node_id=str(self.backend_node_id),
                metal_controller_deployment_mode=self.metal_controller_deployment_mode,  # noqa:E501
                intended_state="available",
            ),
            deploy_interface="direct",
            properties=properties,
            name=self.name,
            description=self.description,
            boot_interface="ipxe",
            **driver_interface
        )

        if ironic_node.provision_state == "manageable":
            conn.baremetal.set_node_provision_state(
                ironic_node.id,
                target="provide",
            )
        else:
            conn.baremetal.set_node_provision_state(
                ironic_node.id,
                target="manage",
            )


@dataclasses.dataclass
class PassAction(Action):
    KIND = "pass"

    backend_node_id: typing.Optional[str]
    message: typing.Optional[str] = None

    def apply_to(self, conn, ironic_node):
        model.update_extra_attributes(
            conn,
            ironic_node,
            backend_node_id=self.backend_node_id,
            last_message=self.message,
        )


@dataclasses.dataclass
class IgnoreAction(Action):
    KIND = "ignore"

    backend_node_id: typing.Optional[str]

    def apply_to(self, conn, ironic_node):
        pass


@dataclasses.dataclass
class ErrorAction(Action):
    KIND = "error"

    message: str
    backend_node_id: typing.Optional[str]

    def apply_to(self, conn, ironic_node):
        model.update_extra_attributes(
            conn,
            ironic_node,
            backend_node_id=self.backend_node_id,
            last_message=self.message,
        )


@dataclasses.dataclass
class DeployAction(Action):
    KIND = "deploy"

    backend_node_id: str

    # nocloud-compatible netplan subset to configure the network of the node
    network_config: typing.Mapping

    # Unique identifier for the cluster. This is used by the frontend to
    # select the cluster repository and other cluster-specific information.
    cluster: str

    cluster_info: model.ClusterInfo | None

    # Extra host vars for this node
    # Use sparingly, if at all. The only valid use case is assigning a VRRP
    # priority.
    host_vars: typing.Mapping

    # Configure whether the node is a k8s control plane node
    is_control_plane: bool

    metal_controller_deployment_mode: model.MetalControllerDeployMethod

    hostname: str

    primary_ipv4: typing.Optional[str]
    primary_ipv6: typing.Optional[str]

    config_context: typing.Mapping

    def apply_to(self, conn, ironic_node):
        plan = deploy.plan_node(
            cluster=self.cluster,
            cluster_info=self.cluster_info,
            uuid=str(ironic_node.id),
            hostname=ironic_node.name,
            primary_ipv4=self.primary_ipv4,
            primary_ipv6=self.primary_ipv6,
            is_control_plane=self.is_control_plane,
            host_vars=self.host_vars,
            network_config=self.network_config,
            metal_controller_deployment_mode=self.metal_controller_deployment_mode,   # noqa:E501
            config_context=self.config_context
        )

        conn.baremetal.update_node(
            ironic_node.id,
            instance_info={
                "image_source": plan.image_source,
                "image_checksum": plan.image_checksum,
            },
            extra=model.update_extra_dict(
                ironic_node.extra,
                backend_node_id=str(self.backend_node_id),
                metal_controller_deployment_mode=self.metal_controller_deployment_mode,  # noqa:E501
                intended_state="active",
            ),
        )
        conn.baremetal.set_node_provision_state(
            ironic_node.id,
            target="active",
            config_drive=plan.configdrive,
        )


class Backend(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def get_cluster_info(self, cluster: str) -> model.ClusterInfo:
        """
        Return information about a cluster.

        This will be called with the value of :attr:`DeployAction.cluster`
        whenever a node is being deployed and potentially on further life-cycle
        management activities.
        """

    @abc.abstractmethod
    def enroll(
            self,
            node: model.NodeBrief,
            introspection_data: typing.Mapping,
            ) -> Action:
        """
        Return an action for a node which is in enroll state.

        This is called once for each node in enroll state on each full sync,
        unless the node has been ignored before.
        """

    @abc.abstractmethod
    def list_sync(self, nodes: typing.Collection[model.NodeBrief]) -> None:
        pass

    @abc.abstractmethod
    def manage(
            self,
            node: model.NodeBrief,
            introspection_data: typing.Mapping,
            ) -> Action:
        pass

    @abc.abstractmethod
    def failure(self, node: model.NodeBrief, error: str) -> None:
        """
        Report a failed action on a node.

        This is called when a node action fails asynchronously. The error is
        the `last_error` as reported by Ironic.
        """

    @abc.abstractmethod
    def success(self, node: model.NodeBrief) -> None:
        """
        Report a succeded action on a node.

        This is called when a node action completes asynchronously. It may or
        may not be followed immediately by a call to :meth:`manage` or
        :meth:`enroll`.
        """
