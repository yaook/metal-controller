import logging
import environ

from . import netbox, daemon


@environ.config(prefix="METAL_CONTROLLER")
class MetalControllerConfig:
    reconcile_interval = environ.var(converter=int, default=5)


def main() -> int:
    logging.basicConfig(level=logging.WARNING)
    logger = logging.getLogger("metal_controller")
    logger.setLevel(logging.DEBUG)
    backend = netbox.NetBoxBackend(logger=logger.getChild("backend"))
    config = environ.to_config(MetalControllerConfig)
    d = daemon.Daemon(
        interval=config.reconcile_interval,
        backend=backend,
        logger=logger.getChild("daemon"),
    )
    d.run()
    return 0
