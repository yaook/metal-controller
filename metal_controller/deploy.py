import dataclasses
import io
import logging
import json
import os
import pathlib
import subprocess
import sys
import typing

import yaml

import environ

from . import vault, configdrive, model


logger = logging.getLogger(__name__)


def check_cluster_map(_instance, _attribute, value):
    if (not isinstance(value, dict) or
        not all(isinstance(k, str) and isinstance(v, str)
                for k, v in value.items())):
        raise ValueError("cluster map must be key-value pairs encoded as YAML")


def keylist(s: str) -> typing.Sequence[str]:
    return s.split("\n")


@environ.config(prefix="YAOOK")
class ClusterConfig:
    cluster_map = environ.var(converter=yaml.safe_load,
                              validator=check_cluster_map)

    deploy_public_keys = environ.var(converter=keylist)

    deploy_image = environ.var()
    deploy_image_checksum = environ.var()

    deploy_configdrive_debug = environ.bool_var()

    deploy_vault_addr = environ.var("")

    apt_proxy = environ.var(None)
    apt_hashicorp_gpg_key = environ.var(None)
    apt_hashicorp_repo_url = environ.var(None)

    pip_mirror_url = environ.var(None)
    pip_mirror_cacert = environ.var(None)

    ansible_collections_preload = environ.var(None)
    ansible_collections_preload_ca = environ.var(None)


@dataclasses.dataclass(frozen=True)
class NodePlan:
    image_source: str
    image_checksum: str
    configdrive: str


def _write_inventory(
        out: io.TextIOBase,
        nodes: typing.Mapping[str, model.NodeInfo]) -> None:
    groups: typing.Dict[str, typing.List[str]] = {
        "all:vars": [
            "on_openstack=False",
        ],
        "orchestrator": [
            "localhost ansible_connection=local "
            "ansible_python_interpreter=\"{{ ansible_playbook_python }}\""
        ],
        "k8s_nodes:children": ["masters", "workers"],
        "frontend:children": ["masters", "gateways"],
        "gateways": [],
        "masters": [],
        "workers": [],
    }

    any_have_ipv4 = False
    any_have_ipv6 = False
    all_have_ipv4 = True
    all_have_ipv6 = True

    for node_name, node_info in nodes.items():
        parts = [node_name, f"ansible_host={node_info.ipv4 or node_info.ipv6}"]
        if node_info.ipv4 is not None:
            parts.append(f"local_ipv4_address={node_info.ipv4}")
            any_have_ipv4 = True
        else:
            all_have_ipv4 = False
        if node_info.ipv6 is not None:
            parts.append(f"local_ipv6_address={node_info.ipv6}")
            any_have_ipv6 = True
        else:
            all_have_ipv6 = False
        parts.append("ansible_user=ubuntu")
        line = " ".join(parts)
        group_name = "masters" if node_info.is_control_plane else "workers"
        groups[group_name].append(line)

    if any_have_ipv4 != all_have_ipv4:
        logger.warn("Only some nodes have a primary IPv4 address assigned. "
                    "We will still assume that the cluster overall has IPv4 "
                    "support, but stuff may break.")

    if any_have_ipv6 != all_have_ipv6:
        logger.warn("Only some nodes have a primary IPv6 address assigned. "
                    "We will still assume that the cluster overall has IPv6 "
                    "support, but stuff may break.")

    if not any_have_ipv4 and not any_have_ipv6:
        raise ValueError(
            "No node in inventory has an IPv4 or an IPv6 address. "
            "We cannot fathom how that is a valid configuration, so we bail "
            "out here."
        )

    groups["all:vars"].append(f"ipv4_enabled={any_have_ipv4}")
    groups["all:vars"].append(f"ipv6_enabled={any_have_ipv6}")

    print(file=out)
    for group, entries in groups.items():
        print(f"[{group}]", file=out)
        for entry in entries:
            print(entry, file=out)
        print(file=out)


def _reset_cluster_repository(cluster_repository: str) -> None:
    """
    Hard-reset the repository to the latest remote state of the selected
    branch.
    """
    try:
        remote_ref = subprocess.check_output(
            ["git", "rev-parse",
             "--abbrev-ref",
             "--symbolic-full-name",
             "@{u}"],
            cwd=cluster_repository,
        ).strip().decode("utf-8")
    except subprocess.CalledProcessError as exc:
        if exc.returncode != 128:
            # unexpected error, bail out
            raise
        subprocess.check_call(
            ["git", "checkout", "."],
            cwd=cluster_repository,
        )
    else:
        remote_name, _ = remote_ref.split("/", 1)
        try:
            subprocess.check_call(
                ["git", "fetch", remote_name],
                cwd=cluster_repository,
            )
        except subprocess.CalledProcessError as exc:
            if exc.returncode != 128:
                raise
            # ignore failing to fetch from upstream; it's very well possible
            # that it is down because WE NEED TO DEPLOY THIS NODE ASAP TO GET
            # IT FIXED

        subprocess.check_call(
            ["git", "reset", "--hard", remote_ref],
            cwd=cluster_repository,
        )

        try:
            subprocess.check_call(
                ["git", "submodule", "update", "--init", "--recursive"],
                cwd=cluster_repository,
            )
        except subprocess.CalledProcessError as exc:
            if exc.returncode != 128:
                raise
            # ignore failing to fetch from upstream; it's very well possible
            # that it is down because WE NEED TO DEPLOY THIS NODE ASAP TO GET
            # IT FIXED

    subprocess.check_call(
        ["git", "clean", "-f", "."],
        cwd=cluster_repository,
    )


def _write_extra_vars(
        out: typing.IO[str],
        cluster: str,
        cluster_info: model.ClusterInfo,
        ) -> None:
    extra_vars: typing.Dict[str, typing.Union[str, bool]] = {}
    if cluster_info.subnet_v4 is not None:
        extra_vars["subnet_cidr"] = cluster_info.subnet_v4
    if cluster_info.api_server_vrrp_ip is not None:
        extra_vars["networking_fixed_ip"] = cluster_info.api_server_vrrp_ip
    if cluster_info.api_server_frontend_ip is not None:
        extra_vars["networking_floating_ip"] = \
            cluster_info.api_server_frontend_ip
    if cluster_info.subnet_v6 is not None:
        extra_vars["subnet_v6_cidr"] = cluster_info.subnet_v6
    if cluster_info.api_server_vrrp_ip6 is not None:
        extra_vars["networking_fixed_ip_v6"] = cluster_info.api_server_vrrp_ip6
    if cluster_info.api_server_frontend_ip6 is not None:
        extra_vars["networking_floating_ip_v6"] = \
            cluster_info.api_server_frontend_ip6

    json.dump(
        extra_vars,
        out,
    )


def prepare_cluster_repository(
        cluster: str,
        cluster_repository: str,
        cluster_info: model.ClusterInfo) -> None:
    """
    Write the current inventory file for a cluster repository, as well as
    cluster-wide configuration controlled by the backend.
    """
    _reset_cluster_repository(cluster_repository)

    subprocess.check_call(
        [sys.executable, "managed-k8s/actions/update_inventory.py"],
        cwd=cluster_repository,
    )

    repo_path = pathlib.Path(cluster_repository)
    inventory_dir = repo_path / "inventory" / "yaook-k8s"
    inventory_dir.mkdir(parents=True, exist_ok=True)
    with (inventory_dir / "hosts").open("w") as f:
        _write_inventory(f, cluster_info.active_nodes)

    cfg_dir = inventory_dir / "group_vars" / "all"
    cfg_dir.mkdir(parents=True, exist_ok=True)
    with (cfg_dir / "terraform_metal-controller.json").open("w") as f:
        _write_extra_vars(f, cluster, cluster_info)


def plan_node(
        cluster: str,
        cluster_info: model.ClusterInfo,
        uuid: str,
        hostname: str,
        primary_ipv4: typing.Optional[str],
        primary_ipv6: typing.Optional[str],
        is_control_plane: bool,
        metal_controller_deployment_mode: model.MetalControllerDeployMethod,
        host_vars: typing.Mapping,
        network_config: typing.Mapping,
        config_context: typing.Mapping,
        ) -> NodePlan:
    config = environ.to_config(ClusterConfig)

    user_data = configdrive.assemble_user_data(
        configdrive.generate_base_script(metal_controller_deployment_mode),
        {},
    )

    match metal_controller_deployment_mode:
        case model.MetalControllerDeployMethod.YAOOK_K8S:
            cluster_repository = config.cluster_map[cluster]

            logger.debug("using cluster repository at %r for cluster %r to "
                         "deploy %r (%s)",
                         cluster_repository, cluster, hostname, uuid)

            prepare_cluster_repository(
                cluster,
                cluster_repository,
                cluster_info
            )

            deploy_script = configdrive.assemble_deploy_script(
                configdrive.build_tarball(
                    cluster_repository=cluster_repository,
                ),
            )
            if is_control_plane:
                node_role = "control-plane"
            else:
                node_role = "worker"
        case model.MetalControllerDeployMethod.NO_ADDITIONAL_DEPLOYMENT:
            deploy_script = bytes("", "ascii")
            node_role = "generic"
        case model.MetalControllerDeployMethod.TEMPLATE_BASE:
            node_role = "generic"
            deploy_script = bytes("", "ascii")

    policies = []
    if is_control_plane:
        policies.append("k8s-control-plane")
    policies.extend(["k8s-node", "node"])

    logger.debug("configuring server %r in vault (policies: %r)",
                 hostname, policies)

    role_id, wrapping_token = vault.create_server(
        cluster,
        hostname,
        policies,
        primary_ipv4,
        primary_ipv6,
    )

    logger.debug("server %r is using role %r", hostname, role_id)

    try:
        vault_cacert = os.environ["VAULT_CACERT"]
    except KeyError:
        vault_cacert = os.environ["VAULT_CAPATH"]
        logger.warn("VAULT_CAPATH is set instead of VAULT_CACERT. "
                    "Please update the metal-controller pod spec to set "
                    "VAULT_CACERT instead.")

    meta_data = configdrive.assemble_meta_data(
        cluster=cluster,
        uuid=uuid,
        hostname=hostname,
        vault_addr=config.deploy_vault_addr or os.environ["VAULT_ADDR"],
        vault_cacert=pathlib.Path(vault_cacert).read_text(),
        public_keys=config.deploy_public_keys,
        skip_tags=[] if is_control_plane else ["control-plane"],
        vault_role_id=role_id,
        vault_wrapped_secret_id=wrapping_token,
        node_role=node_role,
        apt_proxy=config.apt_proxy,
        apt_hashicorp_gpg_key=config.apt_hashicorp_gpg_key,
        apt_hashicorp_repo_url=config.apt_hashicorp_repo_url,
        pip_mirror_url=config.pip_mirror_url,
        pip_mirror_cacert=config.pip_mirror_cacert,
        ansible_collections_preload=config.ansible_collections_preload,
        ansible_collections_preload_ca=config.ansible_collections_preload_ca,
        netbox_context_generic_environment=config_context.get(
            "custom_environment", {})
    )

    drive = configdrive.wrap_tarball(configdrive.assemble_configdrive(
        meta_data=meta_data,
        network_config=network_config,
        user_data=user_data,
        deploy_script=deploy_script
    ))

    logger.debug("using %d bytes of base64-encoded gzip'd configdrive",
                 len(drive))

    if config.deploy_configdrive_debug:
        outfile = pathlib.Path(cluster_repository) / "configdrive.iso.gz.b64"
        logger.debug("dumping configdrive to %s", outfile)
        with outfile.open("w") as f:
            assert f.write(drive) == len(drive)
        logger.info("dumped configdrive to %s", outfile)

    if "image_source" in config_context:
        deploy_image = config_context["image_source"]
    else:
        deploy_image = config.deploy_image
    logger.debug(f"using image_source {deploy_image} for node {uuid}")

    if "image_checksum" in config_context:
        deploy_image_checksum = config_context["image_checksum"]
    else:
        deploy_image_checksum = config.deploy_image_checksum
    logger.debug(f"using image_checksum {deploy_image_checksum}"
                 f" for node {uuid}")

    return NodePlan(
        configdrive=drive,
        image_source=deploy_image,
        image_checksum=deploy_image_checksum,
    )
