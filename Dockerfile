FROM python:3.12-slim-bookworm@sha256:5f41dbed222db6466292bf588f85bc3d21ccc460b181a5f5002cdc4e608359be

ARG BUILD_PACKAGES="build-essential gcc python3-dev"

RUN set -eu; \
    apt-get update; \
    DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade -y; \
    DEBIAN_FRONTEND=noninteractive apt-get install --no-install-suggests --no-install-recommends -y python3 python3-pip tini git python3-toml genisoimage vim gcc openssh-client $BUILD_PACKAGES

COPY --chmod=700 metal_controller /app/metal_controller
COPY --chmod=700 requirements.txt /app/requirements.txt

RUN set -eu; \
    cd /app; \
    pip3 install -r requirements.txt --break-system-packages; \
    DEBIAN_FRONTEND=noninteractive apt-get purge --autoremove -y $BUILD_PACKAGES; \
    apt-get clean autoclean; \
    rm -rf /var/lib/{apt,dpkg,cache,log}/

COPY --chmod=700 docker/entrypoint.sh /entrypoint.sh
COPY --chmod=700 docker/configdrive /docker-run/configdrive
COPY --chmod=700 docker/metal_controller /docker-run/metal_controller
COPY --chmod=700 utils/mkcluster.sh /docker-run/mkcluster

ENV WG_USAGE=false
ENV TF_USAGE=false

ENTRYPOINT ["/entrypoint.sh"]
CMD ["metal_controller"]
