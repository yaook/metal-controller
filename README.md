# metal-controller

## Synopsis

The metal-controller is glue code between OpenStack Ironic, NetBox and HashiCorp Vault to orchestrate automated deployment of nodes into a Yaook bare metal Kubernetes cluster.

## Configuration

<span style="text-decoration:line-through;">
In the spirit of [12-factor apps](https://12factor.net/), metal-controller is exclusively configured via environment variables. For a complete reference please see [`docs/config.md`](docs/config.md).</span>

Configuration of the MetalController is split into `environment variables`, for the base configuration of the MetalController, and into `config context` options, which reside within Netbox.
The latter allows users to use node specific options, like different images or different CPU architectures.


## Running

Docker images are provided in [this repositories docker container registry](https://gitlab.com/yaook/metal-controller/container_registry/2371024).

## Deployment methods

- `yaook/k8s based`: default deployment method and the reason why, the MetalController has been created. This deploys a pre-configured `yaook/k8s` cluster-repo to Ironic managed nodes
- `Templated based`: a node will be deployed and executes afterwards a Bash script on that node. Basically like a cloud-init based deployment, but with a bit more options due to the configurable Bash script
- `Generic`: deploys a node with the configured OS image node and nothing else
