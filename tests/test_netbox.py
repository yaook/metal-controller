import contextlib
import functools
import itertools
import logging
import socket
import unittest
import unittest.mock
from unittest.mock import sentinel

import ddt

import pynetbox.core.query

import metal_controller.backend as backend
import metal_controller.netbox as netbox
import metal_controller.model as model


def _as_generator(items):
    yield from items


def _mk_vlan_shim(vid):
    vlan = unittest.mock.Mock([])
    vlan.id = 10000 + vid
    vlan.vid = vid
    return vlan


class Test_merge_dump(unittest.TestCase):
    def test__merge_dump_on_empty_string_adds_markers_and_content(self):
        result = netbox._merge_dump(
            "",
            "foobar\nbaz\nfnord",
        )

        self.assertEqual(
            result,
            "\n\n<!-- BEGIN OF METAL CONTROLLER DUMP -->\n\nfoobar\nbaz\nfnord"
            "\n\n<!-- END OF METAL CONTROLLER DUMP -->"
        )

    def test__merge_dump_appends_to_existing_content(self):
        result = netbox._merge_dump(
            "preexisting\nother\nstuff\n\n",
            "foobar\nbaz\nfnord",
        )

        self.assertEqual(
            result,
            "preexisting\nother\nstuff\n\n"
            "\n\n<!-- BEGIN OF METAL CONTROLLER DUMP -->\n\nfoobar\nbaz\nfnord"
            "\n\n<!-- END OF METAL CONTROLLER DUMP -->"
        )

    def test__merge_dump_replaces_existing_block(self):
        result = netbox._merge_dump(
            "preexisting\nother\nstuff\n\n"
            "\n\n<!-- BEGIN OF METAL CONTROLLER DUMP -->\n\nsome\nold\nstuff"
            "\n\n<!-- END OF METAL CONTROLLER DUMP -->\n\n"
            "some trailing content\n",
            "new\ncontent"
        )

        self.assertEqual(
            result,
            "preexisting\nother\nstuff\n\n"
            "\n\n<!-- BEGIN OF METAL CONTROLLER DUMP -->\n\nnew\ncontent"
            "\n\n<!-- END OF METAL CONTROLLER DUMP -->\n\n"
            "some trailing content",
        )

    def test__merge_dump_replaces_existing_even_with_stripped_newlines(self):
        result = netbox._merge_dump(
            "preexisting\nother\nstuff\n\n"
            "\n\n<!-- BEGIN OF METAL CONTROLLER DUMP -->\n\nsome\nold\nstuff"
            "\n\n<!-- END OF METAL CONTROLLER DUMP -->",
            "new\ncontent"
        )

        self.assertEqual(
            result,
            "preexisting\nother\nstuff\n\n"
            "\n\n<!-- BEGIN OF METAL CONTROLLER DUMP -->\n\nnew\ncontent"
            "\n\n<!-- END OF METAL CONTROLLER DUMP -->",
        )

    def test__merge_dump_replaces_existing_even_with_stripped_leading_newlines(self):  # noqa:E501
        result = netbox._merge_dump(
            "<!-- BEGIN OF METAL CONTROLLER DUMP -->\n\nsome\nold\nstuff"
            "\n\n<!-- END OF METAL CONTROLLER DUMP -->\n\n"
            "some trailing content\n",
            "new\ncontent"
        )

        self.assertEqual(
            result,
            "\n\n<!-- BEGIN OF METAL CONTROLLER DUMP -->\n\nnew\ncontent"
            "\n\n<!-- END OF METAL CONTROLLER DUMP -->\n\n"
            "some trailing content",
        )


class Test_merge_info(unittest.TestCase):
    def test__merge_info_on_empty_string_adds_markers_and_content(self):
        result = netbox._merge_info(
            "",
            "foobar\nbaz\nfnord",
        )

        self.assertEqual(
            result,
            "\n\n<!-- BEGIN OF METAL CONTROLLER INFO -->\n\nfoobar\nbaz\nfnord"
            "\n\n<!-- END OF METAL CONTROLLER INFO -->"
        )

    def test__merge_info_appends_to_existing_content(self):
        result = netbox._merge_info(
            "preexisting\nother\nstuff\n\n",
            "foobar\nbaz\nfnord",
        )

        self.assertEqual(
            result,
            "preexisting\nother\nstuff\n\n"
            "\n\n<!-- BEGIN OF METAL CONTROLLER INFO -->\n\nfoobar\nbaz\nfnord"
            "\n\n<!-- END OF METAL CONTROLLER INFO -->"
        )

    def test__merge_info_replaces_existing_block(self):
        result = netbox._merge_info(
            "preexisting\nother\nstuff\n\n"
            "\n\n<!-- BEGIN OF METAL CONTROLLER INFO -->\n\nsome\nold\nstuff"
            "\n\n<!-- END OF METAL CONTROLLER INFO -->\n\n"
            "some trailing content\n",
            "new\ncontent"
        )

        self.assertEqual(
            result,
            "preexisting\nother\nstuff\n\n"
            "\n\n<!-- BEGIN OF METAL CONTROLLER INFO -->\n\nnew\ncontent"
            "\n\n<!-- END OF METAL CONTROLLER INFO -->\n\n"
            "some trailing content",
        )

    def test__merge_info_replaces_existing_even_with_stripped_trailing_newlines(self):  # noqa:E501
        result = netbox._merge_info(
            "preexisting\nother\nstuff\n\n"
            "\n\n<!-- BEGIN OF METAL CONTROLLER INFO -->\n\nsome\nold\nstuff"
            "\n\n<!-- END OF METAL CONTROLLER INFO -->",
            "new\ncontent"
        )

        self.assertEqual(
            result,
            "preexisting\nother\nstuff\n\n"
            "\n\n<!-- BEGIN OF METAL CONTROLLER INFO -->\n\nnew\ncontent"
            "\n\n<!-- END OF METAL CONTROLLER INFO -->",
        )

    def test__merge_info_replaces_existing_even_with_stripped_leading_newlines(self):  # noqa:E501
        result = netbox._merge_info(
            "<!-- BEGIN OF METAL CONTROLLER INFO -->\n\nsome\nold\nstuff"
            "\n\n<!-- END OF METAL CONTROLLER INFO -->\n\n"
            "some trailing content\n",
            "new\ncontent"
        )

        self.assertEqual(
            result,
            "\n\n<!-- BEGIN OF METAL CONTROLLER INFO -->\n\nnew\ncontent"
            "\n\n<!-- END OF METAL CONTROLLER INFO -->\n\n"
            "some trailing content",
        )


class Test_make_netplan(unittest.TestCase):
    def test__make_netplan_vlans_no_addrs_no_bonding(self):
        client = unittest.mock.Mock()

        iface1 = unittest.mock.Mock()
        iface1.name = "foo-if"
        iface1.mac_address = "AA:BB:CC:DD:EE:FF"
        iface1.untagged_vlan = _mk_vlan_shim(100)
        iface1.tagged_vlans = []
        iface1.mtu = 1337

        iface2 = unittest.mock.Mock()
        iface2.name = "bar-if"
        iface2.mac_address = "12:34:56:78:9a:bc"
        iface2.untagged_vlan = _mk_vlan_shim(200)
        iface2.tagged_vlans = [
            _mk_vlan_shim(201),
            _mk_vlan_shim(202),
        ]
        iface2.mtu = 1337

        client.ipam.ip_addresses.filter.return_value = []

        device = unittest.mock.Mock()
        device.id = sentinel.device_id

        client.dcim.interfaces.filter.return_value = [iface1, iface2]

        result = netbox._make_netplan(
            client,
            device,
            sentinel.gateway_tag,
            sentinel.nameserver_field,
            sentinel.search_domain,
        )

        client.dcim.interfaces.filter.assert_called_once_with(
            device_id=device.id,
            mgmt_only=False,
        )

        self.assertEqual(result["version"], 2)
        self.assertDictEqual(result["ethernets"]["foo-if"], {
            "match": {"macaddress": "aa:bb:cc:dd:ee:ff"},
            "set-name": "foo-if",
            "link-local": [],
            "mtu": 1337
        })
        self.assertDictEqual(result["ethernets"]["bar-if"], {
            "match": {"macaddress": "12:34:56:78:9a:bc"},
            "set-name": "bar-if",
            "link-local": [],
            "mtu": 1337
        })
        self.assertDictEqual(result["vlans"]["bar-if.201"], {
            "id": 201,
            "link": "bar-if",
            "link-local": [],
            "mtu": 1337
        })
        self.assertDictEqual(result["vlans"]["bar-if.202"], {
            "id": 202,
            "link": "bar-if",
            "link-local": [],
            "mtu": 1337
        })

    def test__make_netbox_longest_prefix_match(self):
        prefixes = [
            "10.0.0.0/15",
            "10.0.0.0/16",
            "10.0.1.0/24",
            "10.0.1.0/27"
        ]
        self.assertEqual(netbox._netbox_longest_prefix_match(prefixes),
                         "10.0.1.0/27")

    def test__make_netbox_multiple_prefixes(self):
        client = unittest.mock.Mock()

        iface1 = unittest.mock.Mock()
        iface1.id = sentinel.iface1_id
        iface1.name = "foo-if"
        iface1.mac_address = "AA:BB:CC:DD:EE:FF"
        iface1.untagged_vlan = _mk_vlan_shim(100)
        iface1.tagged_vlans = [_mk_vlan_shim(101)]
        iface1.mtu = 1337

        common_vrf_id = sentinel.vrf_id

        addr1 = unittest.mock.Mock()
        addr1.address = "10.1.0.1/16"
        addr1.vrf.id = common_vrf_id

        prefix1 = unittest.mock.Mock()
        prefix1.vlan = _mk_vlan_shim(101)
        prefix1.prefix = "10.1.0.0/16"
        prefix1.custom_fields = {"yaook_nameserver": None}
        prefix1.custom_fields = {"yaook_kubernetes_node": True}

        addrs = {
            sentinel.iface1_id: [
                addr1,
            ]
        }

        def ip_addresses_impl(**kwargs):
            try:
                interface_id = kwargs["interface_id"]
                return _as_generator(addrs.get(interface_id, []))
            except KeyError:
                return _as_generator([])

        def match_prefixes_impl(*, contains, vrf_id, status, **kwargs):
            self.assertEqual(status, "active")
            self.assertEqual(vrf_id, common_vrf_id)
            return ["10.0.0.0/8", "10.1.0.0/9", "10.1.0.0/16"]

        def prefixes_impl(*, prefix, vrf_id, status, **kwargs):
            self.assertEqual(status, "active")
            self.assertEqual(vrf_id, common_vrf_id)
            self.assertEqual(prefix, "10.1.0.0/16")
            return prefix1

        client.ipam.ip_addresses.filter.side_effect = ip_addresses_impl
        client.ipam.prefixes.filter.side_effect = match_prefixes_impl
        client.ipam.prefixes.get.side_effect = prefixes_impl

        device = unittest.mock.Mock()
        device.id = sentinel.device_id

        client.dcim.interfaces.filter.return_value = [iface1]

        result = netbox._make_netplan(
            client,
            device,
            sentinel.gateway_tag,
            sentinel.nameserver_field,
            sentinel.search_domain,
        )

        client.dcim.interfaces.filter.assert_called_once_with(
            device_id=device.id,
            mgmt_only=False,
        )

        self.assertDictEqual(result["vlans"]["foo-if.101"], {
            'addresses': ['10.1.0.1/16'],
            'id': 101,
            'link': 'foo-if',
            'link-local': [],
            "mtu": 1337}
        )

    def test__make_netplan_vlans_v4_singlestack_no_bonding(self):
        client = unittest.mock.Mock()

        iface1 = unittest.mock.Mock()
        iface1.id = sentinel.iface1_id
        iface1.name = "foo-if"
        iface1.mac_address = "AA:BB:CC:DD:EE:FF"
        iface1.untagged_vlan = _mk_vlan_shim(100)
        iface1.tagged_vlans = []
        iface1.mtu = 1337

        iface2 = unittest.mock.Mock()
        iface2.id = sentinel.iface2_id
        iface2.name = "bar-if"
        iface2.mac_address = "12:34:56:78:9a:bc"
        iface2.untagged_vlan = _mk_vlan_shim(200)
        iface2.tagged_vlans = [
            _mk_vlan_shim(201),
            _mk_vlan_shim(202),
        ]
        iface2.mtu = 1337

        common_vrf_id = sentinel.vrf_id

        addr1 = unittest.mock.Mock()
        addr1.address = "10.1.0.1/16"
        addr1.vrf.id = common_vrf_id

        addr2 = unittest.mock.Mock()
        addr2.address = "10.2.0.1/16"
        addr2.vrf.id = common_vrf_id

        addr3 = unittest.mock.Mock()
        addr3.address = "10.3.0.1/16"
        addr3.vrf.id = common_vrf_id

        addr4 = unittest.mock.Mock()
        addr4.address = "10.4.0.1/16"
        addr4.vrf.id = common_vrf_id

        addr5 = unittest.mock.Mock()
        addr5.address = "10.1.0.2/16"
        addr5.vrf.id = common_vrf_id

        prefix1 = unittest.mock.Mock()
        prefix1.vlan = _mk_vlan_shim(100)
        prefix1.prefix = "10.1.0.0/16"
        prefix1.custom_fields = {"yaook_nameserver": None}

        prefix2 = unittest.mock.Mock()
        prefix2.vlan = _mk_vlan_shim(200)
        prefix2.prefix = "10.2.0.0/16"
        prefix2.custom_fields = {"yaook_nameserver": None}

        prefix3 = unittest.mock.Mock()
        prefix3.vlan = _mk_vlan_shim(201)
        prefix3.prefix = "10.3.0.0/16"
        prefix3.custom_fields = {"yaook_nameserver": None}

        prefix4 = unittest.mock.Mock()
        prefix4.vlan = _mk_vlan_shim(202)
        prefix4.prefix = "10.4.0.0/16"
        prefix4.custom_fields = {"yaook_nameserver": None}

        addrs = {
            sentinel.iface1_id: [
                addr1,
                addr5,
            ],
            sentinel.iface2_id: [
                addr4,
                addr2,
                addr3,
            ],
        }

        prefixes = {
            "10.1.0.1/16": prefix1,
            "10.1.0.2/16": prefix1,
            "10.2.0.1/16": prefix2,
            "10.3.0.1/16": prefix3,
            "10.4.0.1/16": prefix4,
        }

        def ip_addresses_impl(**kwargs):
            try:
                interface_id = kwargs["interface_id"]
                return _as_generator(addrs.get(interface_id, []))
            except KeyError:
                return _as_generator([])

        def match_prefixes_impl(*, contains, vrf_id, status, **kwargs):
            self.assertEqual(status, "active")
            self.assertEqual(vrf_id, common_vrf_id)
            return [contains]

        def prefixes_impl(*, prefix, vrf_id, status, **kwargs):
            self.assertEqual(status, "active")
            self.assertEqual(vrf_id, common_vrf_id)
            return prefixes[prefix]

        client.ipam.ip_addresses.filter.side_effect = ip_addresses_impl
        client.ipam.prefixes.filter.side_effect = match_prefixes_impl
        client.ipam.prefixes.get.side_effect = prefixes_impl

        device = unittest.mock.Mock()
        device.id = sentinel.device_id

        client.dcim.interfaces.filter.return_value = [iface1, iface2]

        result = netbox._make_netplan(
            client,
            device,
            sentinel.gateway_tag,
            sentinel.nameserver_field,
            sentinel.search_domain,
        )

        client.dcim.interfaces.filter.assert_called_once_with(
            device_id=device.id,
            mgmt_only=False,
        )

        self.assertIn(
            unittest.mock.call(interface_id=sentinel.iface1_id),
            client.ipam.ip_addresses.filter.mock_calls,
        )

        self.assertIn(
            unittest.mock.call(interface_id=sentinel.iface2_id),
            client.ipam.ip_addresses.filter.mock_calls,
        )

        self.assertEqual(
            5,
            len(client.ipam.prefixes.get.mock_calls),
        )

        self.assertEqual(result["version"], 2)
        self.assertDictEqual(result["ethernets"]["foo-if"], {
            "match": {"macaddress": "aa:bb:cc:dd:ee:ff"},
            "set-name": "foo-if",
            "addresses": ["10.1.0.1/16", "10.1.0.2/16"],
            "link-local": [],
            "mtu": 1337,
        })
        self.assertDictEqual(result["ethernets"]["bar-if"], {
            "match": {"macaddress": "12:34:56:78:9a:bc"},
            "set-name": "bar-if",
            "addresses": ["10.2.0.1/16"],
            "link-local": [],
            "mtu": 1337,
        })
        self.assertDictEqual(result["vlans"]["bar-if.201"], {
            "id": 201,
            "link": "bar-if",
            "addresses": ["10.3.0.1/16"],
            "link-local": [],
            "mtu": 1337,
        })
        self.assertDictEqual(result["vlans"]["bar-if.202"], {
            "id": 202,
            "link": "bar-if",
            "addresses": ["10.4.0.1/16"],
            "link-local": [],
            "mtu": 1337,
        })

    def test__make_netplan_vlans_dualstack_bonding(self):
        client = unittest.mock.Mock()

        iface1 = unittest.mock.Mock()
        iface1.id = sentinel.iface1_id
        iface1.name = "foo-if"
        iface1.mac_address = "AA:BB:CC:DD:EE:FF"
        iface1.untagged_vlan = _mk_vlan_shim(100)
        iface1.tagged_vlans = []
        iface1.mtu = 1337

        iface2 = unittest.mock.Mock()
        iface2.id = sentinel.iface2_id
        iface2.name = "bar-if"
        iface2.mac_address = "12:34:56:78:9a:bc"
        iface2.untagged_vlan = _mk_vlan_shim(200)
        iface2.tagged_vlans = [
            _mk_vlan_shim(201),
            _mk_vlan_shim(202),
        ]
        iface2.type.value = "lag"
        iface2.custom_fields = {
            "bond_mode": "802.3ad",
            "bond_hash": "layer3+4"
        }
        iface2.mtu = 1337

        iface3 = unittest.mock.Mock()
        iface3.id = sentinel.iface3_id
        iface3.name = "phy-if1"
        iface3.untagged_vlan = None
        iface3.tagged_vlans = []
        iface3.mtu = 1337

        iface4 = unittest.mock.Mock()
        iface4.id = sentinel.iface4_id
        iface4.name = "phy-if2"
        iface4.untagged_vlan = None
        iface4.tagged_vlans = []
        iface4.mtu = 1337

        common_vrf_id = sentinel.vrf_id

        addr1 = unittest.mock.Mock()
        addr1.address = "10.1.0.1/16"
        addr1.vrf.id = common_vrf_id

        addr2 = unittest.mock.Mock()
        addr2.address = "10.2.0.1/16"
        addr2.vrf.id = common_vrf_id

        addr3 = unittest.mock.Mock()
        addr3.address = "10.3.0.1/16"
        addr3.vrf.id = common_vrf_id

        addr4 = unittest.mock.Mock()
        addr4.address = "10.4.0.1/16"
        addr4.vrf.id = common_vrf_id

        addr5 = unittest.mock.Mock()
        addr5.address = "2001:db8::1/64"
        addr5.vrf.id = common_vrf_id
        addr5.family.value = 6

        prefix1 = unittest.mock.Mock()
        prefix1.vlan = _mk_vlan_shim(100)
        prefix1.prefix = "10.1.0.0/16"
        prefix1.custom_fields = {"yaook_nameserver": None}

        prefix2 = unittest.mock.Mock()
        prefix2.vlan = _mk_vlan_shim(200)
        prefix2.prefix = "10.2.0.0/16"
        prefix2.custom_fields = {"yaook_nameserver": None}

        prefix3 = unittest.mock.Mock()
        prefix3.vlan = _mk_vlan_shim(201)
        prefix3.prefix = "10.3.0.0/16"
        prefix3.custom_fields = {"yaook_nameserver": None}

        prefix4 = unittest.mock.Mock()
        prefix4.vlan = _mk_vlan_shim(202)
        prefix4.prefix = "10.4.0.0/16"
        prefix4.custom_fields = {"yaook_nameserver": None}

        prefix5 = unittest.mock.Mock()
        prefix5.vlan = _mk_vlan_shim(100)
        prefix5.prefix = "2001:db8::/64"
        prefix5.custom_fields = {"yaook_nameserver": None}

        addrs = {
            sentinel.iface1_id: [
                addr1,
                addr5,
            ],
            sentinel.iface2_id: [
                addr4,
                addr2,
                addr3,
            ],
            sentinel.iface3_id: [],
            sentinel.iface4_id: [],
        }

        prefixes = {
            "10.1.0.1/16": prefix1,
            "2001:db8::1/64": prefix5,
            "10.2.0.1/16": prefix2,
            "10.3.0.1/16": prefix3,
            "10.4.0.1/16": prefix4,
        }

        def ip_addresses_impl(**kwargs):
            try:
                interface_id = kwargs["interface_id"]
                return _as_generator(addrs.get(interface_id, []))
            except KeyError:
                return _as_generator([])

        def match_prefixes_impl(*, contains, vrf_id, status, **kwargs):
            self.assertEqual(status, "active")
            self.assertEqual(vrf_id, common_vrf_id)
            return [contains]

        def prefixes_impl(*, prefix, vrf_id, status, **kwargs):
            self.assertEqual(status, "active")
            self.assertEqual(vrf_id, common_vrf_id)
            return prefixes[prefix]

        def interfaces_impl(**kwargs):
            try:
                kwargs["device_id"]
                return _as_generator([iface1, iface2, iface3, iface4])
            except KeyError:
                kwargs["lag_id"]
                return _as_generator([iface3, iface4])

        client.ipam.ip_addresses.filter.side_effect = ip_addresses_impl
        client.ipam.prefixes.filter.side_effect = match_prefixes_impl
        client.ipam.prefixes.get.side_effect = prefixes_impl
        client.dcim.interfaces.filter.side_effect = interfaces_impl

        device = unittest.mock.Mock()
        device.id = sentinel.device_id

        client.dcim.interfaces.filter.return_value = [iface1, iface2,
                                                      iface3, iface4]

        result = netbox._make_netplan(
            client,
            device,
            sentinel.gateway_tag,
            sentinel.nameserver_field,
            sentinel.search_domain,
        )

        self.assertCountEqual(
            [
                unittest.mock.call(device_id=device.id, mgmt_only=False),
                unittest.mock.call(lag_id=iface2.id, mgmt_only=False),
            ],
            client.dcim.interfaces.filter.mock_calls,
        )

        self.assertIn(
            unittest.mock.call(interface_id=sentinel.iface1_id),
            client.ipam.ip_addresses.filter.mock_calls,
        )

        self.assertIn(
            unittest.mock.call(interface_id=sentinel.iface2_id),
            client.ipam.ip_addresses.filter.mock_calls,
        )

        self.assertIn(
            unittest.mock.call(interface_id=sentinel.iface3_id),
            client.ipam.ip_addresses.filter.mock_calls,
        )

        self.assertIn(
            unittest.mock.call(interface_id=sentinel.iface4_id),
            client.ipam.ip_addresses.filter.mock_calls,
        )

        self.assertEqual(
            5,
            len(client.ipam.prefixes.get.mock_calls),
        )

        self.assertEqual(result["version"], 2)
        self.assertNotIn("link-local", result["bonds"])
        self.assertDictEqual(result["ethernets"]["foo-if"], {
            "set-name": "foo-if",
            "match": {"macaddress": "aa:bb:cc:dd:ee:ff"},
            "addresses": ["10.1.0.1/16", "2001:db8::1/64"],
            "link-local": ["ipv6"],
            "mtu": 1337,
            })
        self.assertDictEqual(result["bonds"]["bar-if"], {
            "interfaces": ["phy-if1", "phy-if2"],
            "parameters": {
                "mode": "802.3ad",
                "transmit-hash-policy": "layer3+4",
            },
            "addresses": ["10.2.0.1/16"],
            "link-local": [],
            "mtu": 1337,
        })
        self.assertDictEqual(result["vlans"]["bar-if.201"], {
            "id": 201,
            "link": "bar-if",
            "addresses": ["10.3.0.1/16"],
            "link-local": [],
            "mtu": 1337,
        })
        self.assertDictEqual(result["vlans"]["bar-if.202"], {
            "id": 202,
            "link": "bar-if",
            "addresses": ["10.4.0.1/16"],
            "link-local": [],
            "mtu": 1337,
        })

    def test__make_netplan_vlans_dualstack_bonding_bare_bond(self):
        client = unittest.mock.Mock()

        iface1 = unittest.mock.Mock()
        iface1.id = sentinel.iface1_id
        iface1.name = "foo-if"
        iface1.mac_address = "AA:BB:CC:DD:EE:FF"
        iface1.untagged_vlan = _mk_vlan_shim(100)
        iface1.tagged_vlans = []
        iface1.mtu = 1337

        iface2 = unittest.mock.Mock()
        iface2.id = sentinel.iface2_id
        iface2.name = "bar-if"
        iface2.mac_address = "12:34:56:78:9a:bc"
        iface2.untagged_vlan = None
        iface2.tagged_vlans = [
            _mk_vlan_shim(201),
            _mk_vlan_shim(202),
        ]
        iface2.type.value = "lag"
        iface2.custom_fields = {
            "bond_mode": "802.3ad",
            "bond_hash": "layer3+4"
        }
        iface2.mtu = 1337

        iface3 = unittest.mock.Mock()
        iface3.id = sentinel.iface3_id
        iface3.name = "phy-if1"
        iface3.untagged_vlan = None
        iface3.tagged_vlans = []
        iface3.mtu = 1337

        iface4 = unittest.mock.Mock()
        iface4.id = sentinel.iface4_id
        iface4.name = "phy-if2"
        iface4.untagged_vlan = None
        iface4.tagged_vlans = []
        iface4.mtu = 1337

        common_vrf_id = sentinel.vrf_id

        addr1 = unittest.mock.Mock()
        addr1.address = "10.1.0.1/16"
        addr1.vrf.id = common_vrf_id

        addr3 = unittest.mock.Mock()
        addr3.address = "10.3.0.1/16"
        addr3.vrf.id = common_vrf_id

        addr4 = unittest.mock.Mock()
        addr4.address = "10.4.0.1/16"
        addr4.vrf.id = common_vrf_id

        addr5 = unittest.mock.Mock()
        addr5.address = "2001:db8::1/64"
        addr5.vrf.id = common_vrf_id
        addr5.family.value = 6

        prefix1 = unittest.mock.Mock()
        prefix1.vlan = _mk_vlan_shim(100)
        prefix1.prefix = "10.1.0.0/16"
        prefix1.custom_fields = {"yaook_nameserver": None}

        prefix3 = unittest.mock.Mock()
        prefix3.vlan = _mk_vlan_shim(201)
        prefix3.prefix = "10.3.0.0/16"
        prefix3.custom_fields = {"yaook_nameserver": None}

        prefix4 = unittest.mock.Mock()
        prefix4.vlan = _mk_vlan_shim(202)
        prefix4.prefix = "10.4.0.0/16"
        prefix4.custom_fields = {"yaook_nameserver": None}

        prefix5 = unittest.mock.Mock()
        prefix5.vlan = _mk_vlan_shim(100)
        prefix5.prefix = "2001:db8::/64"
        prefix5.custom_fields = {"yaook_nameserver": None}

        addrs = {
            sentinel.iface1_id: [
                addr1,
                addr5,
            ],
            sentinel.iface2_id: [
                addr4,
                addr3,
            ],
            sentinel.iface3_id: [],
            sentinel.iface4_id: [],
        }

        prefixes = {
            "10.1.0.1/16": prefix1,
            "2001:db8::1/64": prefix5,
            "10.3.0.1/16": prefix3,
            "10.4.0.1/16": prefix4,
        }

        def ip_addresses_impl(**kwargs):
            try:
                interface_id = kwargs["interface_id"]
                return _as_generator(addrs.get(interface_id, []))
            except KeyError:
                return _as_generator([])

        def match_prefixes_impl(*, contains, vrf_id, status, **kwargs):
            self.assertEqual(status, "active")
            self.assertEqual(vrf_id, common_vrf_id)
            return [contains]

        def prefixes_impl(*, prefix, vrf_id, status, **kwargs):
            self.assertEqual(status, "active")
            self.assertEqual(vrf_id, common_vrf_id)
            return prefixes[prefix]

        def interfaces_impl(**kwargs):
            try:
                kwargs["device_id"]
                return _as_generator([iface1, iface2, iface3, iface4])
            except KeyError:
                kwargs["lag_id"]
                return _as_generator([iface3, iface4])

        client.ipam.ip_addresses.filter.side_effect = ip_addresses_impl
        client.ipam.prefixes.filter.side_effect = match_prefixes_impl
        client.ipam.prefixes.get.side_effect = prefixes_impl
        client.dcim.interfaces.filter.side_effect = interfaces_impl

        device = unittest.mock.Mock()
        device.id = sentinel.device_id

        client.dcim.interfaces.filter.return_value = [iface1, iface2,
                                                      iface3, iface4]

        result = netbox._make_netplan(
            client,
            device,
            sentinel.gateway_tag,
            sentinel.nameserver_field,
            sentinel.search_domain,
        )

        self.assertCountEqual(
            [
                unittest.mock.call(device_id=device.id, mgmt_only=False),
                unittest.mock.call(lag_id=iface2.id, mgmt_only=False),
            ],
            client.dcim.interfaces.filter.mock_calls,
        )

        self.assertIn(
            unittest.mock.call(interface_id=sentinel.iface1_id),
            client.ipam.ip_addresses.filter.mock_calls,
        )

        self.assertIn(
            unittest.mock.call(interface_id=sentinel.iface2_id),
            client.ipam.ip_addresses.filter.mock_calls,
        )

        self.assertIn(
            unittest.mock.call(interface_id=sentinel.iface3_id),
            client.ipam.ip_addresses.filter.mock_calls,
        )

        self.assertIn(
            unittest.mock.call(interface_id=sentinel.iface4_id),
            client.ipam.ip_addresses.filter.mock_calls,
        )

        self.assertEqual(
            4,
            len(client.ipam.prefixes.get.mock_calls),
        )

        self.assertEqual(result["version"], 2)
        self.assertDictEqual(result["ethernets"]["foo-if"], {
            "set-name": "foo-if",
            "match": {"macaddress": "aa:bb:cc:dd:ee:ff"},
            "addresses": ["10.1.0.1/16", "2001:db8::1/64"],
            "link-local": ["ipv6"],
            "mtu": 1337,
            })
        self.assertDictEqual(result["bonds"]["bar-if"], {
            "interfaces": ["phy-if1", "phy-if2"],
            "parameters": {
                "mode": "802.3ad",
                "transmit-hash-policy": "layer3+4",
            },
            "link-local": [],
            "mtu": 1337,
        })
        self.assertDictEqual(result["vlans"]["bar-if.201"], {
            "id": 201,
            "link": "bar-if",
            "addresses": ["10.3.0.1/16"],
            "link-local": [],
            "mtu": 1337,
        })
        self.assertDictEqual(result["vlans"]["bar-if.202"], {
            "id": 202,
            "link": "bar-if",
            "addresses": ["10.4.0.1/16"],
            "link-local": [],
            "mtu": 1337,
        })

    def test__make_netplan_vlans_dualstack_no_bonding(self):
        client = unittest.mock.Mock()

        iface1 = unittest.mock.Mock()
        iface1.id = sentinel.iface1_id
        iface1.name = "foo-if"
        iface1.mac_address = "AA:BB:CC:DD:EE:FF"
        iface1.untagged_vlan = _mk_vlan_shim(100)
        iface1.tagged_vlans = []
        iface1.mtu = 1337

        iface2 = unittest.mock.Mock()
        iface2.id = sentinel.iface2_id
        iface2.name = "bar-if"
        iface2.mac_address = "12:34:56:78:9a:bc"
        iface2.untagged_vlan = _mk_vlan_shim(200)
        iface2.tagged_vlans = [
            _mk_vlan_shim(201),
            _mk_vlan_shim(202),
        ]
        iface2.mtu = 1337

        common_vrf_id = sentinel.vrf_id

        addr1 = unittest.mock.Mock()
        addr1.address = "10.1.0.1/16"
        addr1.vrf.id = common_vrf_id

        addr2 = unittest.mock.Mock()
        addr2.address = "10.2.0.1/16"
        addr2.vrf.id = common_vrf_id

        addr3 = unittest.mock.Mock()
        addr3.address = "10.3.0.1/16"
        addr3.vrf.id = common_vrf_id

        addr4 = unittest.mock.Mock()
        addr4.address = "10.4.0.1/16"
        addr4.vrf.id = common_vrf_id

        addr5 = unittest.mock.Mock()
        addr5.address = "2001:db8::1/64"
        addr5.vrf.id = common_vrf_id
        addr5.family.value = 6

        prefix1 = unittest.mock.Mock()
        prefix1.vlan = _mk_vlan_shim(100)
        prefix1.prefix = "10.1.0.0/16"
        prefix1.custom_fields = {"yaook_nameserver": None}

        prefix2 = unittest.mock.Mock()
        prefix2.vlan = _mk_vlan_shim(200)
        prefix2.prefix = "10.2.0.0/16"
        prefix2.custom_fields = {"yaook_nameserver": None}

        prefix3 = unittest.mock.Mock()
        prefix3.vlan = _mk_vlan_shim(201)
        prefix3.prefix = "10.3.0.0/16"
        prefix3.custom_fields = {"yaook_nameserver": None}

        prefix4 = unittest.mock.Mock()
        prefix4.vlan = _mk_vlan_shim(202)
        prefix4.prefix = "10.4.0.0/16"
        prefix4.custom_fields = {"yaook_nameserver": None}

        prefix5 = unittest.mock.Mock()
        prefix5.vlan = _mk_vlan_shim(100)
        prefix5.prefix = "2001:db8::/64"
        prefix5.custom_fields = {"yaook_nameserver": None}

        addrs = {
            sentinel.iface1_id: [
                addr1,
                addr5,
            ],
            sentinel.iface2_id: [
                addr4,
                addr2,
                addr3,
            ],
        }

        prefixes = {
            "10.1.0.1/16": prefix1,
            "2001:db8::1/64": prefix5,
            "10.2.0.1/16": prefix2,
            "10.3.0.1/16": prefix3,
            "10.4.0.1/16": prefix4,
        }

        def ip_addresses_impl(**kwargs):
            try:
                interface_id = kwargs["interface_id"]
                return _as_generator(addrs.get(interface_id, []))
            except KeyError:
                return _as_generator([])

        def match_prefixes_impl(*, contains, vrf_id, status, **kwargs):
            self.assertEqual(status, "active")
            self.assertEqual(vrf_id, common_vrf_id)
            return [contains]

        def prefixes_impl(*, prefix, vrf_id, status, **kwargs):
            self.assertEqual(status, "active")
            self.assertEqual(vrf_id, common_vrf_id)
            return prefixes[prefix]

        client.ipam.ip_addresses.filter.side_effect = ip_addresses_impl
        client.ipam.prefixes.filter.side_effect = match_prefixes_impl
        client.ipam.prefixes.get.side_effect = prefixes_impl

        device = unittest.mock.Mock()
        device.id = sentinel.device_id

        client.dcim.interfaces.filter.return_value = [iface1, iface2]

        result = netbox._make_netplan(
            client,
            device,
            sentinel.gateway_tag,
            sentinel.nameserver_field,
            sentinel.search_domain,
        )

        client.dcim.interfaces.filter.assert_called_once_with(
            device_id=device.id,
            mgmt_only=False,
        )

        self.assertIn(
            unittest.mock.call(interface_id=sentinel.iface1_id),
            client.ipam.ip_addresses.filter.mock_calls,
        )

        self.assertIn(
            unittest.mock.call(interface_id=sentinel.iface2_id),
            client.ipam.ip_addresses.filter.mock_calls,
        )

        self.assertEqual(
            5,
            len(client.ipam.prefixes.get.mock_calls),
        )

        self.assertEqual(result["version"], 2)
        self.assertDictEqual(result["ethernets"]["foo-if"], {
            "match": {"macaddress": "aa:bb:cc:dd:ee:ff"},
            "set-name": "foo-if",
            "addresses": ["10.1.0.1/16", "2001:db8::1/64"],
            "link-local": ["ipv6"],
            "mtu": 1337,
        })
        self.assertDictEqual(result["ethernets"]["bar-if"], {
            "match": {"macaddress": "12:34:56:78:9a:bc"},
            "set-name": "bar-if",
            "addresses": ["10.2.0.1/16"],
            "link-local": [],
            "mtu": 1337,
        })
        self.assertDictEqual(result["vlans"]["bar-if.201"], {
            "id": 201,
            "link": "bar-if",
            "addresses": ["10.3.0.1/16"],
            "link-local": [],
            "mtu": 1337,
        })
        self.assertDictEqual(result["vlans"]["bar-if.202"], {
            "id": 202,
            "link": "bar-if",
            "addresses": ["10.4.0.1/16"],
            "link-local": [],
            "mtu": 1337,
        })

    def test__make_netplan_vlans_v4_gateway_nameserver_no_bonding(self):
        client = unittest.mock.Mock()

        iface1 = unittest.mock.Mock()
        iface1.id = sentinel.iface1_id
        iface1.name = "foo-if"
        iface1.mac_address = "AA:BB:CC:DD:EE:FF"
        iface1.untagged_vlan = _mk_vlan_shim(100)
        iface1.tagged_vlans = []
        iface1.mtu = 1337

        common_vrf_id = sentinel.vrf_id

        addr1 = unittest.mock.Mock()
        addr1.address = "10.1.0.1/16"
        addr1.vrf.id = common_vrf_id
        addr1.family.value = 4

        addr_gw = unittest.mock.Mock()
        addr_gw.address = "10.1.0.254/16"

        prefix1 = unittest.mock.Mock()
        prefix1.vlan = _mk_vlan_shim(100)
        prefix1.prefix = "10.1.0.0/16"
        prefix1.custom_fields = {
            "yaook_nameserver": "10.1.0.252,10.1.0.253"
        }

        addrs = {
            sentinel.iface1_id: [
                addr1,
            ],
        }

        addrs_by_tag = {
            sentinel.gateway_tag: [
                addr_gw,
            ],
        }

        prefixes = {
            "10.1.0.1/16": prefix1,
        }

        def ip_addresses_impl(**kwargs):
            try:
                interface_id = kwargs["interface_id"]
                return _as_generator(addrs.get(interface_id, []))
            except KeyError:
                tag = kwargs["tag"]
                return _as_generator(addrs_by_tag.get(tag, []))

        def match_prefixes_impl(*, contains, vrf_id, status, **kwargs):
            self.assertEqual(status, "active")
            self.assertEqual(vrf_id, common_vrf_id)
            return [contains]

        def prefixes_impl(*, prefix, vrf_id, status, **kwargs):
            self.assertEqual(status, "active")
            self.assertEqual(vrf_id, common_vrf_id)
            return prefixes[prefix]

        client.ipam.ip_addresses.filter.side_effect = ip_addresses_impl
        client.ipam.prefixes.filter.side_effect = match_prefixes_impl
        client.ipam.prefixes.get.side_effect = prefixes_impl

        device = unittest.mock.Mock()
        device.id = sentinel.device_id

        client.dcim.interfaces.filter.return_value = [iface1]

        result = netbox._make_netplan(
            client, device,
            gateway_tag=sentinel.gateway_tag,
            nameserver_field="yaook_nameserver",
            search_domains=["abc", "def"],
        )

        client.dcim.interfaces.filter.assert_called_once_with(
            device_id=device.id,
            mgmt_only=False,
        )

        self.assertCountEqual(
            [
                unittest.mock.call(interface_id=sentinel.iface1_id),
                unittest.mock.call(
                    vrf_id=addr1.vrf.id,
                    status="active",
                    parent=prefix1.prefix,
                    tag=sentinel.gateway_tag,
                ),
            ],
            client.ipam.ip_addresses.filter.mock_calls,
        )

        self.assertEqual(
            1,
            len(client.ipam.prefixes.get.mock_calls),
        )

        self.assertEqual(result["version"], 2)
        self.assertDictEqual(result["ethernets"]["foo-if"], {
            "match": {"macaddress": "aa:bb:cc:dd:ee:ff"},
            "set-name": "foo-if",
            "addresses": ["10.1.0.1/16"],
            "gateway4": "10.1.0.254",
            "link-local": [],
            "nameservers": {
                "search": ["abc", "def"],
                "addresses": [
                    "10.1.0.252",
                    "10.1.0.253",
                ]
            },
            "mtu": 1337,
        })

    def test__make_netplan_vlans_v6_gateway_nameserver_no_bonding(self):
        client = unittest.mock.Mock()

        iface1 = unittest.mock.Mock()
        iface1.id = sentinel.iface1_id
        iface1.name = "foo-if"
        iface1.mac_address = "AA:BB:CC:DD:EE:FF"
        iface1.untagged_vlan = _mk_vlan_shim(100)
        iface1.tagged_vlans = []
        iface1.mtu = 1337

        common_vrf_id = sentinel.vrf_id

        addr1 = unittest.mock.Mock()
        addr1.address = "2001:db8::1/64"
        addr1.vrf.id = common_vrf_id
        addr1.family.value = 6

        addr_gw = unittest.mock.Mock()
        addr_gw.address = "2001:db8::0/64"

        prefix1 = unittest.mock.Mock()
        prefix1.prefix = "2001:db8::/64"
        prefix1.vlan = _mk_vlan_shim(100)
        prefix1.custom_fields = {
            "yaook_nameserver": "2001:db8::fe,2001:db8::ff"
        }

        addrs = {
            sentinel.iface1_id: [
                addr1,
            ],
        }

        addrs_by_tag = {
            sentinel.gateway_tag: [
                addr_gw,
            ],
        }

        prefixes = {
            "2001:db8::1/64": prefix1,
        }

        def ip_addresses_impl(**kwargs):
            try:
                interface_id = kwargs["interface_id"]
                return _as_generator(addrs.get(interface_id, []))
            except KeyError:
                tag = kwargs["tag"]
                return _as_generator(addrs_by_tag.get(tag, []))

        def match_prefixes_impl(*, contains, vrf_id, status, **kwargs):
            self.assertEqual(status, "active")
            self.assertEqual(vrf_id, common_vrf_id)
            return [contains]

        def prefixes_impl(*, prefix, vrf_id, status, **kwargs):
            self.assertEqual(status, "active")
            self.assertEqual(vrf_id, common_vrf_id)
            return prefixes[prefix]

        client.ipam.ip_addresses.filter.side_effect = ip_addresses_impl
        client.ipam.prefixes.filter.side_effect = match_prefixes_impl
        client.ipam.prefixes.get.side_effect = prefixes_impl

        device = unittest.mock.Mock()
        device.id = sentinel.device_id

        client.dcim.interfaces.filter.return_value = [iface1]

        result = netbox._make_netplan(
            client, device,
            gateway_tag=sentinel.gateway_tag,
            nameserver_field="yaook_nameserver",
            search_domains=["abc", "def"],
        )

        client.dcim.interfaces.filter.assert_called_once_with(
            device_id=device.id,
            mgmt_only=False,
        )

        self.assertCountEqual(
            [
                unittest.mock.call(interface_id=sentinel.iface1_id),
                unittest.mock.call(
                    vrf_id=addr1.vrf.id,
                    status="active",
                    parent=prefix1.prefix,
                    tag=sentinel.gateway_tag,
                ),
            ],
            client.ipam.ip_addresses.filter.mock_calls,
        )

        self.assertEqual(
            1,
            len(client.ipam.prefixes.get.mock_calls),
        )

        self.assertEqual(result["version"], 2)
        self.assertDictEqual(result["ethernets"]["foo-if"], {
            "match": {"macaddress": "aa:bb:cc:dd:ee:ff"},
            "set-name": "foo-if",
            "addresses": ["2001:db8::1/64"],
            "gateway6": "2001:db8::",
            "link-local": ["ipv6"],
            "nameservers": {
                "search": ["abc", "def"],
                "addresses": [
                    "2001:db8::fe",
                    "2001:db8::ff",
                ]
            },
            "mtu": 1337,
        })

    def test__make_netplan_vlans_dualstack_gateway_nameserver_no_bonding(self):
        client = unittest.mock.Mock()

        iface1 = unittest.mock.Mock()
        iface1.id = sentinel.iface1_id
        iface1.name = "foo-if"
        iface1.mac_address = "AA:BB:CC:DD:EE:FF"
        iface1.untagged_vlan = _mk_vlan_shim(100)
        iface1.tagged_vlans = []
        iface1.mtu = 1337

        common_vrf_id = sentinel.vrf_id

        addr1 = unittest.mock.Mock()
        addr1.address = "2001:db8::1/64"
        addr1.vrf.id = common_vrf_id
        addr1.family.value = 6

        addr2 = unittest.mock.Mock()
        addr2.address = "10.0.0.1/24"
        addr2.vrf.id = common_vrf_id
        addr2.family.value = 4

        addr_gw6 = unittest.mock.Mock()
        addr_gw6.address = "2001:db8::0/64"
        addr_gw6.family.value = 6

        addr_gw4 = unittest.mock.Mock()
        addr_gw4.address = "10.0.0.254/24"

        prefix1 = unittest.mock.Mock()
        prefix1.vlan = _mk_vlan_shim(100)
        prefix1.prefix = "2001:db8::/64"
        prefix1.custom_fields = {
            "yaook_nameserver": "2001:db8::fe,2001:db8::ff"
        }

        prefix2 = unittest.mock.Mock()
        prefix2.vlan = _mk_vlan_shim(100)
        prefix2.prefix = "10.0.0.0/24"
        prefix2.custom_fields = {
            "yaook_nameserver": "10.0.0.252,10.0.0.253"
        }

        addrs = {
            sentinel.iface1_id: [
                addr1,
                addr2,
            ],
        }

        addrs_by_tag = {
            (prefix1.prefix, sentinel.gateway_tag): [
                addr_gw6,
            ],
            (prefix2.prefix, sentinel.gateway_tag): [
                addr_gw4,
            ],
        }

        prefixes = {
            "2001:db8::1/64": prefix1,
            "10.0.0.1/24": prefix2,
        }

        def ip_addresses_impl(**kwargs):
            try:
                interface_id = kwargs["interface_id"]
                return _as_generator(addrs.get(interface_id, []))
            except KeyError:
                tag = kwargs["tag"]
                prefix = kwargs["parent"]
                return _as_generator(addrs_by_tag.get((prefix, tag), []))

        def match_prefixes_impl(*, contains, vrf_id, status, **kwargs):
            self.assertEqual(status, "active")
            self.assertEqual(vrf_id, common_vrf_id)
            return [contains]

        def prefixes_impl(*, prefix, vrf_id, status, **kwargs):
            self.assertEqual(status, "active")
            self.assertEqual(vrf_id, common_vrf_id)
            return prefixes[prefix]

        client.ipam.ip_addresses.filter.side_effect = ip_addresses_impl
        client.ipam.prefixes.filter.side_effect = match_prefixes_impl
        client.ipam.prefixes.get.side_effect = prefixes_impl

        device = unittest.mock.Mock()
        device.id = sentinel.device_id

        client.dcim.interfaces.filter.return_value = [iface1]

        result = netbox._make_netplan(
            client, device,
            gateway_tag=sentinel.gateway_tag,
            nameserver_field="yaook_nameserver",
            search_domains=["abc", "def"],
        )

        client.dcim.interfaces.filter.assert_called_once_with(
            device_id=device.id,
            mgmt_only=False,
        )

        self.assertCountEqual(
            [
                unittest.mock.call(interface_id=sentinel.iface1_id),
                unittest.mock.call(
                    vrf_id=addr1.vrf.id,
                    status="active",
                    parent=prefix1.prefix,
                    tag=sentinel.gateway_tag,
                ),
                unittest.mock.call(
                    vrf_id=addr2.vrf.id,
                    status="active",
                    parent=prefix2.prefix,
                    tag=sentinel.gateway_tag,
                ),
            ],
            client.ipam.ip_addresses.filter.mock_calls,
        )

        self.assertEqual(
            2,
            len(client.ipam.prefixes.get.mock_calls),
        )

        self.assertEqual(result["version"], 2)
        self.assertDictEqual(result["ethernets"]["foo-if"], {
            "match": {"macaddress": "aa:bb:cc:dd:ee:ff"},
            "set-name": "foo-if",
            "addresses": ["2001:db8::1/64", "10.0.0.1/24"],
            "gateway4": "10.0.0.254",
            "gateway6": "2001:db8::",
            "link-local": ["ipv6"],
            "nameservers": {
                "search": ["abc", "def"],
                "addresses": [
                    "2001:db8::fe",
                    "2001:db8::ff",
                    "10.0.0.252",
                    "10.0.0.253",
                ]
            },
            "mtu": 1337,
        })

    def test__make_netplan_deduplicates_nameservers(self):
        client = unittest.mock.Mock()

        iface1 = unittest.mock.Mock()
        iface1.id = sentinel.iface1_id
        iface1.name = "foo-if"
        iface1.mac_address = "AA:BB:CC:DD:EE:FF"
        iface1.untagged_vlan = _mk_vlan_shim(100)
        iface1.tagged_vlans = []
        iface1.mtu = 1337

        common_vrf_id = sentinel.vrf_id

        addr1 = unittest.mock.Mock()
        addr1.address = "10.1.0.1/16"
        addr1.vrf.id = common_vrf_id
        addr1.family.value = 4

        addr2 = unittest.mock.Mock()
        addr2.address = "10.1.0.2/16"
        addr2.vrf.id = common_vrf_id
        addr2.family.value = 4

        addr_gw = unittest.mock.Mock()
        addr_gw.address = "10.1.0.254/16"

        prefix1 = unittest.mock.Mock()
        prefix1.vlan = _mk_vlan_shim(100)
        prefix1.prefix = "10.1.0.0/16"
        prefix1.custom_fields = {
            "yaook_nameserver": "10.1.0.252,10.1.0.253"
        }

        addrs = {
            sentinel.iface1_id: [
                addr1,
                addr2,
            ],
        }

        addrs_by_tag = {
            sentinel.gateway_tag: [
                addr_gw,
            ],
        }

        prefixes = {
            "10.1.0.1/16": prefix1,
            "10.1.0.2/16": prefix1,
        }

        def ip_addresses_impl(**kwargs):
            try:
                interface_id = kwargs["interface_id"]
                return _as_generator(addrs.get(interface_id, []))
            except KeyError:
                tag = kwargs["tag"]
                return _as_generator(addrs_by_tag.get(tag, []))

        def match_prefixes_impl(*, contains, vrf_id, status, **kwargs):
            self.assertEqual(status, "active")
            self.assertEqual(vrf_id, common_vrf_id)
            return [contains]

        def prefixes_impl(*, prefix, vrf_id, status, **kwargs):
            self.assertEqual(status, "active")
            self.assertEqual(vrf_id, common_vrf_id)
            return prefixes[prefix]

        client.ipam.ip_addresses.filter.side_effect = ip_addresses_impl
        client.ipam.prefixes.filter.side_effect = match_prefixes_impl
        client.ipam.prefixes.get.side_effect = prefixes_impl

        device = unittest.mock.Mock()
        device.id = sentinel.device_id

        client.dcim.interfaces.filter.return_value = [iface1]

        result = netbox._make_netplan(
            client, device,
            gateway_tag=sentinel.gateway_tag,
            nameserver_field="yaook_nameserver",
            search_domains=["abc", "def"],
        )

        client.dcim.interfaces.filter.assert_called_once_with(
            device_id=device.id,
            mgmt_only=False,
        )

        self.assertCountEqual(
            [
                unittest.mock.call(interface_id=sentinel.iface1_id),
                unittest.mock.call(
                    vrf_id=addr1.vrf.id,
                    status="active",
                    parent=prefix1.prefix,
                    tag=sentinel.gateway_tag,
                ),
                unittest.mock.call(
                    vrf_id=addr1.vrf.id,
                    status="active",
                    parent=prefix1.prefix,
                    tag=sentinel.gateway_tag,
                ),
            ],
            client.ipam.ip_addresses.filter.mock_calls,
        )

        self.assertEqual(
            2,
            len(client.ipam.prefixes.get.mock_calls),
        )

        self.assertEqual(result["version"], 2)
        self.assertDictEqual(result["ethernets"]["foo-if"], {
            "match": {"macaddress": "aa:bb:cc:dd:ee:ff"},
            "set-name": "foo-if",
            "addresses": ["10.1.0.1/16", "10.1.0.2/16"],
            "gateway4": "10.1.0.254",
            "nameservers": {
                "search": ["abc", "def"],
                "addresses": [
                    "10.1.0.252",
                    "10.1.0.253",
                ]
            },
            "link-local": [],
            "mtu": 1337,
        })

    def test__customized_bond_settings(self):
        client = unittest.mock.Mock()

        iface1 = unittest.mock.Mock()
        iface1.id = sentinel.iface1_id
        iface1.name = "foo-if"
        iface1.untagged_vlan = None
        iface1.tagged_vlans = []
        iface1.type.value = "lag"
        iface1.custom_fields = {
            "bond_mode": "802.3ad",
            "bond_hash": "layer2"
        }
        iface1.mtu = 1337

        iface2 = unittest.mock.Mock()
        iface2.id = sentinel.iface2_id
        iface2.name = "bar-if"
        iface2.untagged_vlan = None
        iface2.tagged_vlans = []
        iface2.type.value = "lag"
        iface2.custom_fields = {
            "bond_mode": "active-backup",
        }
        iface2.mtu = 1337

        client.ipam.ip_addresses.filter.return_value = []

        device = unittest.mock.Mock()
        device.id = sentinel.device_id

        client.dcim.interfaces.filter.return_value = [iface1, iface2]

        result = netbox._make_netplan(
            client,
            device,
            sentinel.gateway_tag,
            sentinel.nameserver_field,
            sentinel.search_domain,
        )

        self.assertEqual(result["version"], 2)
        self.assertDictEqual(result["bonds"]["foo-if"], {
            "interfaces": ["foo-if", "bar-if"],
            "link-local": [],
            "parameters": {
                "mode": "802.3ad",
                "transmit-hash-policy": "layer2",
            },
            "mtu": 1337
        })
        self.assertDictEqual(result["bonds"]["bar-if"], {
            "interfaces": ["foo-if", "bar-if"],
            "link-local": [],
            "parameters": {
                "mode": "active-backup",
            },
            "mtu": 1337
        })

    def test_fails_on_netmask_missmatch(self):
        client = unittest.mock.Mock()

        iface1 = unittest.mock.Mock()
        iface1.id = sentinel.iface1_id
        iface1.name = "foo-if"
        iface1.untagged_vlan = None
        iface1.tagged_vlans = []
        iface1.type.value = "lag"
        iface1.custom_fields = {
            "bond_mode": "802.3ad",
            "bond_hash": "layer2"
        }
        iface1.mtu = 1337

        addr = unittest.mock.Mock()
        addr.address = "192.168.0.10/32"
        prefix = unittest.mock.Mock()
        prefix.prefix = "192.168.0.0/24"

        client.ipam.ip_addresses.filter.return_value = [addr]
        client.ipam.prefixes.get.return_value = prefix
        client.ipam.prefixes.filter.return_value = [sentinel.prefixentry]

        device = unittest.mock.Mock()
        device.id = sentinel.device_id

        client.dcim.interfaces.filter.return_value = [iface1]

        with self.assertRaises(ValueError):
            netbox._make_netplan(
                client,
                device,
                sentinel.gateway_tag,
                sentinel.nameserver_field,
                sentinel.search_domain,
            )


@ddt.ddt
class TestNetBoxBackend(unittest.TestCase):
    def setUp(self):
        self.logger = logging.getLogger("test")

    def _create_backend(self, *, env=None, client=None, vault_client=None):
        nb_url = "<some url, e.g. http://127.0.0.1:8080>"
        nb_token = "<some token>"

        env = env or {}
        env.setdefault("NETBOX_URL", nb_url)
        env.setdefault("NETBOX_TOKEN", nb_token)
        env.setdefault("NETBOX_ERROR_TAG_AUTOCREATE", "false")
        env.setdefault("NETBOX_CLUSTER_REPO_TEMPLATE",
                       "http://repo-server/{cluster}/cluster.git")
        env.setdefault("NETBOX_MK8S_REPO_TEMPLATE",
                       "http://repo-server/{cluster}/managed-k8s.git")
        env.setdefault("NETBOX_IMAGE_URL",
                       "http://image-server/{device.id}/image.qcow2")
        env.setdefault("NETBOX_IMAGE_CHECKSUM", "2342")
        env.setdefault("NETBOX_CONFIGDRIVE_MAKEFILE_PATH", "/dev/null")

        client = client or unittest.mock.Mock()

        with contextlib.ExitStack() as stack:
            api = stack.enter_context(unittest.mock.patch("pynetbox.api"))
            api.return_value = client

            stack.enter_context(unittest.mock.patch.dict("os.environ", env))

            nb = netbox.NetBoxBackend(self.logger)

        api.assert_called_once_with(
            url=nb_url,
            token=nb_token,
        )

        return client, nb

    def test_create_from_env(self):
        _, nb = self._create_backend()
        self.assertIsInstance(nb, netbox.NetBoxBackend)

    def test_creates_error_tag_if_non_existent(self):
        client = unittest.mock.Mock()
        client.extras.tags.get.return_value = None

        tag_slug = "fancy-magic"

        _, nb = self._create_backend(
            client=client,
            env={
                "NETBOX_ERROR_TAG": tag_slug,
                "NETBOX_ERROR_TAG_AUTOCREATE": "True",
            }
        )

        client.extras.tags.get.assert_called_once_with(
            slug=tag_slug,
        )

        client.extras.tags.create.assert_called_once_with(
            slug=tag_slug,
            name=tag_slug,
        )

    def test__find_node_in_netbox_by_interface(self):
        client, nb = self._create_backend(env={
            "NETBOX_NODE_MATCH_MODE": "by-interface",
            "NETBOX_NODE_MATCH_FIELD": "mac_address",
            "NETBOX_NODE_MATCH_EXPR": "{inventory[bmc_mac]}",
        })

        iface = unittest.mock.Mock()
        iface.device.id = sentinel.device_id

        client.dcim.devices.get.return_value = sentinel.device

        client.dcim.interfaces.filter.return_value = [
            iface,
        ]

        device = nb._find_node_in_netbox({
            "inventory": {"bmc_mac": sentinel.bmc_mac},
        })

        client.dcim.interfaces.filter.assert_called_once_with(
            mac_address=str(sentinel.bmc_mac),
        )
        client.dcim.devices.get.assert_called_once_with(sentinel.device_id)

        self.assertEqual(device, sentinel.device)

    def test__find_node_in_netbox_by_interface_returns_none_on_no_match(self):
        client, nb = self._create_backend(env={
            "NETBOX_NODE_MATCH_MODE": "by-interface",
            "NETBOX_NODE_MATCH_FIELD": "mac_address",
            "NETBOX_NODE_MATCH_EXPR": "{inventory[bmc_mac]}",
        })

        iface = unittest.mock.Mock()
        iface.device.id = sentinel.device_id

        client.dcim.interfaces.filter.return_value = []

        device = nb._find_node_in_netbox({
            "inventory": {"bmc_mac": sentinel.bmc_mac},
        })

        client.dcim.interfaces.filter.assert_called_once_with(
            mac_address=str(sentinel.bmc_mac),
        )
        client.dcim.devices.get.assert_not_called()

        self.assertIsNone(device)

    def test__find_node_in_netbox_by_interface_raises_on_multiple_results(self):  # noqa:E501
        client, nb = self._create_backend(env={
            "NETBOX_NODE_MATCH_MODE": "by-interface",
            "NETBOX_NODE_MATCH_FIELD": "mac_address",
            "NETBOX_NODE_MATCH_EXPR": "{inventory[bmc_mac]}",
        })

        client.dcim.interfaces.filter.return_value = [
            sentinel.iface1,
            sentinel.iface2,
        ]

        with self.assertRaisesRegex(ValueError,
                                    "multiple matching interfaces"):
            nb._find_node_in_netbox({
                "inventory": {"bmc_mac": sentinel.bmc_mac},
            })

        client.dcim.interfaces.filter.assert_called_once_with(
            mac_address=str(sentinel.bmc_mac),
        )
        client.dcim.devices.get.assert_not_called()

    def test__find_node_in_netbox_by_attribute(self):
        client, nb = self._create_backend(env={
            "NETBOX_NODE_MATCH_MODE": "by-attribute",
            "NETBOX_NODE_MATCH_FIELD": "serial_number",
            "NETBOX_NODE_MATCH_EXPR": "{inventory[system_vendor][serial]}",
        })

        client.dcim.devices.filter.return_value = [
            sentinel.device
        ]

        device = nb._find_node_in_netbox({
            "inventory": {"system_vendor": {"serial": sentinel.serial}},
        })

        client.dcim.devices.filter.assert_called_once_with(
            serial_number=str(sentinel.serial),
        )

        self.assertEqual(device, sentinel.device)

    def test__find_node_in_netbox_by_attribute_returns_none_on_no_match(self):
        client, nb = self._create_backend(env={
            "NETBOX_NODE_MATCH_MODE": "by-attribute",
            "NETBOX_NODE_MATCH_FIELD": "serial_number",
            "NETBOX_NODE_MATCH_EXPR": "{inventory[system_vendor][serial]}",
        })

        client.dcim.devices.filter.return_value = []

        device = nb._find_node_in_netbox({
            "inventory": {"system_vendor": {"serial": sentinel.serial}},
        })

        client.dcim.devices.filter.assert_called_once_with(
            serial_number=str(sentinel.serial),
        )

        self.assertIsNone(device)

    def test__find_node_in_netbox_by_attribute_raises_on_multiple_results(self):  # noqa:E501
        client, nb = self._create_backend(env={
            "NETBOX_NODE_MATCH_MODE": "by-attribute",
            "NETBOX_NODE_MATCH_FIELD": "serial_number",
            "NETBOX_NODE_MATCH_EXPR": "{inventory[system_vendor][serial]}",
        })

        client.dcim.devices.filter.return_value = [
            sentinel.device1,
            sentinel.device2,
        ]

        with self.assertRaisesRegex(ValueError,
                                    "multiple matching devices"):
            nb._find_node_in_netbox({
                "inventory": {"system_vendor": {"serial": sentinel.serial}},
            })

        client.dcim.devices.filter.assert_called_once_with(
            serial_number=str(sentinel.serial),
        )

    def test__read_interfaces_returns_filtered_from_api(self):
        client, nb = self._create_backend()

        client.dcim.interfaces.filter.return_value = [
            sentinel.iface1,
            sentinel.iface2,
        ]

        ifaces = nb._read_interfaces(sentinel.device_id)

        client.dcim.interfaces.filter.assert_called_once_with(
            device_id=sentinel.device_id,
        )

        self.assertCountEqual(
            ifaces,
            [
                sentinel.iface1,
                sentinel.iface2,
            ],
        )

    def test__read_cluster_returns_none_if_no_cluster_assigned(self):
        _, nb = self._create_backend()

        device = unittest.mock.Mock()
        device.cluster = None

        result = nb._read_cluster(device)

        self.assertIsNone(result)

    def test__read_cluster_returns_none_if_cluster_has_wrong_type(self):
        _, nb = self._create_backend(env={
            "NETBOX_CLUSTER_TYPES": "matching-cluster",
        })

        def fetch_full_details():
            device.cluster.type = unittest.mock.Mock()
            device.cluster.type.slug = "mismatching-cluster"

        device = unittest.mock.Mock()
        device.cluster = unittest.mock.Mock([])
        device.cluster.id = 123
        device.cluster.full_details = unittest.mock.Mock([])
        device.cluster.full_details.side_effect = fetch_full_details

        result = nb._read_cluster(device)

        device.cluster.full_details.assert_called_once_with()

        self.assertIsNone(result)

    def test__read_cluster_returns_custom_field_of_matching_cluster(self):
        _, nb = self._create_backend(env={
            "NETBOX_CLUSTER_TYPES": "matching-cluster",
            "NETBOX_CLUSTER_DOMAIN_FIELD": "cf_foobar",
        })

        def fetch_full_details():
            device.cluster.type = unittest.mock.Mock()
            device.cluster.type.slug = "matching-cluster"
            device.cluster.custom_fields = {"foobar": sentinel.value}

        device = unittest.mock.Mock()
        device.cluster = unittest.mock.Mock([])
        device.cluster.id = 123
        device.cluster.full_details = unittest.mock.Mock([])
        device.cluster.full_details.side_effect = fetch_full_details

        result = nb._read_cluster(device)

        device.cluster.full_details.assert_called_once_with()

        self.assertEqual(result, sentinel.value)

    def _make_interface(self, name, *, mac=None):
        iface = unittest.mock.Mock()
        iface.name = name
        iface.mac_address = mac
        return iface

    def test__merge_interfaces_updates_names_of_matching_macs_in_place(self):
        client, nb = self._create_backend()

        if1 = self._make_interface("unknown0", mac="mac1")
        if2 = self._make_interface("unknown1", mac="mac2")
        if3 = self._make_interface("unknown2", mac="mac3")
        if4 = self._make_interface("unknown3", mac="mac4")

        nb._merge_interfaces(
            sentinel.device_id,
            [if1, if2, if3, if4],
            [
                {
                    "name": "enp6s0f2",
                    "mac_address": "mac3",
                },
                {
                    "name": "eno1",
                    "mac_address": "mac1",
                },
                {
                    "name": "enp6s0f1",
                    "mac_address": "mac2",
                },
            ]
        )

        self.assertEqual(if1.name, "eno1")
        self.assertEqual(if2.name, "enp6s0f1")
        self.assertEqual(if3.name, "enp6s0f2")
        self.assertEqual(if4.name, "unknown3")

        if1.save.assert_called_once_with()
        if2.save.assert_called_once_with()
        if3.save.assert_called_once_with()
        if4.save.assert_not_called()

    def test__merge_interfaces_updates_macs_of_matching_ifaces_by_name(self):
        client, nb = self._create_backend()

        if1 = self._make_interface("eno1")
        if2 = self._make_interface("eno2")
        if3 = self._make_interface("enp6s0f1", mac="other_mac3")
        if4 = self._make_interface("enp6s0f2")

        nb._merge_interfaces(
            sentinel.device_id,
            [if1, if2, if3, if4],
            [
                {
                    "name": "enp6s0f2",
                    "mac_address": "mac3",
                },
                {
                    "name": "eno1",
                    "mac_address": "mac1",
                },
                {
                    "name": "enp6s0f1",
                    "mac_address": "mac2",
                },
            ]
        )

        self.assertEqual(if1.mac_address, "mac1")
        self.assertIsNone(if2.mac_address)
        self.assertEqual(if3.mac_address, "other_mac3")
        self.assertEqual(if4.mac_address, "mac3")

        if1.save.assert_called_once_with()
        if2.save.assert_not_called()
        if3.save.assert_not_called()
        if4.save.assert_called_once_with()

    def test__merge_interfaces_handles_naming_conflict_on_mixup(self):
        client, nb = self._create_backend()

        if1 = self._make_interface("eno1", mac="mac2")
        if1.saved_name = if1.name
        if2 = self._make_interface("eno2", mac="mac1")
        if2.saved_name = if2.name

        def checked_save(iface):
            if ((if1.saved_name == iface.name and iface is not if1) or
                    (if2.saved_name == iface.name and iface is not if2)):
                raise RuntimeError("naming conflict!")
            iface.saved_name = iface.name

        if1.save.side_effect = functools.partial(checked_save, if1)
        if2.save.side_effect = functools.partial(checked_save, if2)

        nb._merge_interfaces(
            sentinel.device_id,
            [if1, if2],
            [
                {
                    "name": "eno2",
                    "mac_address": "mac2",
                },
                {
                    "name": "eno1",
                    "mac_address": "mac1",
                },
            ]
        )

    def test__merge_interfaces_creates_missing_interfaces(self):
        client, nb = self._create_backend()

        if1 = self._make_interface("unknown0", mac="mac1")
        if2 = self._make_interface("unknown1", mac="mac2")

        nb._merge_interfaces(
            sentinel.device_id,
            [if1, if2],
            [
                {
                    "name": "enp6s0f2",
                    "mac_address": "mac3",
                },
                {
                    "name": "eno1",
                    "mac_address": "mac1",
                },
                {
                    "name": "enp6s0f1",
                    "mac_address": "mac2",
                },
            ]
        )

        client.dcim.interfaces.create.assert_called_once_with(
            device=sentinel.device_id,
            name="enp6s0f2",
            type="other",
            mac_address="mac3",
            description=(
                "filled in by metal-operator from Ironic introspection data"
            ),
        )

    def test__merge_comment_injects_ipmi_address(self):
        dev = unittest.mock.Mock()
        dev.comments = sentinel.comments_in

        client, nb = self._create_backend()

        with contextlib.ExitStack() as stack:
            _merge_dump = stack.enter_context(unittest.mock.patch(
                "metal_controller.netbox._merge_dump",
            ))
            _merge_dump.return_value = sentinel.comments_out

            result = nb._merge_comment(
                dev,
                {
                    "ipmi_address": "foo.bar.baz",
                },
            )

        self.assertTrue(result)

        _merge_dump.assert_called_once_with(
            sentinel.comments_in,
            unittest.mock.ANY,
        )

        self.assertEqual(dev.comments, sentinel.comments_out)

        _, (_, text), _ = _merge_dump.mock_calls[0]

        self.assertIn("IPMI address: foo.bar.baz\n", text)

    def test__merge_comment_returns_false_if_unchanged(self):
        dev = unittest.mock.Mock()
        dev.comments = sentinel.comments_in

        client, nb = self._create_backend()

        with contextlib.ExitStack() as stack:
            _merge_dump = stack.enter_context(unittest.mock.patch(
                "metal_controller.netbox._merge_dump",
            ))
            _merge_dump.return_value = sentinel.comments_in

            result = nb._merge_comment(
                dev,
                {
                    "ipmi_address": "foo.bar.baz",
                },
            )

        self.assertFalse(result)

    def test__set_message_injects_text(self):
        dev = unittest.mock.Mock()
        dev.comments = sentinel.comments_in

        client, nb = self._create_backend()

        with contextlib.ExitStack() as stack:
            _merge_info = stack.enter_context(unittest.mock.patch(
                "metal_controller.netbox._merge_info",
            ))
            _merge_info.return_value = sentinel.comments_out

            nb._set_message(
                dev,
                sentinel.msg,
            )

        _merge_info.assert_called_once_with(
            sentinel.comments_in,
            sentinel.msg,
        )

        self.assertEqual(dev.comments, sentinel.comments_out)
        dev.save.assert_called_once_with()

    def test__set_message_does_not_save_if_unchanged(self):
        dev = unittest.mock.Mock()
        dev.comments = sentinel.comments_in

        client, nb = self._create_backend()

        with contextlib.ExitStack() as stack:
            _merge_info = stack.enter_context(unittest.mock.patch(
                "metal_controller.netbox._merge_info",
            ))
            _merge_info.return_value = sentinel.comments_in

            nb._set_message(
                dev,
                sentinel.msg,
            )

        dev.save.assert_not_called()

    def test__merge_introspection_merges_all_the_things(self):
        dev = unittest.mock.Mock()
        dev.id = sentinel.device_id

        client, nb = self._create_backend()

        with contextlib.ExitStack() as stack:
            _read_interfaces = stack.enter_context(unittest.mock.patch.object(
                nb, "_read_interfaces"
            ))
            _read_interfaces.return_value = sentinel.interfaces

            _merge_interfaces = stack.enter_context(unittest.mock.patch.object(
                nb, "_merge_interfaces"
            ))

            _merge_comment = stack.enter_context(unittest.mock.patch.object(
                nb, "_merge_comment"
            ))
            _merge_comment.return_value = True

            nb._merge_introspection(
                dev,
                {
                    "inventory": {
                        "interfaces": sentinel.inv_interfaces,
                    },
                },
            )

        _read_interfaces.assert_called_once_with(dev.id)
        _merge_interfaces.assert_called_once_with(
            dev.id,
            sentinel.interfaces,
            sentinel.inv_interfaces,
        )

        _merge_comment.assert_called_once_with(
            dev,
            {"inventory": {"interfaces": sentinel.inv_interfaces}},
        )

        dev.save.assert_called_once_with()

    def test__get_ipmi_credentials(self):
        _, nb = self._create_backend(env={
            "NETBOX_IPMI_VAULT_MOUNTPOINT_TEMPLATE": "foo/{cluster}/kv",
            "NETBOX_IPMI_VAULT_PATH_TEMPLATE": "ipmi/{device.id}",
            "NETBOX_IPMI_VAULT_USERNAME_KEY": "the_username",
            "NETBOX_IPMI_VAULT_PASSWORD_KEY": "the_password",
        })

        device = unittest.mock.Mock()
        device.id = "foobar"

        vault_client = unittest.mock.Mock()
        vault_client.secrets.kv.read_secret_version.return_value = {
            "data": {"data": {
                "the_username": sentinel.username,
                "the_password": sentinel.password,
            }}
        }

        with contextlib.ExitStack() as stack:
            get_client = stack.enter_context(unittest.mock.patch(
                "metal_controller.vault.get_client",
                mock=unittest.mock.MagicMock(),
            ))
            get_client.return_value.__enter__.return_value = vault_client

            username, password = nb._get_ipmi_credentials(
                "some-yaook-cluster",
                device,
            )

        get_client.assert_called_once_with()
        vault_client.secrets.kv.read_secret_version.assert_called_once_with(
            mount_point="foo/some-yaook-cluster/kv",
            path="ipmi/foobar",
        )

        self.assertEqual(username, sentinel.username)
        self.assertEqual(password, sentinel.password)

    def test__get_device_returns_none_if_id_is_none(self):
        client, nb = self._create_backend()

        self.assertIsNone(nb._get_device(None))

        client.dcim.devices.get.assert_not_called()

    def test__get_device_raises_on_non_integer_id(self):
        client, nb = self._create_backend()

        with self.assertRaisesRegex(ValueError,
                                    "invalid device id: 'foobar'"):
            nb._get_device("foobar")

        client.dcim.devices.get.assert_not_called()

    def test__get_device_fetches_device_from_netbox_on_match(self):
        client, nb = self._create_backend()

        client.dcim.devices.get.return_value = sentinel.device

        result = nb._get_device("2342")

        client.dcim.devices.get.assert_called_once_with(2342)

        self.assertEqual(result, sentinel.device)

    @ddt.data(4, 6)
    def test__get_ip_address_in_vlan_matches_on_parent_prefix_vlan(
            self,
            family):
        client, nb = self._create_backend()

        iface = unittest.mock.Mock()
        vlan = unittest.mock.Mock()
        addr1 = unittest.mock.Mock()
        addr1.address = "1.2.3.4/16"

        addr2 = unittest.mock.Mock()
        addr2.address = "fd00:1:2:3:4:5::6/64"

        addr3 = unittest.mock.Mock()
        addr3.address = "1.2.3.5/16"

        addr4 = unittest.mock.Mock()
        addr4.address = "fd00:1:2:3:4:5::7/64"

        client.ipam.ip_addresses.filter.return_value = _as_generator([
            addr1,
            addr2,
            addr3,
            addr4,
        ])
        client.ipam.prefixes.filter.side_effect = _as_generator([
            _as_generator([]),
            _as_generator([sentinel.prefix1, sentinel.prefix2]),
        ])

        result = nb._get_ip_address_in_vlan(iface, vlan, family)

        client.ipam.ip_addresses.filter.assert_called_once_with(
            interface_id=iface.id,
        )

        self.assertCountEqual(
            [
                unittest.mock.call(
                    contains="1.2.3.4",
                    vlan_id=vlan.id,
                ),
                unittest.mock.call(
                    contains="1.2.3.5",
                    vlan_id=vlan.id,
                ),
            ] if family == 4 else [
                unittest.mock.call(
                    contains="fd00:1:2:3:4:5:0:6",
                    vlan_id=vlan.id,
                ),
                unittest.mock.call(
                    contains="fd00:1:2:3:4:5:0:7",
                    vlan_id=vlan.id,
                ),
            ],
            client.ipam.prefixes.filter.mock_calls,
        )

        self.assertEqual(addr3 if family == 4 else addr4, result)

    @ddt.data(4, 6)
    def test__get_ip_address_in_vlan_returns_false_on_no_match(
            self,
            family):
        client, nb = self._create_backend()

        iface = unittest.mock.Mock()
        vlan = unittest.mock.Mock()
        addr1 = unittest.mock.Mock()
        addr1.address = "1.2.3.4/16"

        addr2 = unittest.mock.Mock()
        addr2.address = "fd00:1:2:3:4:5::6/64"

        addr3 = unittest.mock.Mock()
        addr3.address = "1.2.3.5/16"

        addr4 = unittest.mock.Mock()
        addr4.address = "fd00:1:2:3:4:5::7/64"

        client.ipam.ip_addresses.filter.return_value = _as_generator([
            addr1,
            addr2,
            addr3,
            addr4,
        ])
        client.ipam.prefixes.filter.side_effect = _as_generator([
            _as_generator([]),
            _as_generator([]),
        ])

        result = nb._get_ip_address_in_vlan(iface, vlan, family)

        client.ipam.ip_addresses.filter.assert_called_once_with(
            interface_id=iface.id,
        )

        self.assertCountEqual(
            [
                unittest.mock.call(
                    contains="1.2.3.4",
                    vlan_id=vlan.id,
                ),
                unittest.mock.call(
                    contains="1.2.3.5",
                    vlan_id=vlan.id,
                ),
            ] if family == 4 else [
                unittest.mock.call(
                    contains="fd00:1:2:3:4:5:0:6",
                    vlan_id=vlan.id,
                ),
                unittest.mock.call(
                    contains="fd00:1:2:3:4:5:0:7",
                    vlan_id=vlan.id,
                ),
            ],
            client.ipam.prefixes.filter.mock_calls,
        )

        self.assertFalse(result)

    def test__find_interface_prefix_length(self):
        client, nb = self._create_backend()

        prefix1 = unittest.mock.Mock()
        prefix1.prefix = "10.2.0.0/16"

        prefix2 = unittest.mock.Mock()
        prefix2.prefix = "10.2.16.0/24"

        prefix3 = unittest.mock.Mock()
        prefix3.prefix = "10.2.16.128/25"

        client.ipam.prefixes.filter.return_value = _as_generator([
            prefix2,
            prefix1,
            prefix3,
        ])

        result = nb._find_interface_prefix_length(
            "10.2.16.178",
            sentinel.vlan_id,
        )

        client.ipam.prefixes.filter.assert_called_once_with(
            vlan_id=sentinel.vlan_id,
            contains="10.2.16.178",
        )

        self.assertEqual(result, 16)

    def test__autoallocate_ip_address_in_vlan_raises_if_no_prefix_exists(self):
        client, nb = self._create_backend()

        iface = unittest.mock.Mock()
        vlan = unittest.mock.Mock()

        client.ipam.prefixes.filter.return_value = _as_generator([])

        with contextlib.ExitStack() as stack:
            _get_ip_address_in_vlan = stack.enter_context(
                unittest.mock.patch.object(nb, "_get_ip_address_in_vlan")
            )
            _get_ip_address_in_vlan.return_value = None

            with self.assertRaisesRegex(
                    ValueError,
                    "no prefix declared with tag .* in vlan .*"):
                nb._autoallocate_ip_address_in_vlan(
                    iface,
                    vlan,
                    sentinel.family,
                    sentinel.hostfqdn,
                )

        client.ipam.prefixes.filter.assert_called_once_with(
            vlan_id=vlan.id,
            tag="yaook--ip-auto-assign",
            family=sentinel.family,
            status="active",
        )

    @ddt.data(
        # 1st value: true = on vlan, false = on prefix
        # 2nd value: the dns label as configured in netbox
        (True, "some-prefix"),
        (False, "other-prefix"),
        (True, ""),
        (False, ""),
        (True, None),
        (False, None),
        (True, "."),
        (False, "."),
    )
    @ddt.unpack
    def test__autoallocate_ip_address_in_vlan_creates_and_assigns_ip_from_first_pool_with_prefix_size_of_largest_parent_vlan_prefix(  # noqa:E501
            self,
            role_on_vlan,
            dns_label,
            ):
        client, nb = self._create_backend()

        hostfqdn = "foo.bar.example"

        iface = unittest.mock.Mock()
        vlan = unittest.mock.Mock()
        prefix_role = unittest.mock.Mock()

        def full_details_side_effect():
            prefix_role.custom_fields = {
                netbox.CF_DNS_LABEL: dns_label,
            }

        prefix_role.full_details.side_effect = full_details_side_effect

        ipaddress = unittest.mock.Mock()
        ipaddress.address = "1.2.3.4/28"
        ipaddress.dns_name = sentinel.unchanged

        def save_check():
            self.assertEqual(ipaddress.address, "1.2.3.4/16")
            self.assertEqual(ipaddress.assigned_object_type, "dcim.interface")
            self.assertEqual(ipaddress.assigned_object_id, iface.id)
            if dns_label == ".":
                # disable assignment
                self.assertEqual(ipaddress.dns_name, sentinel.unchanged)
            elif not dns_label:
                self.assertEqual(ipaddress.dns_name, hostfqdn)
            else:
                self.assertEqual(ipaddress.dns_name, f"{dns_label}.{hostfqdn}")

        ipaddress.save.side_effect = save_check

        prefix1 = unittest.mock.Mock()
        prefix1.available_ips.create.return_value = ipaddress

        if role_on_vlan:
            vlan.role = prefix_role
            prefix1.role = None
        else:
            vlan.role = None
            prefix1.role = prefix_role

        client.ipam.prefixes.filter.return_value = _as_generator([
            prefix1,
            sentinel.prefix2,
        ])

        with contextlib.ExitStack() as stack:
            _get_ip_address_in_vlan = stack.enter_context(
                unittest.mock.patch.object(nb, "_get_ip_address_in_vlan")
            )
            _get_ip_address_in_vlan.return_value = None

            _find_interface_prefix_length = stack.enter_context(
                unittest.mock.patch.object(nb, "_find_interface_prefix_length")
            )
            _find_interface_prefix_length.return_value = 16

            result = nb._autoallocate_ip_address_in_vlan(
                iface,
                vlan,
                sentinel.family,
                hostfqdn,
            )

        client.ipam.prefixes.filter.assert_called_once_with(
            vlan_id=vlan.id,
            tag="yaook--ip-auto-assign",
            family=sentinel.family,
            status="active",
        )

        prefix1.available_ips.create.assert_called_once_with()
        ipaddress.save.assert_called_once_with()
        prefix_role.full_details.assert_called_once_with()
        ipaddress.delete.assert_not_called()

        self.assertIs(result, ipaddress)

    def test__autoallocate_ip_address_in_vlan_does_nothing_if_allocated(self):
        client, nb = self._create_backend()

        iface = unittest.mock.Mock()
        vlan = unittest.mock.Mock()

        with contextlib.ExitStack() as stack:
            _get_ip_address_in_vlan = stack.enter_context(
                unittest.mock.patch.object(nb, "_get_ip_address_in_vlan")
            )
            _get_ip_address_in_vlan.return_value = sentinel.addr

            _find_interface_prefix_length = stack.enter_context(
                unittest.mock.patch.object(nb, "_find_interface_prefix_length")
            )
            _find_interface_prefix_length.return_value = 16

            result = nb._autoallocate_ip_address_in_vlan(
                iface,
                vlan,
                sentinel.family,
                sentinel.hostfqdn,
            )

        client.ipam.prefixes.filter.assert_not_called()

        self.assertIs(result, sentinel.addr)

    def test__autoallocate_ip_address_in_vlan_deletes_ip_if_save_fails(self):
        class FooException(Exception):
            pass

        client, nb = self._create_backend()

        iface = unittest.mock.Mock()
        prefix_role = unittest.mock.Mock()

        def full_details_side_effect():
            prefix_role.custom_fields = {
                netbox.CF_DNS_LABEL: "prefix",
            }

        prefix_role.full_details.side_effect = full_details_side_effect

        vlan = unittest.mock.Mock()
        ipaddress = unittest.mock.Mock()
        ipaddress.address = "fd00::/128"

        ipaddress.save.side_effect = FooException

        prefix1 = unittest.mock.Mock()
        prefix1.available_ips.create.return_value = ipaddress
        prefix1.role = prefix_role

        client.ipam.prefixes.filter.return_value = _as_generator([
            prefix1,
            sentinel.prefix2,
        ])

        with contextlib.ExitStack() as stack:
            _get_ip_address_in_vlan = stack.enter_context(
                unittest.mock.patch.object(nb, "_get_ip_address_in_vlan")
            )
            _get_ip_address_in_vlan.return_value = None

            _find_interface_prefix_length = stack.enter_context(
                unittest.mock.patch.object(nb, "_find_interface_prefix_length")
            )
            _find_interface_prefix_length.return_value = 16

            with self.assertRaises(FooException):
                nb._autoallocate_ip_address_in_vlan(
                    iface,
                    vlan,
                    sentinel.family,
                    sentinel.hostfqdn,
                )

        client.ipam.prefixes.filter.assert_called_once_with(
            vlan_id=vlan.id,
            tag="yaook--ip-auto-assign",
            family=sentinel.family,
            status="active",
        )

        prefix1.available_ips.create.assert_called_once_with()
        ipaddress.save.assert_called_once_with()
        ipaddress.delete.assert_called_once_with()

    def test__autoallocate_ip_address_in_vlan_falls_back_to_next_pool_on_allocation_error(self):  # noqa:E501
        class AllocException(pynetbox.core.query.AllocationError):
            def __init__(self):
                # moohaha
                pass

        client, nb = self._create_backend()

        iface = unittest.mock.Mock()
        vlan_role = unittest.mock.Mock()

        def full_details_side_effect():
            vlan_role.custom_fields = {
                netbox.CF_DNS_LABEL: "some-prefix",
            }

        vlan_role.full_details.side_effect = full_details_side_effect

        vlan = unittest.mock.Mock()
        vlan.role = vlan_role
        ipaddress = unittest.mock.Mock()
        ipaddress.address = "fd00::/128"

        prefix1 = unittest.mock.Mock()
        prefix1.role = None
        prefix1.available_ips.create.side_effect = AllocException

        prefix2 = unittest.mock.Mock()
        prefix2.role = None
        prefix2.available_ips.create.side_effect = AllocException

        prefix3 = unittest.mock.Mock()
        prefix3.role = None
        prefix3.available_ips.create.return_value = ipaddress

        client.ipam.prefixes.filter.return_value = _as_generator([
            prefix1,
            prefix2,
            prefix3,
        ])

        with contextlib.ExitStack() as stack:
            _get_ip_address_in_vlan = stack.enter_context(
                unittest.mock.patch.object(nb, "_get_ip_address_in_vlan")
            )
            _get_ip_address_in_vlan.return_value = None

            _find_interface_prefix_length = stack.enter_context(
                unittest.mock.patch.object(nb, "_find_interface_prefix_length")
            )
            _find_interface_prefix_length.return_value = 16

            result = nb._autoallocate_ip_address_in_vlan(
                iface,
                vlan,
                sentinel.family,
                sentinel.hostfqdn,
            )

        client.ipam.prefixes.filter.assert_called_once_with(
            vlan_id=vlan.id,
            tag="yaook--ip-auto-assign",
            family=sentinel.family,
            status="active",
        )

        prefix1.available_ips.create.assert_called_once_with()
        prefix2.available_ips.create.assert_called_once_with()
        prefix3.available_ips.create.assert_called_once_with()
        ipaddress.save.assert_called_once_with()
        ipaddress.delete.assert_not_called()

        self.assertIs(result, ipaddress)

    def test__autoallocate_ip_address_in_vlan_reraises_if_all_pools_are_full(self):  # noqa:E501
        class AllocException(pynetbox.core.query.AllocationError):
            def __init__(self):
                # moohaha
                pass

        client, nb = self._create_backend()

        iface = unittest.mock.Mock()
        vlan = unittest.mock.Mock()

        prefix1 = unittest.mock.Mock()
        prefix1.available_ips.create.side_effect = AllocException

        client.ipam.prefixes.filter.return_value = _as_generator([
            prefix1,
        ])

        with contextlib.ExitStack() as stack:
            _get_ip_address_in_vlan = stack.enter_context(
                unittest.mock.patch.object(nb, "_get_ip_address_in_vlan")
            )
            _get_ip_address_in_vlan.return_value = None

            _find_interface_prefix_length = stack.enter_context(
                unittest.mock.patch.object(nb, "_find_interface_prefix_length")
            )
            _find_interface_prefix_length.return_value = 16

            with self.assertRaises(AllocException):
                nb._autoallocate_ip_address_in_vlan(
                    iface,
                    vlan,
                    sentinel.family,
                    sentinel.hostfqdn,
                )

        client.ipam.prefixes.filter.assert_called_once_with(
            vlan_id=vlan.id,
            tag="yaook--ip-auto-assign",
            family=sentinel.family,
            status="active",
        )

        prefix1.available_ips.create.assert_called_once_with()

    @ddt.data(
        [True, "foo-tag"],
        [True, "bar-tag"],
        [False, "foo-tag"],
        [False, "bar-tag"],
    )
    @ddt.unpack
    def test__autoallocate_ip_addresses_for_iface_allocates_by_tag(
            self,
            untagged_allocatable,
            tag):
        client, nb = self._create_backend(env={
            "NETBOX_IP_ALLOCATE_TAG": tag,
        })

        matching_tag = unittest.mock.Mock()
        matching_tag.slug = tag

        unrelated_tag = unittest.mock.Mock()

        vlan1 = unittest.mock.Mock([])
        vlan1.tags = [matching_tag]

        vlan2 = unittest.mock.Mock([])
        vlan2.tags = [unrelated_tag]

        vlan3 = unittest.mock.Mock([])
        vlan3.tags = [matching_tag]

        iface = unittest.mock.Mock([])
        iface.untagged_vlan = vlan1 if untagged_allocatable else vlan2
        iface.tagged_vlans = [
            vlan2 if untagged_allocatable else vlan1,
            vlan3,
        ]

        with contextlib.ExitStack() as stack:
            _autoallocate_ip_address_in_vlan = stack.enter_context(
                unittest.mock.patch.object(
                    nb, "_autoallocate_ip_address_in_vlan",
                )
            )

            v4, v6 = nb._autoallocate_ip_addresses_for_iface(
                iface,
                sentinel.hostfqdn,
            )

        self.assertCountEqual(
            [
                unittest.mock.call(iface, vlan1, 4, sentinel.hostfqdn),
                unittest.mock.call(iface, vlan1, 6, sentinel.hostfqdn),
                unittest.mock.call(iface, vlan3, 4, sentinel.hostfqdn),
                unittest.mock.call(iface, vlan3, 6, sentinel.hostfqdn),
            ],
            _autoallocate_ip_address_in_vlan.mock_calls,
        )

        self.assertIsNone(v4)
        self.assertIsNone(v6)

    def test__autoallocate_ip_addresses_for_iface_handles_iface_without_untagged(  # noqa:E501
            self):
        client, nb = self._create_backend(env={
            "NETBOX_IP_ALLOCATE_TAG": "foo-tag",
        })

        matching_tag = unittest.mock.Mock()
        matching_tag.slug = "foo-tag"

        unrelated_tag = unittest.mock.Mock()

        vlan1 = unittest.mock.Mock([])
        vlan1.tags = [matching_tag]

        vlan2 = unittest.mock.Mock([])
        vlan2.tags = [unrelated_tag]

        vlan3 = unittest.mock.Mock([])
        vlan3.tags = [matching_tag]

        iface = unittest.mock.Mock([])
        iface.untagged_vlan = None
        iface.tagged_vlans = [
            vlan1,
            vlan2,
            vlan3,
        ]

        with contextlib.ExitStack() as stack:
            _autoallocate_ip_address_in_vlan = stack.enter_context(
                unittest.mock.patch.object(
                    nb, "_autoallocate_ip_address_in_vlan",
                )
            )

            v4, v6 = nb._autoallocate_ip_addresses_for_iface(
                iface,
                sentinel.hostfqdn,
            )

        self.assertCountEqual(
            [
                unittest.mock.call(iface, vlan1, 4, sentinel.hostfqdn),
                unittest.mock.call(iface, vlan1, 6, sentinel.hostfqdn),
                unittest.mock.call(iface, vlan3, 4, sentinel.hostfqdn),
                unittest.mock.call(iface, vlan3, 6, sentinel.hostfqdn),
            ],
            _autoallocate_ip_address_in_vlan.mock_calls,
        )

        self.assertIsNone(v4)
        self.assertIsNone(v6)

    @ddt.data(
        (True, "bar-tag"),
        (True, "baz-tag"),
        (False, "bar-tag"),
        (False, "baz-tag"),
    )
    @ddt.unpack
    def test__autoallocate_ip_addresses_for_iface_returns_primary_ip_if_tagged(
            self,
            primary_untagged,
            primary_tag_slug):
        client, nb = self._create_backend(env={
            "NETBOX_IP_ALLOCATE_TAG": "foo-tag",
            "NETBOX_IP_PRIMARY_TAG": primary_tag_slug,
        })

        matching_tag = unittest.mock.Mock()
        matching_tag.slug = "foo-tag"

        primary_tag = unittest.mock.Mock()
        primary_tag.slug = primary_tag_slug

        vlan1 = unittest.mock.Mock([])
        vlan1.tags = [matching_tag, primary_tag]

        vlan2 = unittest.mock.Mock([])
        vlan2.tags = [matching_tag]

        iface = unittest.mock.Mock([])
        iface.untagged_vlan = vlan1 if primary_untagged else vlan2
        iface.tagged_vlans = [
            vlan2 if primary_untagged else vlan1
        ]

        def ipaddr_gen(_, vlan, family, _fqdn):
            if vlan == vlan1:
                if family == 6:
                    return sentinel.correct_ip6
                else:
                    return sentinel.correct_ip4
            return sentinel.wrong_ip

        with contextlib.ExitStack() as stack:
            _autoallocate_ip_address_in_vlan = stack.enter_context(
                unittest.mock.patch.object(
                    nb, "_autoallocate_ip_address_in_vlan",
                )
            )
            _autoallocate_ip_address_in_vlan.side_effect = ipaddr_gen

            v4, v6 = nb._autoallocate_ip_addresses_for_iface(
                iface,
                sentinel.hostfqdn,
            )

        self.assertCountEqual(
            [
                unittest.mock.call(iface, vlan1, 4, sentinel.hostfqdn),
                unittest.mock.call(iface, vlan1, 6, sentinel.hostfqdn),
                unittest.mock.call(iface, vlan2, 4, sentinel.hostfqdn),
                unittest.mock.call(iface, vlan2, 6, sentinel.hostfqdn),
            ],
            _autoallocate_ip_address_in_vlan.mock_calls,
        )

        self.assertEqual(v4, sentinel.correct_ip4)
        self.assertEqual(v6, sentinel.correct_ip6)

    def test__autoallocate_ip_addresses_for_iface_support_split_v4_v6(self):
        client, nb = self._create_backend(env={
            "NETBOX_IP_ALLOCATE_TAG": "foo-tag",
            "NETBOX_IP_PRIMARY_TAG": "primary",
        })

        matching_tag = unittest.mock.Mock()
        matching_tag.slug = "foo-tag"

        primary_tag = unittest.mock.Mock()
        primary_tag.slug = "primary"

        vlan1 = unittest.mock.Mock([])
        vlan1.tags = [matching_tag, primary_tag]

        vlan2 = unittest.mock.Mock([])
        vlan2.tags = [matching_tag, primary_tag]

        iface = unittest.mock.Mock([])
        iface.untagged_vlan = None
        iface.tagged_vlans = [vlan1, vlan2]

        def ipaddr_gen(_, vlan, family, _fqdn):
            if family == 6 and vlan == vlan1:
                return sentinel.correct_ip6
            elif family == 4 and vlan == vlan2:
                return sentinel.correct_ip4
            return None

        with contextlib.ExitStack() as stack:
            _autoallocate_ip_address_in_vlan = stack.enter_context(
                unittest.mock.patch.object(
                    nb, "_autoallocate_ip_address_in_vlan",
                )
            )
            _autoallocate_ip_address_in_vlan.side_effect = ipaddr_gen

            v4, v6 = nb._autoallocate_ip_addresses_for_iface(
                iface,
                sentinel.hostfqdn,
            )

        self.assertCountEqual(
            [
                unittest.mock.call(iface, vlan1, 4, sentinel.hostfqdn),
                unittest.mock.call(iface, vlan1, 6, sentinel.hostfqdn),
                unittest.mock.call(iface, vlan2, 4, sentinel.hostfqdn),
                unittest.mock.call(iface, vlan2, 6, sentinel.hostfqdn),
            ],
            _autoallocate_ip_address_in_vlan.mock_calls,
        )

        self.assertEqual(v4, sentinel.correct_ip4)
        self.assertEqual(v6, sentinel.correct_ip6)

    def test__autoallocate_ip_addresses_for_device_allocates_for_all_ifaces_with_fqdn(self):  # noqa:E501
        client, nb = self._create_backend()

        fqdn = "foo-host.node.cluster.domain"

        device = unittest.mock.Mock()
        device.id = sentinel.device_id
        device.name = "foo-host"

        client.dcim.interfaces.filter.return_value = [
            sentinel.iface1,
            sentinel.iface2,
            sentinel.iface3,
        ]

        with contextlib.ExitStack() as stack:
            _autoallocate_ip_addresses_for_iface = stack.enter_context(
                unittest.mock.patch.object(
                    nb, "_autoallocate_ip_addresses_for_iface",
                )
            )
            _autoallocate_ip_addresses_for_iface.return_value = None, None

            _read_cluster = stack.enter_context(unittest.mock.patch.object(
                nb, "_read_cluster",
            ))
            _read_cluster.return_value = "cluster.domain"

            nb._autoallocate_ip_addresses_for_device(device)

        client.dcim.interfaces.filter.assert_called_once_with(
            device_id=sentinel.device_id,
        )

        self.assertCountEqual(
            [
                unittest.mock.call(sentinel.iface1, fqdn),
                unittest.mock.call(sentinel.iface2, fqdn),
                unittest.mock.call(sentinel.iface3, fqdn),
            ],
            _autoallocate_ip_addresses_for_iface.mock_calls,
        )

        device.save.assert_not_called()

    def test__autoallocate_ip_addresses_for_device_assigns_primary(self):
        client, nb = self._create_backend()

        device = unittest.mock.Mock()
        device.id = sentinel.device_id

        def checked_save():
            self.assertEqual(device.primary_ip4, prim_v4.id)
            self.assertEqual(device.primary_ip6, prim_v6.id)

        device.save.side_effect = checked_save

        prim_v4 = unittest.mock.Mock()
        prim_v6 = unittest.mock.Mock()

        client.dcim.interfaces.filter.return_value = [
            sentinel.iface1,
            sentinel.iface2,
            sentinel.iface3,
        ]

        with contextlib.ExitStack() as stack:
            _autoallocate_ip_addresses_for_iface = stack.enter_context(
                unittest.mock.patch.object(
                    nb, "_autoallocate_ip_addresses_for_iface",
                )
            )
            _autoallocate_ip_addresses_for_iface.side_effect = _as_generator([
                (None, None),
                (prim_v4, None),
                (None, prim_v6),
            ])

            nb._autoallocate_ip_addresses_for_device(device)

        client.dcim.interfaces.filter.assert_called_once_with(
            device_id=sentinel.device_id,
        )

        self.assertCountEqual(
            [
                unittest.mock.call(sentinel.iface1, unittest.mock.ANY),
                unittest.mock.call(sentinel.iface2, unittest.mock.ANY),
                unittest.mock.call(sentinel.iface3, unittest.mock.ANY),
            ],
            _autoallocate_ip_addresses_for_iface.mock_calls,
        )

        device.save.assert_called_once_with()

    def test__validate_role_rejects_role_without_hostname_prefix(self):
        client, nb = self._create_backend()

        role = unittest.mock.Mock()
        role.custom_fields = {
            netbox.CF_HOSTNAME_PREFIX: "",
            netbox.CF_SCHEDULING_KEYS: "",
            netbox.CF_CONTROL_PLANE: False,
            netbox.CF_YAOOK_KUBERNETES_NODE: "true",
        }

        self.assertFalse(nb._validate_role(role))

    @ddt.data(netbox.CF_CONTROL_PLANE,
              netbox.CF_HOSTNAME_PREFIX,
              netbox.CF_SCHEDULING_KEYS)
    def test__validate_role_raises_for_role_without_some_custom_field(
            self,
            missing_field):
        client, nb = self._create_backend()

        role = unittest.mock.Mock()
        role.id = 2342
        role.name = "foo role"
        role.custom_fields = {
            netbox.CF_HOSTNAME_PREFIX: "foo",
            netbox.CF_SCHEDULING_KEYS: "",
            netbox.CF_CONTROL_PLANE: False,
            netbox.CF_YAOOK_KUBERNETES_NODE: "true",
        }

        del role.custom_fields[missing_field]

        with self.assertRaisesRegex(
                ValueError,
                r"role foo role\(2342\) is missing the custom field .*"):
            nb._validate_role(role)

    def test__validate_role_accepts_complete_role(self):
        client, nb = self._create_backend()

        role = unittest.mock.Mock()
        role.custom_fields = {
            netbox.CF_HOSTNAME_PREFIX: "foo",
            netbox.CF_SCHEDULING_KEYS: "",
            netbox.CF_CONTROL_PLANE: False,
            netbox.CF_YAOOK_KUBERNETES_NODE: "true",
        }

        self.assertTrue(nb._validate_role(role))

    def test__autoassign_hostname_is_noop_if_already_assigned(self):
        client, nb = self._create_backend()

        device = unittest.mock.Mock()
        device.name = "foobar"

        with contextlib.ExitStack() as stack:
            gen = stack.enter_context(unittest.mock.patch(
                "metal_controller.netbox.gen_cvcv_Nnnn",
            ))

            nb._autoassign_hostname(device)

        gen.assert_not_called()

        self.assertEqual(device.name, "foobar")
        device.save.assert_not_called()

    def test__autoassign_hostname_raises_if_assigned_name_is_not_a_valid_hostname(self):  # noqa:E501
        client, nb = self._create_backend()

        device = unittest.mock.Mock()
        device.name = "foobar baz"

        with contextlib.ExitStack() as stack:
            gen = stack.enter_context(unittest.mock.patch(
                "metal_controller.netbox.gen_cvcv_Nnnn",
            ))

            with self.assertRaisesRegex(
                    ValueError,
                    "'foobar baz' is not a valid hostname"):
                nb._autoassign_hostname(device)

        gen.assert_not_called()
        device.save.assert_not_called()

    def test__autoassign_hostname_generates_hostname_from_generator_with_prefix_from_role(self):  # noqa:E501
        client, nb = self._create_backend()

        device = unittest.mock.Mock()
        device.name = None

        role = unittest.mock.Mock()

        def full_details_side_effect():
            role.custom_fields = {netbox.CF_HOSTNAME_PREFIX: "<prefix>"}

        role.full_details.side_effect = full_details_side_effect

        def save_check():
            self.assertEqual(device.name, "<prefix>-<random part>")

        device.role = role
        device.save.side_effect = save_check

        with contextlib.ExitStack() as stack:
            gen = stack.enter_context(unittest.mock.patch(
                "metal_controller.netbox.gen_cvcv_Nnnn",
            ))
            gen.return_value = "<random part>"

            nb._autoassign_hostname(device)

        role.full_details.assert_called_once_with()
        gen.assert_called_once_with()
        device.save.assert_called_once_with()

    def test__autoassign_hostname_tries_with_another_hostname_if_save_fails(self):  # noqa:E501
        last_generated_name = None
        ninvoke = 0

        def generate_name():
            nonlocal last_generated_name
            for i in range(3):
                name = f"foo{i}"
                last_generated_name = name
                yield name
            raise AssertionError

        client, nb = self._create_backend()

        device = unittest.mock.Mock()
        device.name = None

        role = unittest.mock.Mock()

        def full_details_side_effect():
            role.custom_fields = {netbox.CF_HOSTNAME_PREFIX: "<prefix>"}

        role.full_details.side_effect = full_details_side_effect

        def save_check():
            nonlocal ninvoke
            ninvoke += 1
            self.assertEqual(device.name, f"<prefix>-{last_generated_name}")
            if ninvoke < 3:
                raise pynetbox.core.query.RequestError(unittest.mock.Mock())

        device.role = role
        device.save.side_effect = save_check

        with contextlib.ExitStack() as stack:
            gen = stack.enter_context(unittest.mock.patch(
                "metal_controller.netbox.gen_cvcv_Nnnn",
            ))
            gen.side_effect = generate_name()

            nb._autoassign_hostname(device)

        role.full_details.assert_called_once_with()
        self.assertCountEqual(
            [
                unittest.mock.call(),
            ]*3,
            gen.mock_calls,
        )

        self.assertCountEqual(
            [
                unittest.mock.call(),
            ]*3,
            device.save.mock_calls,
        )

    def test__autoassign_hostname_gives_up_after_three_attempts(self):
        client, nb = self._create_backend()

        device = unittest.mock.Mock()
        device.id = 2342
        device.name = None

        role = unittest.mock.Mock()

        def full_details_side_effect():
            role.custom_fields = {netbox.CF_HOSTNAME_PREFIX: "<prefix>"}

        role.full_details.side_effect = full_details_side_effect

        device.role = role
        device.save.side_effect = \
            pynetbox.core.query.RequestError(unittest.mock.Mock())

        with contextlib.ExitStack() as stack:
            gen = stack.enter_context(unittest.mock.patch(
                "metal_controller.netbox.gen_cvcv_Nnnn",
            ))
            gen.return_value = "<random part>"

            with self.assertRaisesRegex(
                    RuntimeError,
                    "failed to generate unique hostname for device 2342 after "
                    "three attempts",
                    ):
                nb._autoassign_hostname(device)

        role.full_details.assert_called_once_with()

    def test_enroll_node_updates_introspection_data_for_matching_status_and_manages(self):  # noqa:E501
        client, nb = self._create_backend(env={
            "NETBOX_MANAGEABLE_STATUSES": "foobar",
        })

        device = unittest.mock.Mock()
        device.id = 2342
        device.status.value = "foobar"
        device.role.custom_fields.yaook_kubernetes_node = True

        brief = unittest.mock.Mock()
        brief.backend_node_id = "2342"

        introspection = {
            "ipmi_address": sentinel.ipmi_address,
        }

        with contextlib.ExitStack() as stack:
            _find_node_in_netbox = stack.enter_context(
                unittest.mock.patch.object(nb, "_find_node_in_netbox")
            )
            _find_node_in_netbox.return_value = device

            _merge_introspection = stack.enter_context(
                unittest.mock.patch.object(nb, "_merge_introspection")
            )

            _read_cluster = stack.enter_context(
                unittest.mock.patch.object(nb, "_read_cluster")
            )
            _read_cluster.return_value = "some-cluster"

            _read_node_type = stack.enter_context(
                unittest.mock.patch.object(nb, "_read_node_type")
            )

            _read_node_type.return_value = True

            _get_ipmi_credentials = stack.enter_context(
                unittest.mock.patch.object(nb, "_get_ipmi_credentials")
            )
            _get_ipmi_credentials.return_value = (
                sentinel.ipmi_username, sentinel.ipmi_password,
            )

            _autoassign_hostname = stack.enter_context(
                unittest.mock.patch.object(nb, "_autoassign_hostname"),
            )

            _set_error = stack.enter_context(
                unittest.mock.patch.object(nb, "_set_error")
            )

            _clear_error = stack.enter_context(
                unittest.mock.patch.object(nb, "_clear_error")
            )

            _validate_role = stack.enter_context(
                unittest.mock.patch.object(nb, "_validate_role")
            )
            _validate_role.return_value = True

            result = nb.enroll(
                brief,
                introspection,
            )

        _read_cluster.assert_called_once_with(device)
        _read_node_type.assert_called_once_with(device)
        _merge_introspection.assert_called_once_with(
            device,
            introspection,
        )
        _get_ipmi_credentials.assert_called_once_with(
            "some-cluster",
            device,
        )
        _clear_error.assert_not_called()
        _set_error.assert_not_called()
        _autoassign_hostname.assert_called_once_with(device)

        self.assertIsInstance(result, backend.ManageAction)
        self.assertEqual(result.name, device.name)
        self.assertTrue(result.description)
        self.assertEqual(result.backend_node_id, str(device.id))
        self.assertEqual(result.driver, "ipmi")
        self.assertEqual(result.driver_info["ipmi_address"],
                         sentinel.ipmi_address)
        self.assertEqual(result.driver_info["ipmi_username"],
                         sentinel.ipmi_username)
        self.assertEqual(result.driver_info["ipmi_password"],
                         sentinel.ipmi_password)

    def test_manages_redfish(self):
        client, nb = self._create_backend(env={
            "NETBOX_MANAGEABLE_STATUSES": "foobar",
        })

        device = unittest.mock.Mock()
        device.id = 2342
        device.status.value = "foobar"
        device.role.custom_fields.yaook_kubernetes_node = True
        device.device_type.custom_fields. \
            yaook_bmc_management_method = "redfish"

        brief = unittest.mock.Mock()
        brief.backend_node_id = "2342"

        introspection = {
            "ipmi_address": sentinel.ipmi_address,
        }

        with contextlib.ExitStack() as stack:
            _find_node_in_netbox = stack.enter_context(
                unittest.mock.patch.object(nb, "_find_node_in_netbox")
            )
            _find_node_in_netbox.return_value = device

            _merge_introspection = stack.enter_context( # noqa
                unittest.mock.patch.object(nb, "_merge_introspection")
            )

            _read_cluster = stack.enter_context(
                unittest.mock.patch.object(nb, "_read_cluster")
            )
            _read_cluster.return_value = "some-cluster"

            _read_node_type = stack.enter_context(
                unittest.mock.patch.object(nb, "_read_node_type")
            )

            _read_node_type.return_value = True

            _get_ipmi_credentials = stack.enter_context(
                unittest.mock.patch.object(nb, "_get_ipmi_credentials")
            )
            _get_ipmi_credentials.return_value = (
                sentinel.ipmi_username, sentinel.ipmi_password,
            )

            _autoassign_hostname = stack.enter_context( # noqa
                unittest.mock.patch.object(nb, "_autoassign_hostname"),
            )

            _set_error = stack.enter_context( # noqa
                unittest.mock.patch.object(nb, "_set_error")
            )

            _clear_error = stack.enter_context( # noqa
                unittest.mock.patch.object(nb, "_clear_error")
            )

            _validate_role = stack.enter_context(
                unittest.mock.patch.object(nb, "_validate_role")
            )
            _validate_role.return_value = True

            _get_bmc_management_type = stack.enter_context(
                unittest.mock.patch.object(nb, "_get_bmc_management_type")
            )
            _get_bmc_management_type.return_value = "redfish"

            _dns_reverse_lookup = stack.enter_context(
                unittest.mock.patch.object(nb, "_dns_reverse_lookup")
            )
            _dns_reverse_lookup.return_value = introspection['ipmi_address']

            result = nb.enroll(
                brief,
                introspection,
            )

        self.assertIsInstance(result, backend.ManageAction)
        self.assertEqual(result.name, device.name)
        self.assertTrue(result.description)
        self.assertEqual(result.backend_node_id, str(device.id))
        self.assertEqual(result.driver, "redfish")
        self.assertEqual(result.driver_info["redfish_address"],
                         f"https://{sentinel.ipmi_address}")
        self.assertEqual(result.driver_info["redfish_username"],
                         sentinel.ipmi_username)
        self.assertEqual(result.driver_info["redfish_password"],
                         sentinel.ipmi_password)
        self.assertFalse(result.driver_info["redfish_verify_ca"])

    def test_manages_redfish_with_ipmi_address_missing(self):
        client, nb = self._create_backend(env={
            "NETBOX_MANAGEABLE_STATUSES": "foobar",
        })

        device = unittest.mock.Mock()
        device.id = 2342
        device.status.value = "foobar"
        device.role.custom_fields.yaook_kubernetes_node = True
        device.device_type.custom_fields. \
            yaook_bmc_management_method = "redfish"
        device.config_context = {
            "metal-controller": {
                "bmc_dns_suffix": "oh.my.bmc"
            }
        }

        brief = unittest.mock.Mock()
        brief.backend_node_id = "2342"

        introspection = {
            "ipmi_address": "",
            "inventory": {
                "system_vendor": {
                    "manufacturer": "HPE",
                    "serial_number": "CZSHWKDFZFGH"
                }
            }
        }

        with contextlib.ExitStack() as stack:
            _find_node_in_netbox = stack.enter_context(
                unittest.mock.patch.object(nb, "_find_node_in_netbox")
            )
            _find_node_in_netbox.return_value = device

            _merge_introspection = stack.enter_context( # noqa
                unittest.mock.patch.object(nb, "_merge_introspection")
            )

            _read_cluster = stack.enter_context(
                unittest.mock.patch.object(nb, "_read_cluster")
            )
            _read_cluster.return_value = "some-cluster"

            _get_ipmi_credentials = stack.enter_context(
                unittest.mock.patch.object(nb, "_get_ipmi_credentials")
            )
            _get_ipmi_credentials.return_value = (
                sentinel.ipmi_username, sentinel.ipmi_password,
            )

            _autoassign_hostname = stack.enter_context( # noqa
                unittest.mock.patch.object(nb, "_autoassign_hostname"),
            )

            _set_error = stack.enter_context( # noqa
                unittest.mock.patch.object(nb, "_set_error")
            )

            _clear_error = stack.enter_context( # noqa
                unittest.mock.patch.object(nb, "_clear_error")
            )

            _validate_role = stack.enter_context(
                unittest.mock.patch.object(nb, "_validate_role")
            )
            _validate_role.return_value = True

            _get_bmc_management_type = stack.enter_context(
                unittest.mock.patch.object(nb, "_get_bmc_management_type")
            )
            _get_bmc_management_type.return_value = "redfish"

            gethostbyaddr = stack.enter_context(
                unittest.mock.patch.object(socket, "gethostbyaddr")
            )
            gethostbyaddr.return_value = [
                sentinel.ipmi_address, "dummy2", [sentinel.ipmi_address]
            ]

            # _dns_reverse_lookup = stack.enter_context(
            #     unittest.mock.patch.object(nb, "_dns_reverse_lookup")
            # )
            # _dns_reverse_lookup.return_value = sentinel

            result = nb.enroll(
                brief,
                introspection,
            )

        self.assertIsInstance(result, backend.ManageAction)
        self.assertEqual(result.name, device.name)
        self.assertTrue(result.description)
        self.assertEqual(result.backend_node_id, str(device.id))
        self.assertEqual(result.driver, "redfish")
        self.assertEqual(result.driver_info["redfish_address"],
                         f"https://{sentinel.ipmi_address}")
        self.assertEqual(result.driver_info["redfish_username"],
                         sentinel.ipmi_username)
        self.assertEqual(result.driver_info["redfish_password"],
                         sentinel.ipmi_password)
        self.assertFalse(result.driver_info["redfish_verify_ca"])

    def test_enroll_node_updates_introspection_data_for_mismatching_status_and_passes(self):  # noqa:E501
        client, nb = self._create_backend(env={
            "NETBOX_MANAGEABLE_STATUSES": "foobar",
        })

        device = unittest.mock.Mock()
        device.id = 2342
        device.status.value = "not-foobar"
        device.role.custom_fields.yaook_kubernetes_node = True

        brief = unittest.mock.Mock()
        brief.backend_node_id = "2342"

        introspection = {
            "ipmi_address": sentinel.ipmi_address,
        }

        with contextlib.ExitStack() as stack:
            _find_node_in_netbox = stack.enter_context(
                unittest.mock.patch.object(nb, "_find_node_in_netbox")
            )
            _find_node_in_netbox.return_value = device

            _merge_introspection = stack.enter_context(
                unittest.mock.patch.object(nb, "_merge_introspection")
            )

            _read_cluster = stack.enter_context(
                unittest.mock.patch.object(nb, "_read_cluster")
            )
            _read_cluster.return_value = "some-cluster"

            _read_node_type = stack.enter_context(
                unittest.mock.patch.object(nb, "_read_node_type")
            )

            _read_node_type.return_value = True

            _get_ipmi_credentials = stack.enter_context(
                unittest.mock.patch.object(nb, "_get_ipmi_credentials")
            )
            _get_ipmi_credentials.return_value = (
                sentinel.ipmi_username, sentinel.ipmi_password,
            )

            _autoassign_hostname = stack.enter_context(
                unittest.mock.patch.object(nb, "_autoassign_hostname"),
            )

            _validate_role = stack.enter_context(
                unittest.mock.patch.object(nb, "_validate_role")
            )
            _validate_role.return_value = True

            _set_error = stack.enter_context(
                unittest.mock.patch.object(nb, "_set_error")
            )

            _clear_error = stack.enter_context(
                unittest.mock.patch.object(nb, "_clear_error")
            )

            result = nb.enroll(
                brief,
                introspection,
            )

        _read_cluster.assert_called_once_with(device)
        _read_node_type.assert_called_once_with(device)
        _merge_introspection.assert_called_once_with(
            device,
            introspection,
        )
        _get_ipmi_credentials.assert_not_called()
        _clear_error.assert_not_called()
        _set_error.assert_not_called()
        _autoassign_hostname.assert_not_called()

        self.assertIsInstance(result, backend.PassAction)
        self.assertRegex(result.message,
                         r"status 'not-foobar' is not enrollable")

    def test_enroll_node_ignores_device_without_cluster(self):
        client, nb = self._create_backend(env={
            "NETBOX_MANAGEABLE_STATUSES": "foobar",
        })

        device = unittest.mock.Mock()
        device.id = 2342
        device.status.value = "foobar"
        device.role.custom_fields.yaook_kubernetes_node = True

        brief = unittest.mock.Mock()
        brief.backend_node_id = "2342"

        with contextlib.ExitStack() as stack:
            _find_node_in_netbox = stack.enter_context(
                unittest.mock.patch.object(nb, "_find_node_in_netbox")
            )
            _find_node_in_netbox.return_value = device

            _merge_introspection = stack.enter_context(
                unittest.mock.patch.object(nb, "_merge_introspection")
            )

            _read_cluster = stack.enter_context(
                unittest.mock.patch.object(nb, "_read_cluster")
            )
            _read_cluster.return_value = ""

            _read_node_type = stack.enter_context(
                unittest.mock.patch.object(nb, "_read_node_type")
            )

            _read_node_type.return_value = True

            result = nb.enroll(
                brief,
                sentinel.introspection,
            )

        _read_cluster.assert_called_once_with(device)
        _read_node_type.assert_called_once_with(device)
        _merge_introspection.assert_called_once_with(
            device,
            sentinel.introspection,
        )

        self.assertIsInstance(result, backend.PassAction)
        self.assertEqual(result.backend_node_id, str(device.id))

    def test_enroll_node_sets_error_on_invalid_role(self):
        client, nb = self._create_backend(env={
            "NETBOX_MANAGEABLE_STATUSES": "foobar",
        })

        device = unittest.mock.Mock()
        device.id = 2342
        device.status.value = "foobar"
        device.role.custom_fields.yaook_kubernetes_node = True

        brief = unittest.mock.Mock()
        brief.backend_node_id = "2342"

        with contextlib.ExitStack() as stack:
            _find_node_in_netbox = stack.enter_context(
                unittest.mock.patch.object(nb, "_find_node_in_netbox")
            )
            _find_node_in_netbox.return_value = device

            _merge_introspection = stack.enter_context(
                unittest.mock.patch.object(nb, "_merge_introspection")
            )

            _read_cluster = stack.enter_context(
                unittest.mock.patch.object(nb, "_read_cluster")
            )
            _read_cluster.return_value = "some-cluster"

            _read_node_type = stack.enter_context(
                unittest.mock.patch.object(nb, "_read_node_type")
            )

            _read_node_type.return_value = True

            _validate_role = stack.enter_context(
                unittest.mock.patch.object(nb, "_validate_role")
            )
            _validate_role.return_value = False

            _set_error = stack.enter_context(
                unittest.mock.patch.object(nb, "_set_error")
            )

            result = nb.enroll(
                brief,
                sentinel.introspection,
            )

        _read_cluster.assert_called_once_with(device)
        _read_node_type.assert_called_once_with(device)
        _validate_role.assert_called_once_with(device.role)
        _merge_introspection.assert_called_once_with(
            device,
            sentinel.introspection,
        )
        _set_error.assert_called_once_with(
            device,
            unittest.mock.ANY,
        )

        self.assertRegex(_set_error.mock_calls[0][1][1],
                         r"role .* is incomplete; ensure that the hostname "
                         r"prefix is set correctly")

        self.assertIsInstance(result, backend.ErrorAction)
        self.assertEqual(result.backend_node_id, str(device.id))

        self.assertRegex(result.message, r"role is invalid")

    def test_enroll_node_raises_for_missing_custom_field(self):
        client, nb = self._create_backend(env={
            "NETBOX_MANAGEABLE_STATUSES": "foobar",
        })

        device = unittest.mock.Mock()
        device.id = 2342
        device.status.value = "foobar"
        device.role.custom_fields.yaook_kubernetes_node = True

        brief = unittest.mock.Mock()
        brief.backend_node_id = "2342"

        with contextlib.ExitStack() as stack:
            _find_node_in_netbox = stack.enter_context(
                unittest.mock.patch.object(nb, "_find_node_in_netbox")
            )
            _find_node_in_netbox.return_value = device

            stack.enter_context(
                unittest.mock.patch.object(nb, "_merge_introspection")
            )

            _read_cluster = stack.enter_context(
                unittest.mock.patch.object(nb, "_read_cluster")
            )
            _read_cluster.side_effect = KeyError

            with self.assertRaisesRegex(
                    RuntimeError,
                    r"cluster domain field .* not found"):
                nb.enroll(
                    brief,
                    sentinel.introspection,
                )

        _read_cluster.assert_called_once_with(device)

    def test_enroll_node_handles_missing_device(self):
        client, nb = self._create_backend(env={
            "NETBOX_MANAGEABLE_STATUSES": "foobar",
        })

        with contextlib.ExitStack() as stack:
            _find_node_in_netbox = stack.enter_context(
                unittest.mock.patch.object(nb, "_find_node_in_netbox")
            )
            _find_node_in_netbox.return_value = None

            _merge_introspection = stack.enter_context(
                unittest.mock.patch.object(nb, "_merge_introspection")
            )

            _read_cluster = stack.enter_context(
                unittest.mock.patch.object(nb, "_read_cluster")
            )
            _read_cluster.side_effect = KeyError

            result = nb.enroll(
                sentinel.brief,
                sentinel.introspection,
            )

        _merge_introspection.assert_not_called()
        _read_cluster.assert_not_called()

        self.assertIsInstance(result, backend.ErrorAction)
        self.assertRegex(result.message, "node not found in backend")
        self.assertIsNone(result.backend_node_id)

    def test_failure_sets_failed_state_on_node(self):
        _, nb = self._create_backend()

        brief = unittest.mock.Mock()
        brief.backend_node_id = sentinel.backend_node_id
        brief.intended_state = "some state"

        with contextlib.ExitStack() as stack:
            _get_device = stack.enter_context(unittest.mock.patch.object(
                nb, "_get_device",
            ))
            _get_device.return_value = sentinel.device

            _set_error = stack.enter_context(unittest.mock.patch.object(
                nb, "_set_error",
            ))

            nb.failure(
                brief,
                "some error",
            )

        _get_device.assert_called_once_with(sentinel.backend_node_id)
        _set_error.assert_called_once_with(
            sentinel.device,
            unittest.mock.ANY,
        )

        _, (_, msg), _ = _set_error.mock_calls[0]

        self.assertRegex(
            msg,
            r"Ironic failed to reach state `some state`:."
            r"*\n.*\n.*some error.*\n",
        )

    def test_failure_handles_nonexistent_device(self):
        _, nb = self._create_backend()

        brief = unittest.mock.Mock()
        brief.backend_node_id = sentinel.backend_node_id
        brief.intended_state = "some state"

        with contextlib.ExitStack() as stack:
            _get_device = stack.enter_context(unittest.mock.patch.object(
                nb, "_get_device",
            ))
            _get_device.return_value = None

            _set_error = stack.enter_context(unittest.mock.patch.object(
                nb, "_set_error",
            ))

            nb.failure(
                brief,
                "some error",
            )

        _get_device.assert_called_once_with(sentinel.backend_node_id)
        _set_error.assert_not_called()

    def test_failure_handles_invalid_device_id(self):
        _, nb = self._create_backend()

        brief = unittest.mock.Mock()
        brief.backend_node_id = sentinel.backend_node_id
        brief.intended_state = "some state"

        with contextlib.ExitStack() as stack:
            _get_device = stack.enter_context(unittest.mock.patch.object(
                nb, "_get_device",
            ))
            _get_device.side_effect = ValueError

            _set_error = stack.enter_context(unittest.mock.patch.object(
                nb, "_set_error",
            ))

            nb.failure(
                brief,
                "some error",
            )

        _get_device.assert_called_once_with(sentinel.backend_node_id)
        _set_error.assert_not_called()

    def test_success_sets_failed_state_on_node(self):
        _, nb = self._create_backend()

        brief = unittest.mock.Mock()
        brief.backend_node_id = sentinel.backend_node_id
        brief.intended_state = "some state"

        with contextlib.ExitStack() as stack:
            _get_device = stack.enter_context(unittest.mock.patch.object(
                nb, "_get_device",
            ))
            _get_device.return_value = sentinel.device

            _clear_error = stack.enter_context(unittest.mock.patch.object(
                nb, "_clear_error",
            ))

            nb.success(brief)

        _get_device.assert_called_once_with(sentinel.backend_node_id)
        _clear_error.assert_called_once_with(sentinel.device)

    def test_success_handles_nonexistent_device(self):
        _, nb = self._create_backend()

        brief = unittest.mock.Mock()
        brief.backend_node_id = sentinel.backend_node_id
        brief.intended_state = "some state"

        with contextlib.ExitStack() as stack:
            _get_device = stack.enter_context(unittest.mock.patch.object(
                nb, "_get_device",
            ))
            _get_device.return_value = None

            _clear_error = stack.enter_context(unittest.mock.patch.object(
                nb, "_clear_error",
            ))

            nb.success(brief)

        _get_device.assert_called_once_with(sentinel.backend_node_id)
        _clear_error.assert_not_called()

    def test_success_handles_invalid_device_id(self):
        _, nb = self._create_backend()

        brief = unittest.mock.Mock()
        brief.backend_node_id = sentinel.backend_node_id
        brief.intended_state = "some state"

        with contextlib.ExitStack() as stack:
            _get_device = stack.enter_context(unittest.mock.patch.object(
                nb, "_get_device",
            ))
            _get_device.side_effect = ValueError

            _clear_error = stack.enter_context(unittest.mock.patch.object(
                nb, "_clear_error",
            ))

            nb.success(brief)

        _get_device.assert_called_once_with(sentinel.backend_node_id)
        _clear_error.assert_not_called()

    def test__manage_preflight_raises_if_cluster_is_invalid(self):
        _, nb = self._create_backend()

        device = unittest.mock.Mock()

        with contextlib.ExitStack() as stack:
            _read_cluster = stack.enter_context(unittest.mock.patch.object(
                nb, "_read_cluster",
            ))
            _read_cluster.return_value = None

            _read_node_type = stack.enter_context(unittest.mock.patch.object(
                nb, "_read_node_type",
            ))
            _read_node_type.return_value = True

            _validate_role = stack.enter_context(unittest.mock.patch.object(
                nb, "_validate_role",
            ))

            _autoallocate_ip_addresses_for_device = stack.enter_context(
                unittest.mock.patch.object(
                    nb, "_autoallocate_ip_addresses_for_device",
                )
            )

            with self.assertRaisesRegex(
                    netbox.PreflightError,
                    r"device is not assigned to a Yaook cluster"):
                nb._manage_preflight(device)

        _read_cluster.assert_called_once_with(device)
        _validate_role.assert_not_called()
        _autoallocate_ip_addresses_for_device.assert_not_called()

    def test__manage_preflight_raises_if_role_is_invalid(self):
        _, nb = self._create_backend()

        device = unittest.mock.Mock()
        role = unittest.mock.Mock()

        def full_details_side_effect():
            role.custom_fields = {
                netbox.CF_CONTROL_PLANE: True,
                sentinel.guard: True,
            }

        def checked_validate_role(role):
            self.assertIn(sentinel.guard, role.custom_fields)

        role.full_details.side_effect = full_details_side_effect
        device.role = role

        with contextlib.ExitStack() as stack:
            _read_cluster = stack.enter_context(unittest.mock.patch.object(
                nb, "_read_cluster",
            ))
            _read_cluster.return_value = "the-cluster.local"

            _read_node_type = stack.enter_context(unittest.mock.patch.object(
                nb, "_read_node_type",
            ))
            _read_node_type.return_value = True

            _validate_role = stack.enter_context(unittest.mock.patch.object(
                nb, "_validate_role",
            ))
            _validate_role.side_effect = checked_validate_role
            _validate_role.return_value = False

            _autoallocate_ip_addresses_for_device = stack.enter_context(
                unittest.mock.patch.object(
                    nb, "_autoallocate_ip_addresses_for_device",
                )
            )

            with self.assertRaisesRegex(
                    netbox.PreflightError,
                    r"role is incomplete"):
                nb._manage_preflight(device)

        role.full_details.assert_called_once_with()
        _validate_role.assert_called_once_with(role)
        _autoallocate_ip_addresses_for_device.assert_not_called()

    def test__manage_preflight_assigns_ip_addresses_and_generates_network_config(self):  # noqa:E501
        client, nb = self._create_backend(env={
            "NETBOX_GATEWAY_TAG": "some-gateway-tag",
            "NETBOX_NAMESERVER_FIELD": "some-nameserver-field",
        })

        device = unittest.mock.Mock()
        device.role.custom_fields = {
            netbox.CF_CONTROL_PLANE: True,
            netbox.CF_YAOOK_KUBERNETES_NODE: True,
        }

        def ipalloc_side_effect(device):
            device.__address_allocated = sentinel.allocated

        def checked_make_netplan(_, device, *args, **kwargs):
            self.assertEqual(device.__address_allocated, sentinel.allocated)
            return {"foo": "bar"}

        with contextlib.ExitStack() as stack:
            _read_cluster = stack.enter_context(unittest.mock.patch.object(
                nb, "_read_cluster",
            ))
            _read_cluster.return_value = "the-cluster.local"

            _validate_role = stack.enter_context(unittest.mock.patch.object(
                nb, "_validate_role",
            ))
            _validate_role.return_value = True

            _autoallocate_ip_addresses_for_device = stack.enter_context(
                unittest.mock.patch.object(
                    nb, "_autoallocate_ip_addresses_for_device",
                )
            )
            _autoallocate_ip_addresses_for_device.side_effect = \
                ipalloc_side_effect

            _make_netplan = stack.enter_context(unittest.mock.patch(
                "metal_controller.netbox._make_netplan"
            ))
            _make_netplan.side_effect = checked_make_netplan

            network_config, *_, message = nb._manage_preflight(device)

        _autoallocate_ip_addresses_for_device.assert_called_once_with(device)
        _make_netplan.assert_called_once_with(
            client,
            device,
            "some-gateway-tag",
            "some-nameserver-field",
            ["node.the-cluster.local", "the-cluster.local"],
        )

        self.assertEqual(network_config, {"foo": "bar"})
        self.assertIn("foo: bar", message[1])

    def test__manage_preflight_returns_cluster_domain(self):
        client, nb = self._create_backend(env={
            "NETBOX_GATEWAY_TAG": "some-gateway-tag",
            "NETBOX_NAMESERVER_FIELD": "some-nameserver-field",
        })

        device = unittest.mock.Mock()
        device.role.custom_fields = {
            netbox.CF_CONTROL_PLANE: True,
            netbox.CF_YAOOK_KUBERNETES_NODE: True,
        }

        def ipalloc_side_effect(device):
            device.__address_allocated = sentinel.allocated

        def checked_make_netplan(_, device, *args, **kwargs):
            self.assertEqual(device.__address_allocated, sentinel.allocated)
            return {"foo": "bar"}

        with contextlib.ExitStack() as stack:
            _read_cluster = stack.enter_context(unittest.mock.patch.object(
                nb, "_read_cluster",
            ))
            _read_cluster.return_value = "the-cluster.local"

            _validate_role = stack.enter_context(unittest.mock.patch.object(
                nb, "_validate_role",
            ))
            _validate_role.return_value = True

            _autoallocate_ip_addresses_for_device = stack.enter_context(
                unittest.mock.patch.object(
                    nb, "_autoallocate_ip_addresses_for_device",
                )
            )
            _autoallocate_ip_addresses_for_device.side_effect = \
                ipalloc_side_effect

            _make_netplan = stack.enter_context(unittest.mock.patch(
                "metal_controller.netbox._make_netplan"
            ))
            _make_netplan.side_effect = checked_make_netplan

            *_, cluster_domain, message = nb._manage_preflight(device)

        self.assertEqual(cluster_domain, "the-cluster.local")

    @ddt.data(True, False)
    def test__manage_preflight_returns_control_plane_flag(
            self,
            is_control_plane):
        client, nb = self._create_backend(env={
            "NETBOX_GATEWAY_TAG": "some-gateway-tag",
        })

        device = unittest.mock.Mock()
        device.role.custom_fields = {
            netbox.CF_CONTROL_PLANE: is_control_plane,
            netbox.CF_YAOOK_KUBERNETES_NODE: True,
        }

        def ipalloc_side_effect(device):
            device.__address_allocated = sentinel.allocated

        def checked_make_netplan(_, device, *args, **kwargs):
            self.assertEqual(device.__address_allocated, sentinel.allocated)
            return {"foo": "bar"}

        with contextlib.ExitStack() as stack:
            _read_cluster = stack.enter_context(unittest.mock.patch.object(
                nb, "_read_cluster",
            ))
            _read_cluster.return_value = "the-cluster.local"

            _validate_role = stack.enter_context(unittest.mock.patch.object(
                nb, "_validate_role",
            ))
            _validate_role.return_value = True

            _autoallocate_ip_addresses_for_device = stack.enter_context(
                unittest.mock.patch.object(
                    nb, "_autoallocate_ip_addresses_for_device",
                )
            )
            _autoallocate_ip_addresses_for_device.side_effect = \
                ipalloc_side_effect

            _make_netplan = stack.enter_context(unittest.mock.patch(
                "metal_controller.netbox._make_netplan"
            ))
            _make_netplan.side_effect = checked_make_netplan

            *_, returned_is_control_plane, _, _, message = \
                nb._manage_preflight(device)

        self.assertEqual(is_control_plane, returned_is_control_plane)

    @ddt.data(True, False)
    def test__manage_preflight_returns_yaook_kubernetes_node_flag(
            self,
            should_deploy_yaook_kubernetes):
        client, nb = self._create_backend(env={
            "NETBOX_GATEWAY_TAG": "some-gateway-tag",
        })

        device = unittest.mock.Mock()
        device.role.custom_fields = {
            netbox.CF_CONTROL_PLANE: False,
            netbox.CF_YAOOK_KUBERNETES_NODE:
            model.MetalControllerDeployMethod.YAOOK_K8S,
        }

        def ipalloc_side_effect(device):
            device.__address_allocated = sentinel.allocated

        def checked_make_netplan(_, device, *args, **kwargs):
            self.assertEqual(device.__address_allocated, sentinel.allocated)
            return {"foo": "bar"}

        with contextlib.ExitStack() as stack:
            _read_cluster = stack.enter_context(unittest.mock.patch.object(
                nb, "_read_cluster",
            ))
            _read_cluster.return_value = "the-cluster.local"

            _validate_role = stack.enter_context(unittest.mock.patch.object(
                nb, "_validate_role",
            ))
            _validate_role.return_value = True

            _autoallocate_ip_addresses_for_device = stack.enter_context(
                unittest.mock.patch.object(
                    nb, "_autoallocate_ip_addresses_for_device",
                )
            )
            _autoallocate_ip_addresses_for_device.side_effect = \
                ipalloc_side_effect

            _make_netplan = stack.enter_context(unittest.mock.patch(
                "metal_controller.netbox._make_netplan"
            ))
            _make_netplan.side_effect = checked_make_netplan

            *_, _, returned_is_yaook_kubernetes_node, _, message = \
                nb._manage_preflight(device)

        self.assertTrue(model.MetalControllerDeployMethod.YAOOK_K8S)

    def test_manage(self):
        client, nb = self._create_backend()

        node = unittest.mock.Mock()
        introspection = unittest.mock.Mock()

        device = unittest.mock.Mock(
            ["status", "cluster", "id", "comments", "save", "name",
             "primary_ip4", "primary_ip6", "config_context"])
        device.id = sentinel.device_id
        device.name = sentinel.device_name
        device.status.value = "active"
        device.cluster.id = sentinel.cluster_id
        device.comments = ""
        device.primary_ip4 = unittest.mock.Mock(["address"])
        device.primary_ip4.address = "192.0.2.1/24"
        device.primary_ip6 = unittest.mock.Mock(["address"])
        device.primary_ip6.address = "2001:db8::/64"
        device.config_context = {}

        with contextlib.ExitStack() as stack:
            _find_node_in_netbox = stack.enter_context(
                unittest.mock.patch.object(
                    nb, "_find_node_in_netbox",
                ))
            _find_node_in_netbox.return_value = device

            _validate_device = stack.enter_context(
                unittest.mock.patch.object(
                    nb, "_validate_device",
                ))

            _manage_preflight = stack.enter_context(
                unittest.mock.patch.object(
                    nb, "_manage_preflight",
                ))
            _manage_preflight.return_value = (
                sentinel.network_config,
                sentinel.is_control_plane,
                model.MetalControllerDeployMethod.YAOOK_K8S,
                sentinel.cluster,
                []
            )

            _get_cluster_info = stack.enter_context(
                unittest.mock.patch.object(
                    nb, "_get_cluster_info",
                ))
            _get_cluster_info.return_value = sentinel.cluster_info

            result = nb.manage(node, introspection)

        self.assertEqual(
            backend.DeployAction(
                backend_node_id=str(sentinel.device_id),
                network_config=sentinel.network_config,
                hostname=sentinel.device_name,
                primary_ipv4="192.0.2.1",
                primary_ipv6="2001:db8::",
                cluster=sentinel.cluster,
                cluster_info=sentinel.cluster_info,
                host_vars={},
                is_control_plane=sentinel.is_control_plane,
                metal_controller_deployment_mode=model.MetalControllerDeployMethod.YAOOK_K8S,  # noqa:E501
                config_context={}
            ),
            result
        )

        _find_node_in_netbox.assert_called_once_with(introspection)

        _validate_device.assert_called_once_with(
            device,
            node
        )

        _manage_preflight.assert_called_once_with(device)

        _get_cluster_info.assert_called_once_with(sentinel.cluster_id)

        device.save.assert_called_once()

    def test_manage_returns_pass_action_if_not_managable(self):
        client, nb = self._create_backend()

        node = unittest.mock.Mock(["primary_ip4", "primary_ip6"])
        node.primary_ip4 = sentinel.primary_ip4
        node.primary_ip6 = sentinel.primary_ip6
        introspection = unittest.mock.Mock()

        device = unittest.mock.Mock(
            ["status", "cluster", "id", "comments", "save", "name"])
        device.id = sentinel.device_id
        device.name = sentinel.device_name
        device.status.value = "foo"
        device.cluster.id = sentinel.cluster_id
        device.comments = ""

        with contextlib.ExitStack() as stack:
            _find_node_in_netbox = stack.enter_context(
                unittest.mock.patch.object(
                    nb, "_find_node_in_netbox",
                ))
            _find_node_in_netbox.return_value = device

            _validate_device = stack.enter_context(
                unittest.mock.patch.object(
                    nb, "_validate_device",
                ))

            _manage_preflight = stack.enter_context(
                unittest.mock.patch.object(
                    nb, "_manage_preflight",
                ))
            _manage_preflight.return_value = (
                sentinel.network_config,
                sentinel.is_control_plane,
                sentinel.cluster,
                []
            )

            _get_cluster_info = stack.enter_context(
                unittest.mock.patch.object(
                    nb, "_get_cluster_info",
                ))
            _get_cluster_info.return_value = sentinel.cluster_info

            result = nb.manage(node, introspection)

        self.assertIsInstance(result, backend.PassAction)
        self.assertEqual(result.backend_node_id, str(sentinel.device_id))

        _find_node_in_netbox.assert_called_once_with(introspection)

        _validate_device.assert_called_once_with(
            device,
            node
        )

        _manage_preflight.assert_not_called()

        _get_cluster_info.assert_not_called()

        device.save.assert_not_called()

    def test_manage_returns_pass_action_if_primary_ip_missing(self):
        client, nb = self._create_backend()

        node = unittest.mock.Mock()
        introspection = unittest.mock.Mock()

        device = unittest.mock.Mock(
            ["status", "cluster", "id", "comments", "save", "name",
             "primary_ip4", "primary_ip6"])
        device.id = sentinel.device_id
        device.name = sentinel.device_name
        device.status.value = "active"
        device.cluster.id = sentinel.cluster_id
        device.comments = ""
        device.primary_ip4 = None
        device.primary_ip6 = None

        with contextlib.ExitStack() as stack:
            _find_node_in_netbox = stack.enter_context(
                unittest.mock.patch.object(
                    nb, "_find_node_in_netbox",
                ))
            _find_node_in_netbox.return_value = device

            _validate_device = stack.enter_context(
                unittest.mock.patch.object(
                    nb, "_validate_device",
                ))

            _manage_preflight = stack.enter_context(
                unittest.mock.patch.object(
                    nb, "_manage_preflight",
                ))
            _manage_preflight.return_value = (
                sentinel.network_config,
                sentinel.is_control_plane,
                sentinel.cluster,
                []
            )

            _get_cluster_info = stack.enter_context(
                unittest.mock.patch.object(
                    nb, "_get_cluster_info",
                ))
            _get_cluster_info.return_value = sentinel.cluster_info

            result = nb.manage(node, introspection)

        self.assertEqual(
            backend.PassAction(
                message='waiting for primary ip to be assigned',
                backend_node_id=str(sentinel.device_id),
            ),
            result
        )

        _find_node_in_netbox.assert_called_once_with(introspection)

        _validate_device.assert_called_once_with(
            device,
            node
        )

        _manage_preflight.assert_not_called()

        _get_cluster_info.assert_not_called()

        device.save.assert_not_called()

    @ddt.data(
        # control plane, has v4, has v6
        (True, True, True),
        (False, True, True),
        (True, False, True),
        (False, False, True),
        (True, True, False),
        (False, True, False),
    )
    @ddt.unpack
    def test__get_node_info_returns_primary_ips_and_control_plane_flag(
            self,
            is_control_plane,
            has_ipv4,
            has_ipv6):
        client, nb = self._create_backend()

        device = unittest.mock.Mock()
        device.name = None
        if has_ipv4:
            device.primary_ip4.address = "192.0.2.1/24"
        else:
            device.primary_ip4 = None
        if has_ipv6:
            device.primary_ip6.address = "2001:db8::1/64"
        else:
            device.primary_ip6 = None

        role = unittest.mock.Mock()

        def full_details_side_effect():
            role.custom_fields = {netbox.CF_CONTROL_PLANE: is_control_plane}

        role.full_details.side_effect = full_details_side_effect
        device.role = role

        info = nb._get_node_info(device)

        role.full_details.assert_called_once_with()

        self.assertEqual(info.is_control_plane, is_control_plane)

        if has_ipv4:
            self.assertEqual(info.ipv4, "192.0.2.1")
        else:
            self.assertIsNone(info.ipv4)

        if has_ipv6:
            self.assertEqual(info.ipv6, "2001:db8::1")
        else:
            self.assertIsNone(info.ipv6)

    def test__get_active_nodes_finds_deployable_yaook_nodes(self):
        client, nb = self._create_backend(env={
            "NETBOX_DEPLOYABLE_STATUSES": "foo,bar,baz",
            "NETBOX_CLUSTER_DOMAIN_FIELD": "cf_cluster_domain",
        })

        all_nodes = {}

        def mknode(name, valid=True):
            node = unittest.mock.Mock(["primary_ip4", "primary_ip6"])
            node.name = name
            node.role = unittest.mock.Mock(["full_details"])

            def full_details_side_effect():
                node.role.__is_valid = valid

            node.role.full_details.side_effect = \
                full_details_side_effect
            all_nodes[name] = node
            return node

        def device_gen():
            yield _as_generator([mknode("node1"), mknode("node2", False)])
            yield _as_generator([mknode("node3")])
            yield _as_generator([mknode("node4", False), mknode("node5"),
                                 mknode("node6", False)])

        def node_info_gen():
            for i in itertools.count():
                yield getattr(sentinel, f"nodeinfo{i}")

        def validate_role_gen(role):
            return role.__is_valid

        client.dcim.devices.filter.side_effect = device_gen()

        with contextlib.ExitStack() as stack:
            _get_node_info = stack.enter_context(unittest.mock.patch.object(
                nb, "_get_node_info",
            ))
            _get_node_info.side_effect = node_info_gen()

            _validate_role = stack.enter_context(unittest.mock.patch.object(
                nb, "_validate_role",
            ))
            _validate_role.side_effect = validate_role_gen

            result = nb._get_active_nodes(sentinel.cluster_id)

        self.assertCountEqual(
            [
                unittest.mock.call(status="foo",
                                   cluster_id=sentinel.cluster_id),
                unittest.mock.call(status="bar",
                                   cluster_id=sentinel.cluster_id),
                unittest.mock.call(status="baz",
                                   cluster_id=sentinel.cluster_id),
            ],
            client.dcim.devices.filter.mock_calls,
        )

        self.assertCountEqual(
            [
                unittest.mock.call(node.role)
                for node in all_nodes.values()
            ],
            _validate_role.mock_calls,
        )

        for node in all_nodes.values():
            node.role.full_details.assert_called_once_with()

        self.assertCountEqual(
            [
                unittest.mock.call(all_nodes["node1"]),
                unittest.mock.call(all_nodes["node3"]),
                unittest.mock.call(all_nodes["node5"]),
            ],
            _get_node_info.mock_calls,
        )

        self.assertDictEqual(
            {
                "node1": sentinel.nodeinfo0,
                "node3": sentinel.nodeinfo1,
                "node5": sentinel.nodeinfo2,
            },
            result,
        )

    def test__get_active_nodes_skips_nodes_without_primary_ip(self):
        client, nb = self._create_backend(env={
            "NETBOX_DEPLOYABLE_STATUSES": "foo,bar,baz",
        })

        all_nodes = {}

        def mknode(name, valid=True, primary_ip=True):
            node = unittest.mock.Mock(["primary_ip4", "primary_ip6"])
            node.name = name
            if not primary_ip:
                node.primary_ip4 = None
                node.primary_ip6 = None
            node.role = unittest.mock.Mock(["full_details"])

            def full_details_side_effect():
                node.role.__is_valid = valid

            node.role.full_details.side_effect = \
                full_details_side_effect
            all_nodes[name] = node
            return node

        def device_gen():
            yield _as_generator([mknode("node1"), mknode("node2", False)])
            yield _as_generator([mknode("node3", primary_ip=False)])
            yield _as_generator([mknode("node4", False), mknode("node5"),
                                 mknode("node6", False)])

        def node_info_gen():
            for i in itertools.count():
                yield getattr(sentinel, f"nodeinfo{i}")

        def validate_role_gen(role):
            return role.__is_valid

        client.dcim.devices.filter.side_effect = device_gen()

        with contextlib.ExitStack() as stack:
            _get_node_info = stack.enter_context(unittest.mock.patch.object(
                nb, "_get_node_info",
            ))
            _get_node_info.side_effect = node_info_gen()

            _validate_role = stack.enter_context(unittest.mock.patch.object(
                nb, "_validate_role",
            ))
            _validate_role.side_effect = validate_role_gen

            result = nb._get_active_nodes(sentinel.cluster_id)

        self.assertCountEqual(
            [
                unittest.mock.call(status="foo",
                                   cluster_id=sentinel.cluster_id),
                unittest.mock.call(status="bar",
                                   cluster_id=sentinel.cluster_id),
                unittest.mock.call(status="baz",
                                   cluster_id=sentinel.cluster_id),
            ],
            client.dcim.devices.filter.mock_calls,
        )

        self.assertCountEqual(
            [
                unittest.mock.call(node.role)
                for node in all_nodes.values()
            ],
            _validate_role.mock_calls,
        )

        for node in all_nodes.values():
            node.role.full_details.assert_called_once_with()

        self.assertCountEqual(
            [
                unittest.mock.call(all_nodes["node1"]),
                unittest.mock.call(all_nodes["node5"]),
            ],
            _get_node_info.mock_calls,
        )

        self.assertDictEqual(
            {
                "node1": sentinel.nodeinfo0,
                "node5": sentinel.nodeinfo1,
            },
            result,
        )

    def test__get_active_nodes_raises_if_it_does_not_find_any_node(self):
        client, nb = self._create_backend(env={
            "NETBOX_DEPLOYABLE_STATUSES": "foo",
        })

        all_nodes = {}

        def mknode(name, valid=True, primary_ip=True):
            node = unittest.mock.Mock(["primary_ip4", "primary_ip6"])
            node.name = name
            if not primary_ip:
                node.primary_ip4 = None
                node.primary_ip6 = None
            node.role = unittest.mock.Mock(["full_details"])

            def full_details_side_effect():
                node.role.__is_valid = valid

            node.role.full_details.side_effect = \
                full_details_side_effect
            all_nodes[name] = node
            return node

        def device_gen():
            yield _as_generator([mknode("node1", primary_ip=False)])

        def node_info_gen():
            for i in itertools.count():
                yield getattr(sentinel, f"nodeinfo{i}")

        def validate_role_gen(role):
            return role.__is_valid

        client.dcim.devices.filter.side_effect = device_gen()

        with contextlib.ExitStack() as stack:
            _get_node_info = stack.enter_context(unittest.mock.patch.object(
                nb, "_get_node_info",
            ))
            _get_node_info.side_effect = node_info_gen()

            _validate_role = stack.enter_context(unittest.mock.patch.object(
                nb, "_validate_role",
            ))
            _validate_role.side_effect = validate_role_gen

            with self.assertRaises(ValueError):
                nb._get_active_nodes(sentinel.cluster_id)

    def test_get_cluster_info_obtains_cluster_and_returns_info(self):
        client, nb = self._create_backend(env={
            "NETBOX_CLUSTER_DOMAIN_FIELD": "cf_cluster_domain",
        })

        cluster = unittest.mock.Mock()

        client.virtualization.clusters.filter.return_value = _as_generator(
            [cluster],
        )

        with contextlib.ExitStack() as stack:
            _get_active_nodes = stack.enter_context(unittest.mock.patch.object(
                nb, "_get_active_nodes",
            ))
            _get_active_nodes.return_value = sentinel.nodes

            result = nb.get_cluster_info(sentinel.cluster_domain)

        client.virtualization.clusters.filter.assert_called_once_with(
            cf_cluster_domain=sentinel.cluster_domain,
        )

        _get_active_nodes.assert_called_once_with(cluster.id)

        self.assertEqual(result.active_nodes, sentinel.nodes)
        self.assertIsNone(result.api_server_frontend_ip)
        self.assertIsNone(result.api_server_vrrp_ip)

    def test_get_cluster_info_raises_lookup_error_if_cluster_not_found(self):
        client, nb = self._create_backend(env={
            "NETBOX_CLUSTER_DOMAIN_FIELD": "cf_cluster_domain",
        })

        client.virtualization.clusters.filter.return_value = _as_generator(
            [],
        )

        with self.assertRaises(LookupError):
            nb.get_cluster_info(sentinel.cluster_domain)

        client.virtualization.clusters.filter.assert_called_once_with(
            cf_cluster_domain=sentinel.cluster_domain,
        )

    def test_validate_hostname_regex(self):
        self.assertTrue(netbox.match_hostname_re("iamashorthostname1337"))
        self.assertTrue(netbox.match_hostname_re("hostname01.do.mai.n.here"))

        self.assertFalse(netbox.match_hostname_re("longhostname"
                                                  "alsolongerthan31"
                                                  "charactersperiod"))
        self.assertFalse(netbox.match_hostname_re("longhostname"
                                                  "alsolongerthan31"
                                                  "charactersperiod.domain"
                                                  ".example"))
