#!/usr/bin/python3

# how to use: ./configure_root_device.py <node>

import argparse
import json
import subprocess

ap = argparse.ArgumentParser()
ap.add_argument("name")

args = ap.parse_args()

node_info = json.loads(subprocess.run(
    ["openstack", "baremetal", "node", "show", "-f", "json", "--", args.name],
    capture_output=True, check=True).stdout)
introspection_data = json.loads(subprocess.run(
    ["openstack", "baremetal", "introspection", "data", "save", "--", args.name],
    capture_output=True, check=True).stdout)


def human_size(size):
    suffix_list = ["kiB", "MiB", "GiB", "TiB", "PiB"]
    suffix = ""

    while size > 1024 and suffix_list:
        size /= 1024
        suffix = suffix_list.pop(0)

    return f"{size:.2f}{suffix}"


print("RAID interface", node_info["raid_interface"])

for i, disk in enumerate(introspection_data["inventory"]["disks"]):
    if disk["wwn"] == node_info["properties"].get("root_device", {}).get("wwn"):
        print("Disk", i, "(selected as primary root device)")
    else:
        print("Disk", i)
    print("    Capacity", human_size(disk["size"]))
    if disk["rotational"]:
        print("    Type HDD")
    else:
        print("    Type SSD")
    print("    Model", disk["model"])
    print()

disk_number = input("disk number (leave empty for unchanged): ").strip()

if disk_number:
    root_device_hint = json.dumps(
        {"wwn": introspection_data["inventory"]["disks"][int(disk_number)]["wwn"]})
    subprocess.run(["openstack", "baremetal", "node", "set", "--property",
                    f"root_device={root_device_hint}", "--", args.name], check=True)
